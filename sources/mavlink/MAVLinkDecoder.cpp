#include "headers/mavlink/MAVLinkDecoder.hpp"
#include "headers/mavlink/MAVLinkEnums.hpp"

QString MAVLinkDecoder::Result::FieldName(int index) const {
    return m_values[index].first;
}

QStringList MAVLinkDecoder::Result::FieldValue(int index) const {
    return m_values[index].second;
}

void MAVLinkDecoder::Result::AddField(const QString& name, const QString& value) {
    m_values.push_back(qMakePair(name, value));
}

void MAVLinkDecoder::Result::AddField(const QString& name, const QStringList& values) {
    m_values.push_back(qMakePair(name, values));
}

int MAVLinkDecoder::Result::FieldQty() const {
    return m_values.size();
}

MAVLinkDecoder::Result MAVLinkDecoder::DecodeFrame(const QString& rawFrame) {
    if(rawFrame.at(0) == 'f' && rawFrame.at(1) == 'e') {
        return decodeFrameV1(rawFrame);
    } else if(rawFrame.at(0) == 'f' && rawFrame.at(1) == 'd') {
        return decodeFrameV2(rawFrame);
    }
    throw std::runtime_error("unknown frame start");
}

MAVLinkDecoder::Result MAVLinkDecoder::decodeFrameV1(const QString& rawFrame) {
    int currPos = 0;
    Result res;

    res.AddField(enum_cast<FRAME>(FRAME::STX), "0x" + rawFrame.mid(currPos, 2));
    currPos += 2;

    int payloadLen = rawFrame.mid(currPos, 2).toInt(nullptr, 16);
    res.AddField(enum_cast<FRAME>(FRAME::LEN), QString::number(payloadLen));
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::SEQ), QString::number(rawFrame.mid(currPos, 2).toInt(nullptr, 16)));
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::SYS_ID), QString::number(rawFrame.mid(currPos, 2).toInt(nullptr, 16)));
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::COMP_ID), QString::number(rawFrame.mid(currPos, 2).toInt(nullptr, 16)));
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::MSG_ID), QString::number(rawFrame.mid(currPos, 2).toInt(nullptr, 16)));
    currPos += 2;

    QString payload;
    for(int i = 0; i < payloadLen; i++) {
        payload += rawFrame.mid(currPos + i * 2, 2) + " ";
    }
    payload = payload.trimmed();
    res.AddField(enum_cast<FRAME>(FRAME::PAYLOAD), payload);
    currPos += 2 * payloadLen;

    res.AddField(enum_cast<FRAME>(FRAME::CHECKSUM), "0x" + rawFrame.mid(currPos + 2, 2) + rawFrame.mid(currPos, 2));
    currPos += 4;

    return res;
}

MAVLinkDecoder::Result MAVLinkDecoder::decodeFrameV2(const QString& rawFrame) {
    int currPos = 0;
    Result res;

    res.AddField(enum_cast<FRAME>(FRAME::STX), "0x" + rawFrame.mid(currPos, 2));
    currPos += 2;

    int payloadLen = rawFrame.mid(currPos, 2).toInt(nullptr, 16);
    res.AddField(enum_cast<FRAME>(FRAME::LEN), QString::number(payloadLen));
    currPos += 2;

    QString isSigned = rawFrame.mid(currPos, 2);
    res.AddField(enum_cast<FRAME>(FRAME::INC_FLAGS), "0x" + isSigned); // TODO process as bitmask
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::CMP_FLAGS), "0x" + rawFrame.mid(currPos, 2));
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::SEQ), QString::number(rawFrame.mid(currPos, 2).toInt(nullptr, 16)));
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::SYS_ID), QString::number(rawFrame.mid(currPos, 2).toInt(nullptr, 16)));
    currPos += 2;

    res.AddField(enum_cast<FRAME>(FRAME::COMP_ID), QString::number(rawFrame.mid(currPos, 2).toInt(nullptr, 16)));
    currPos += 2;

    QString msgId;
    msgId += rawFrame.mid(currPos + 4, 2);
    msgId += rawFrame.mid(currPos + 2, 2);
    msgId += rawFrame.mid(currPos, 2);
    res.AddField(enum_cast<FRAME>(FRAME::MSG_ID), QString::number(msgId.toInt(nullptr, 16)));
    currPos += 6;

    QString payload;
    for(int i = 0; i < payloadLen; i++) {
        payload += rawFrame.mid(currPos + i * 2, 2) + " ";
    }
    payload = payload.trimmed();
    res.AddField(enum_cast<FRAME>(FRAME::PAYLOAD), payload);
    currPos += 2 * payloadLen;

    res.AddField(enum_cast<FRAME>(FRAME::CHECKSUM), "0x" + rawFrame.mid(currPos + 2, 2) + rawFrame.mid(currPos, 2));
    currPos += 4;

    if(currPos != rawFrame.length()) {
        QString signature;
        for(int i = 0; i < 13; i++) {
            signature += "0x" + rawFrame.mid(currPos + i * 2, 2) + " ";
        }
        signature = signature.trimmed();
        res.AddField(enum_cast<FRAME>(FRAME::SIGNATURE), signature);
        currPos += 26;
    } else {
        Q_ASSERT(isSigned == "00");
        res.AddField(enum_cast<FRAME>(FRAME::SIGNATURE), "-");
    }

    return res;
}
