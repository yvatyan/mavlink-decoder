#include "headers/ui/MainWindow.hpp"
#include "headers/ui/DecodedFrameV1.hpp"
#include "headers/ui/DecodedFrameV2.hpp"
#include "headers/mavlink/MAVLinkDecoder.hpp"
#include "ui_MainWindow.h"
#include <QTextBlock>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);
    m_ui->m_leCurrentFrame->setValidator(new QIntValidator(m_ui->m_leCurrentFrame));

    QObject::connect(m_ui->m_pbDecodeStream, SIGNAL(clicked()), this, SLOT(decodeStream()));
    QObject::connect(m_ui->m_pbGoToFirst, &QPushButton::clicked, [this]() {changeFrame(Direction::First);});
    QObject::connect(m_ui->m_pbGoPrevious, &QPushButton::clicked, [this]() {changeFrame(Direction::Previous);});
    QObject::connect(m_ui->m_leCurrentFrame, &QLineEdit::returnPressed, [this]() {changeFrame(Direction::Specified);});
    QObject::connect(m_ui->m_pbGoNext, &QPushButton::clicked, [this]() {changeFrame(Direction::Next);});
    QObject::connect(m_ui->m_pbGoToLast, &QPushButton::clicked, [this]() {changeFrame(Direction::Last);});
}

MainWindow::~MainWindow() {
    delete m_ui;
}

void MainWindow::decodeStream() {
    for(int i = m_ui->stackedWidget->count() - 1; i >= 0; --i) {
        QWidget* widget = m_ui->stackedWidget->widget(i);
        m_ui->stackedWidget->removeWidget(widget);
    }

    QTextDocument* doc = m_ui->m_pteInput->document();
    for(int i = 0; i < doc->lineCount(); ++i) {
        QTextBlock line = doc->findBlockByLineNumber(i);
        QString frame = line.text();
        if(frame.isEmpty()) {
            continue;
        }

        MAVLinkDecoder::Result decodedFrame = MAVLinkDecoder::DecodeFrame(frame);
        if(decodedFrame.FieldValue(0).at(0) == "0xfe") {
            m_ui->stackedWidget->addWidget(new DecodedFrameV1(decodedFrame));
        } else if(decodedFrame.FieldValue(0).at(0) == "0xfd") {
            m_ui->stackedWidget->addWidget(new DecodedFrameV2(decodedFrame));
        } else {
            throw std::runtime_error("invlaid MAVLink packet decoded");
        }
    }
    m_ui->m_leCurrentFrame->setText(QString::number(m_ui->stackedWidget->currentIndex()));
    m_ui->m_pbGoToFirst->setText("Go To First [0]");
    m_ui->m_pbGoToLast->setText("Go To Last [" + QString::number(m_ui->stackedWidget->count() - 1) + "]");
    m_ui->tabWidget->setCurrentIndex(1);
}

void MainWindow::changeFrame(MainWindow::Direction d) {
    if(m_ui->stackedWidget->count() == 0) {
        return;
    }

    int currentIndex = m_ui->stackedWidget->currentIndex();
    switch(d) {
        case Direction::First : {
            currentIndex = 0;
            m_ui->stackedWidget->setCurrentIndex(currentIndex);
            break;
        }
        case Direction::Previous : {
            if(currentIndex > 0) {
                m_ui->stackedWidget->setCurrentIndex(--currentIndex);
            }
            break;
        }
        case Direction::Specified : {
            int frameValue = m_ui->m_leCurrentFrame->text().toInt();
            if(frameValue >= 0 && frameValue < m_ui->stackedWidget->count()) {
                currentIndex = frameValue;
                m_ui->stackedWidget->setCurrentIndex(currentIndex);
            }
            break;
        }
        case Direction::Next : {
            if(currentIndex < m_ui->stackedWidget->count() - 1) {
                m_ui->stackedWidget->setCurrentIndex(++currentIndex);
            }
            break;
        }
        case Direction::Last : {
            currentIndex = m_ui->stackedWidget->count() - 1;
            m_ui->stackedWidget->setCurrentIndex(currentIndex);
            break;
        }
    }
    m_ui->m_leCurrentFrame->setText(QString::number(currentIndex));
}

