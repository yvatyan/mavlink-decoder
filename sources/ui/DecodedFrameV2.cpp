#include "headers/ui/DecodedFrameV2.hpp"
#include "ui_DecodedFrameV2.h"

DecodedFrameV2::DecodedFrameV2(const MAVLinkDecoder::Result& frame, QWidget *parent)
    : QWidget(parent)
    , m_ui(new Ui::decodedFrameV2)
{
    m_ui->setupUi(this);

    m_ui->m_lStartMarker->setText(frame.FieldName(0));
    m_ui->m_lPayloadLength->setText(frame.FieldName(1));
    m_ui->m_lIncompatibilityFlags->setText(frame.FieldName(2));
    m_ui->m_lCompatibilityFlags->setText(frame.FieldName(3));
    m_ui->m_lSequenceNum->setText(frame.FieldName(4));
    m_ui->m_lSystemID->setText(frame.FieldName(5));
    m_ui->m_lComponentID->setText(frame.FieldName(6));
    m_ui->m_lMessageID->setText(frame.FieldName(7));
    m_ui->m_lPayloadData->setText(frame.FieldName(8));
    m_ui->m_lChecksum->setText(frame.FieldName(9));
    m_ui->m_lSignature->setText(frame.FieldName(10));

    m_ui->m_lStartMarkerValue->setText(frame.FieldValue(0).at(0));
    m_ui->m_lPayloadLengthValue->setText(frame.FieldValue(1).at(0));
    m_ui->m_lIncompatibilityFlagsValue->setText(frame.FieldValue(2).at(0));
    m_ui->m_lCompatibilityFlagsValue->setText(frame.FieldValue(3).at(0));
    m_ui->m_lSequenceNumValue->setText(frame.FieldValue(4).at(0));
    m_ui->m_lSystemIDValue->setText(frame.FieldValue(5).at(0));
    m_ui->m_lComponentIDValue->setText(frame.FieldValue(6).at(0));
    m_ui->m_lMessageIDValue->setText(frame.FieldValue(7).at(0));
    m_ui->m_ptePayloadDataValue->setPlainText(frame.FieldValue(8).at(0));
    m_ui->m_lChecksumValue->setText(frame.FieldValue(9).at(0));
    m_ui->m_lSignatureValue->setText(frame.FieldValue(10).at(0));
}
