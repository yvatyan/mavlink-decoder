#include "headers/ui/DecodedFrameV1.hpp"
#include "ui_DecodedFrameV1.h"

DecodedFrameV1::DecodedFrameV1(const MAVLinkDecoder::Result& frame, QWidget *parent)
    : QWidget(parent)
    , m_ui(new Ui::decodedFrameV1)
{
    m_ui->setupUi(this);

    m_ui->m_lStartMarker->setText(frame.FieldName(0));
    m_ui->m_lPayloadLength->setText(frame.FieldName(1));
    m_ui->m_lSequenceNum->setText(frame.FieldName(2));
    m_ui->m_lSystemID->setText(frame.FieldName(3));
    m_ui->m_lComponentID->setText(frame.FieldName(4));
    m_ui->m_lMessageID->setText(frame.FieldName(5));
    m_ui->m_lPayloadData->setText(frame.FieldName(6));
    m_ui->m_lChecksum->setText(frame.FieldName(7));

    m_ui->m_lStartMarkerValue->setText(frame.FieldValue(0).at(0));
    m_ui->m_lPayloadLengthValue->setText(frame.FieldValue(1).at(0));
    m_ui->m_lSequenceNumValue->setText(frame.FieldValue(2).at(0));
    m_ui->m_lSystemIDValue->setText(frame.FieldValue(3).at(0));
    m_ui->m_lComponentIDValue->setText(frame.FieldValue(4).at(0));
    m_ui->m_lMessageIDValue->setText(frame.FieldValue(5).at(0));
    m_ui->m_ptePayloadDataValue->setPlainText(frame.FieldValue(6).at(0));
    m_ui->m_lChecksumValue->setText(frame.FieldValue(7).at(0));
}
