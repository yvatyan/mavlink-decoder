#ifndef DECODEDFRAMEV1_HPP
#define DECODEDFRAMEV1_HPP

#include "headers/mavlink/MAVLinkDecoder.hpp"
#include <QWidget>

namespace Ui {
    class decodedFrameV1;
}

class DecodedFrameV1 : public QWidget {
public:
    DecodedFrameV1(const MAVLinkDecoder::Result& frame, QWidget* parent = nullptr);
private:
    Ui::decodedFrameV1* m_ui;
};

#endif // DECODEDFRAMEV1_HPP
