#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    enum class Direction {
        First,
        Previous,
        Specified,
        Next,
        Last
    };
public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void decodeStream();

private:
    Ui::MainWindow* m_ui;

    void changeFrame(Direction d);
};
#endif // MAINWINDOW_H
