#ifndef DECODEDFRAMEV2_HPP
#define DECODEDFRAMEV2_HPP

#include "headers/mavlink/MAVLinkDecoder.hpp"
#include <QWidget>

namespace Ui {
    class decodedFrameV2;
}

class DecodedFrameV2 : public QWidget {
public:
    DecodedFrameV2(const MAVLinkDecoder::Result& frame, QWidget* parent = nullptr);
private:
    Ui::decodedFrameV2* m_ui;
};

#endif // DECODEDFRAMEV2_HPP
