#ifndef MAVLINKDECODER_HPP
#define MAVLINKDECODER_HPP

#include <QString>
#include <QVector>
#include <QPair>

class MAVLinkDecoder {
public:
    class Result {
    public:
        QString FieldName(int index) const;
        QStringList FieldValue(int index) const;
        void AddField(const QString& name, const QString& value);
        void AddField(const QString& name, const QStringList& values);
        void InsertField(int before, const QString& name, const QString& value);
        void InsertField(int before, const QString& name, const QStringList& values);
        int FieldQty() const;
    private:
        QVector<QPair<QString, QStringList>> m_values;
    };
    static Result DecodeFrame(const QString& rawFrame);
    static Result DecodePayload(const QString& rawPayload);
private:
    static Result decodeFrameV1(const QString& rawFrame);
    static Result decodeFrameV2(const QString& rawFrame);
};

#endif // MAVLINKDECODER_HPP
