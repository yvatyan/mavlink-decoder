#ifndef MAVLINKENUMS_HPP
#define MAVLINKENUMS_HPP

#include <QString>

// TODO: Add Accronims, glossary, return "" to end functions, check that nothing new is added

template<typename T>
QString enum_cast(T value) {
    Q_UNUSED(value)
    throw std::runtime_error("unsupported enum cast");
}

enum class FRAME {
    STX = 0,
    LEN = 1,
    INC_FLAGS = 2,
    CMP_FLAGS = 3,
    SEQ = 4,
    SYS_ID = 5,
    COMP_ID = 6,
    MSG_ID = 7,
    PAYLOAD = 8,
    CHECKSUM = 9,
    SIGNATURE = 10
};

template<>
QString enum_cast<FRAME>(FRAME value) {
    switch(value) {
        case FRAME::STX :
            return "packet start marker";
        case FRAME::LEN :
            return "payload length";
        case FRAME::INC_FLAGS :
            return "incompatible flags";
        case FRAME::CMP_FLAGS :
            return "compatible flags";
        case FRAME::SEQ :
            return "packet sequence number";
        case FRAME::SYS_ID :
            return "system ID";
        case FRAME::COMP_ID :
            return "component ID";
        case FRAME::MSG_ID :
            return "message ID";
        case FRAME::PAYLOAD :
            return "payload data";
        case FRAME::CHECKSUM :
            return "checksum";
        case FRAME::SIGNATURE :
            return "signature";
    }
}

enum class MAV_AUTOPILOT {
    MAV_AUTOPILOT_GENERIC = 0,
    MAV_AUTOPILOT_RESERVED = 1,
    MAV_AUTOPILOT_SLUGS = 2,
    MAV_AUTOPILOT_ARDUPILOTMEGA = 3,
    MAV_AUTOPILOT_OPENPILOT = 4,
    MAV_AUTOPILOT_GENERICIC_WAYPOINTS_ONLY = 5,
    MAV_AUTOPILOT_GENERIC_WAYPOINTS_AND_SIMPLE_NAVIGATION_ONLY = 6,
    MAV_AUTOPILOT_GENERIC_MISSION_FULL = 7,
    MAV_AUTOPILOT_INVALID = 8,
    MAV_AUTOPILOT_PPZ = 9,
    MAV_AUTOPILOT_UDB = 10,
    MAV_AUTOPILOT_FP = 11,
    MAV_AUTOPILOT_PX4 = 12,
    MAV_AUTOPILOT_SMACCMPILOT = 13,
    MAV_AUTOPILOT_AUTOQUAD = 14,
    MAV_AUTOPILOT_ARMAZILA = 15,
    MAV_AUTOPILOT_AEROB = 16,
    MAV_AUTOPILOT_ASLUAV = 17,
    MAV_AUTOPILOT_SMARTAP = 18,
    MAV_AUTOPILOT_AIRRAILS = 19
};

template<>
QString enum_cast<MAV_AUTOPILOT>(MAV_AUTOPILOT value) {
    switch(value) {
        case MAV_AUTOPILOT::MAV_AUTOPILOT_GENERIC :
            return "generic";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_RESERVED :
            return "reserved";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_SLUGS :
            return "SLUGS";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_ARDUPILOTMEGA :
            return "ArduPilot";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_OPENPILOT :
            return "OpenPilot";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_GENERICIC_WAYPOINTS_ONLY :
            return "generic supporting waypoints only";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_GENERIC_WAYPOINTS_AND_SIMPLE_NAVIGATION_ONLY :
            return "generic supporting waypoints and navigation";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_GENERIC_MISSION_FULL :
            return "generic supproting full mission command set";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_INVALID :
            return "not valid";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_PPZ :
            return "PPZ UAV";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_UDB :
            return "UAV dev board";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_FP :
            return "FlexiPilot";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_PX4 :
            return "PX4";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_SMACCMPILOT :
            return "SMACCMPilot";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_AUTOQUAD :
            return "AutoQuad";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_ARMAZILA :
            return "Armazila";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_AEROB :
            return "Aerob";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_ASLUAV :
            return "ASLUAV";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_SMARTAP :
            return "SmartAP";
        case MAV_AUTOPILOT::MAV_AUTOPILOT_AIRRAILS :
            return "AirRails";
    }
}

enum class MAV_TYPE {
    MAV_TYPE_GENERIC = 0,
    MAV_TYPE_FIXED_WING = 1,
    MAV_TYPE_QUADROTOR = 2,
    MAV_TYPE_COAXIAL = 3,
    MAV_TYPE_HELICOPTER = 4,
    MAV_TYPE_ANTENNA_TRACKER = 5,
    MAV_TYPE_GCS = 6,
    MAV_TYPE_AIRSHIP = 7,
    MAV_TYPE_FREE_BALLON = 8,
    MAV_TYPE_ROCKET = 9,
    MAV_TYPE_GROUND_ROVER = 10,
    MAV_TYPE_SURFACE_BOAT = 11,
    MAV_TYPE_SUBMARINE = 12,
    MAV_TYPE_HEXAROTOR = 13,
    MAV_TYPE_OCTOROTOR = 14,
    MAV_TYPE_TRICOPTER = 15,
    MAV_TYPE_FLAPPING_WING = 16,
    MAV_TYPE_KITE = 17,
    MAV_TYPE_ONBOARD_CONTROLLER = 18,
    MAV_TYPE_VTOL_DUOROTOR = 19,
    MAV_TYPE_VTOL_QUADROTOR = 20,
    MAV_TYPE_VTOL_TILTROTOR = 21,
    MAV_TYPE_VTOL_RESERVED2 = 22,
    MAV_TYPE_VTOL_RESERVED3 = 23,
    MAV_TYPE_VTOL_RESERVED4 = 24,
    MAV_TYPE_VTOL_RESERVED5 = 25,
    MAV_TYPE_GIMBAL = 26,
    MAV_TYPE_ADSB = 27,
    MAV_TYPE_PARAFOIL = 28,
    MAV_TYPE_DODECAROTOR = 29,
    MAV_TYPE_CAMERA = 30,
    MAV_TYPE_CHARGING_STATION = 31,
    MAV_TYPE_FLARM = 32,
    MAV_TYPE_SERVO = 33,
    MAV_TYPE_ODID = 34
};

template<>
QString enum_cast<MAV_TYPE>(MAV_TYPE value) {
    switch(value) {
        case MAV_TYPE::MAV_TYPE_GENERIC :
            return "generic micro air";
        case MAV_TYPE::MAV_TYPE_FIXED_WING :
            return "fixed wing";
        case MAV_TYPE::MAV_TYPE_QUADROTOR :
            return "quadrotor";
        case MAV_TYPE::MAV_TYPE_COAXIAL :
            return "coaxial helicopter";
        case MAV_TYPE::MAV_TYPE_HELICOPTER :
            return "helicopter with tail";
        case MAV_TYPE::MAV_TYPE_ANTENNA_TRACKER :
            return "ground installation";
        case MAV_TYPE::MAV_TYPE_GCS :
            return "ground control station";
        case MAV_TYPE::MAV_TYPE_AIRSHIP :
            return "airship";
        case MAV_TYPE::MAV_TYPE_FREE_BALLON :
            return "uncontrolled balloon";
        case MAV_TYPE::MAV_TYPE_ROCKET :
            return "rocket";
        case MAV_TYPE::MAV_TYPE_GROUND_ROVER :
            return "ground rover";
        case MAV_TYPE::MAV_TYPE_SURFACE_BOAT :
            return "surface boat";
        case MAV_TYPE::MAV_TYPE_SUBMARINE :
            return "submarine";
        case MAV_TYPE::MAV_TYPE_HEXAROTOR :
            return "hexarotor";
        case MAV_TYPE::MAV_TYPE_OCTOROTOR :
            return "octarotor";
        case MAV_TYPE::MAV_TYPE_TRICOPTER :
            return "tricopter";
        case MAV_TYPE::MAV_TYPE_FLAPPING_WING :
            return "ornithopter";
        case MAV_TYPE::MAV_TYPE_KITE :
            return "kite";
        case MAV_TYPE::MAV_TYPE_ONBOARD_CONTROLLER :
            return "onboard companion controller";
        case MAV_TYPE::MAV_TYPE_VTOL_DUOROTOR :
            return "dualrotor VTOL";
        case MAV_TYPE::MAV_TYPE_VTOL_QUADROTOR :
            return "quadrotor VTOL";
        case MAV_TYPE::MAV_TYPE_VTOL_TILTROTOR :
            return "tiltrotor VTOL";
        case MAV_TYPE::MAV_TYPE_VTOL_RESERVED2 :
            return "VTOL reserved 2";
        case MAV_TYPE::MAV_TYPE_VTOL_RESERVED3 :
            return "VTOL reserved 3";
        case MAV_TYPE::MAV_TYPE_VTOL_RESERVED4 :
            return "VTOL reserved 4";
        case MAV_TYPE::MAV_TYPE_VTOL_RESERVED5 :
            return "VTOL reserved 5";
        case MAV_TYPE::MAV_TYPE_GIMBAL :
            return "gimbal";
        case MAV_TYPE::MAV_TYPE_ADSB :
            return "ADSB system";
        case MAV_TYPE::MAV_TYPE_PARAFOIL :
            return "nonrigid airfoil";
        case MAV_TYPE::MAV_TYPE_DODECAROTOR :
            return "dodecarotor";
        case MAV_TYPE::MAV_TYPE_CAMERA :
            return "camera";
        case MAV_TYPE::MAV_TYPE_CHARGING_STATION :
            return "charging station";
        case MAV_TYPE::MAV_TYPE_FLARM :
            return "FLARM collision avoidance system";
        case MAV_TYPE::MAV_TYPE_SERVO :
            return "servo";
        case MAV_TYPE::MAV_TYPE_ODID :
            return "Open Drone ID";
    }
}

enum class FIRMWARE_VERSION_TYPE {
    FIRMWARE_VERSION_TYPE_DEV = 0,
    FIRMWARE_VERSION_TYPE_ALPHA = 64,
    FIRMWARE_VERSION_TYPE_BETA = 128,
    FIRMWARE_VERSION_TYPE_RC = 192,
    FIRMWARE_VERSION_TYPE_OFFICIAL = 255
};

template<>
QString enum_cast<FIRMWARE_VERSION_TYPE>(FIRMWARE_VERSION_TYPE value) {
    switch(value) {
        case FIRMWARE_VERSION_TYPE::FIRMWARE_VERSION_TYPE_DEV :
            return "development release";
        case FIRMWARE_VERSION_TYPE::FIRMWARE_VERSION_TYPE_ALPHA :
            return "alpha release";
        case FIRMWARE_VERSION_TYPE::FIRMWARE_VERSION_TYPE_BETA :
            return "beta release";
        case FIRMWARE_VERSION_TYPE::FIRMWARE_VERSION_TYPE_RC :
            return "release candidate";
        case FIRMWARE_VERSION_TYPE::FIRMWARE_VERSION_TYPE_OFFICIAL :
            return "official stable release";
    }
}

enum class HL_FAILURE_FLAG {
    HL_FAILURE_FLAG_GPS = 0x0001,
    HL_FAILURE_FLAG_DIFFERENTIAL_PRESSURE = 0x0002,
    HL_FAILURE_FLAG_ABSOLUTE_PRESSURE = 0x0004,
    HL_FAILURE_FLAG_3D_ACCEL = 0x0008,
    HL_FAILURE_FLAG_3D_GYRO = 0x0010,
    HL_FAILURE_FLAG_MAG = 0x0020,
    HL_FAILURE_FLAG_TERRAIN = 0x0040,
    HL_FAILURE_FLAG_BATTERY = 0x0080,
    HL_FAILURE_FLAG_RC_RECEIVER = 0x0100,
    HL_FAILURE_FLAG_OFFBOARD_LINK = 0x0200,
    HL_FAILURE_FLAG_ENGINE = 0x0400,
    HL_FAILURE_FLAG_GEOFENCE = 0x0800,
    HL_FAILURE_FLAG_ESTIMATOR = 0x1000,
    HL_FAILURE_FLAG_MISSION = 0x2000
};

template<>
QString enum_cast<HL_FAILURE_FLAG>(HL_FAILURE_FLAG value) {
    switch(value) {
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_GPS :
            return "GPS failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_DIFFERENTIAL_PRESSURE :
            return "differential pressure sensor failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_ABSOLUTE_PRESSURE :
            return "absolute pressure sensor failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_3D_ACCEL :
            return "accelerometer sensor failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_3D_GYRO :
            return "gyroscope sensor failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_MAG :
            return "magnetometer sensor failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_TERRAIN :
            return "terrain subsystem failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_BATTERY :
            return "battery failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_RC_RECEIVER :
            return "RC receiver failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_OFFBOARD_LINK :
            return "offboard link failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_ENGINE :
            return "engine failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_GEOFENCE :
            return "geofence failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_ESTIMATOR :
            return "estimator failure";
        case HL_FAILURE_FLAG::HL_FAILURE_FLAG_MISSION :
            return "mission failure";
    }
}

enum class MAV_MODE_FLAG {
    MAV_MODE_FLAG_CUSTOM_MODE_ENABLED = 0x01,
    MAV_MODE_FLAG_TEST_ENABLED = 0x02,
    MAV_MODE_FLAG_AUTO_ENABLED = 0x04,
    MAV_MODE_FLAG_GUIDED_ENABLED = 0x08,
    MAV_MODE_FLAG_STABILIZE_ENABLED = 0x10,
    MAV_MODE_FLAG_HIL_ENABLED = 0x20,
    MAV_MODE_FLAG_MANUAL_INPUT_ENABLED = 0x40,
    MAV_MODE_FLAG_SAFETY_ARMED = 0x80
};

template<>
QString enum_cast<MAV_MODE_FLAG>(MAV_MODE_FLAG value) {
    switch(value) {
        case MAV_MODE_FLAG::MAV_MODE_FLAG_CUSTOM_MODE_ENABLED :
            return "reserved";
        case MAV_MODE_FLAG::MAV_MODE_FLAG_TEST_ENABLED :
            return "test mode enabled";
        case MAV_MODE_FLAG::MAV_MODE_FLAG_AUTO_ENABLED :
            return "auto mode enabled";
        case MAV_MODE_FLAG::MAV_MODE_FLAG_GUIDED_ENABLED :
            return "guided mode enabled";
        case MAV_MODE_FLAG::MAV_MODE_FLAG_STABILIZE_ENABLED :
            return "stabilization enabled";
        case MAV_MODE_FLAG::MAV_MODE_FLAG_HIL_ENABLED :
            return "hardware in the loop";
        case MAV_MODE_FLAG::MAV_MODE_FLAG_MANUAL_INPUT_ENABLED :
            return "remote control mode";
        case MAV_MODE_FLAG::MAV_MODE_FLAG_SAFETY_ARMED :
            return "safety set to armed";
    }
}

enum class MAV_GOTO {
    MAV_GOTO_DO_HOLD = 0,
    MAV_GOTO_DO_CONTINUE = 1,
    MAV_GOTO_HOLD_AT_CURRENT_POSITION = 2,
    MAV_GOTO_HOLD_AT_SPECIFIED_POSITION = 3
};

template<>
QString enum_cast<MAV_GOTO>(MAV_GOTO value) {
    switch(value) {
        case MAV_GOTO::MAV_GOTO_DO_HOLD :
            return "hold at the current position";
        case MAV_GOTO::MAV_GOTO_DO_CONTINUE :
            return "continue with the mission";
        case MAV_GOTO::MAV_GOTO_HOLD_AT_CURRENT_POSITION :
            return "hold at the current position of the system";
        case MAV_GOTO::MAV_GOTO_HOLD_AT_SPECIFIED_POSITION :
            return "hold at the specified position";
    }
}

enum class MAV_MODE {
    MAV_MODE_PREFLIGHT = 0,
    MAV_MODE_MANUAL_DISARMED = 64,
    MAV_MODE_TEST_DISARMED = 66,
    MAV_MODE_STABILIZE_DISARMED = 80,
    MAV_MODE_GUIDED_DISARMED = 88,
    MAV_MODE_AUTO_DISARMED = 92,
    MAV_MODE_MANUAL_ARMED = 192,
    MAV_MODE_TEST_ARMED = 194,
    MAV_MODE_STABILIZE_ARMED = 208,
    MAV_MODE_GUIDED_ARMED = 216,
    MAV_MODE_AUTO_ARMED = 220
};

template<>
QString enum_cast<MAV_MODE>(MAV_MODE value) {
    switch(value) {
        case MAV_MODE::MAV_MODE_PREFLIGHT :
            return "system is not ready";
        case MAV_MODE::MAV_MODE_MANUAL_DISARMED :
            return "disarmed under manual control";
        case MAV_MODE::MAV_MODE_TEST_DISARMED :
            return "test disarmed";
        case MAV_MODE::MAV_MODE_STABILIZE_DISARMED :
            return "disarmed under assisted manual control";
        case MAV_MODE::MAV_MODE_GUIDED_DISARMED :
            return "disarmed under auto control";
        case MAV_MODE::MAV_MODE_AUTO_DISARMED :
            return "disrmed under auto control and navigation";
        case MAV_MODE::MAV_MODE_MANUAL_ARMED :
            return "armed under manual control";
        case MAV_MODE::MAV_MODE_TEST_ARMED :
            return "test armed";
        case MAV_MODE::MAV_MODE_STABILIZE_ARMED :
            return "armed under assisted manual control";
        case MAV_MODE::MAV_MODE_GUIDED_ARMED :
            return "armed under auto control";
        case MAV_MODE::MAV_MODE_AUTO_ARMED :
            return "armed under auto control and navigation";
    }
}

enum class MAV_STATE {
    MAV_STATE_UNINIT = 0,
    MAV_STATE_BOOT,
    MAV_STATE_CALIBRATING,
    MAV_STATE_STANDBY,
    MAV_STATE_ACTIVE,
    MAV_STATE_CRITICAL,
    MAV_STATE_EMERGENCY,
    MAV_STATE_POWEROFF,
    MAV_STATE_FLIGHT_TERMINATION
};

template<>
QString enum_cast<MAV_STATE>(MAV_STATE value) {
    switch(value) {
        case MAV_STATE::MAV_STATE_UNINIT :
            return "uninitialized system";
        case MAV_STATE::MAV_STATE_BOOT :
            return "system is booting";
        case MAV_STATE::MAV_STATE_CALIBRATING :
            return "susyem is calibrating";
        case MAV_STATE::MAV_STATE_STANDBY :
            return "system is on standby";
        case MAV_STATE::MAV_STATE_ACTIVE :
            return "system is active";
        case MAV_STATE::MAV_STATE_CRITICAL :
            return "system is in a non-normal state";
        case MAV_STATE::MAV_STATE_EMERGENCY :
            return "system is in mayday";
        case MAV_STATE::MAV_STATE_POWEROFF :
            return "system initialized power-off sequence";
        case MAV_STATE::MAV_STATE_FLIGHT_TERMINATION :
            return "system is terminating";
    }
}

enum class MAV_COMPONENT {
    MAV_COMP_ID_ALL = 0,
    MAV_COMP_ID_AUTOPILOT1 = 1,
    MAV_COMP_ID_USER1 = 25,
    MAV_COMP_ID_USER2 = 26,
    MAV_COMP_ID_USER3 = 27,
    MAV_COMP_ID_USER4 = 28,
    MAV_COMP_ID_USER5 = 29,
    MAV_COMP_ID_USER6 = 30,
    MAV_COMP_ID_USER7 = 31,
    MAV_COMP_ID_USER8 = 32,
    MAV_COMP_ID_USER9 = 33,
    MAV_COMP_ID_USER10 = 34,
    MAV_COMP_ID_USER11 = 35,
    MAV_COMP_ID_USER12 = 36,
    MAV_COMP_ID_USER13 = 37,
    MAV_COMP_ID_USER14 = 38,
    MAV_COMP_ID_USER15 = 39,
    MAV_COMP_ID_USER16 = 40,
    MAV_COMP_ID_USER17 = 41,
    MAV_COMP_ID_USER18 = 42,
    MAV_COMP_ID_USER19 = 43,
    MAV_COMP_ID_USER20 = 44,
    MAV_COMP_ID_USER21 = 45,
    MAV_COMP_ID_USER22 = 46,
    MAV_COMP_ID_USER23 = 47,
    MAV_COMP_ID_USER24 = 48,
    MAV_COMP_ID_USER25 = 49,
    MAV_COMP_ID_USER26 = 50,
    MAV_COMP_ID_USER27 = 51,
    MAV_COMP_ID_USER28 = 52,
    MAV_COMP_ID_USER29 = 53,
    MAV_COMP_ID_USER30 = 54,
    MAV_COMP_ID_USER31 = 55,
    MAV_COMP_ID_USER32 = 56,
    MAV_COMP_ID_USER33 = 57,
    MAV_COMP_ID_USER34 = 58,
    MAV_COMP_ID_USER35 = 59,
    MAV_COMP_ID_USER36 = 60,
    MAV_COMP_ID_USER37 = 61,
    MAV_COMP_ID_USER38 = 62,
    MAV_COMP_ID_USER39 = 63,
    MAV_COMP_ID_USER40 = 64,
    MAV_COMP_ID_USER41 = 65,
    MAV_COMP_ID_USER42 = 66,
    MAV_COMP_ID_USER43 = 67,
    MAV_COMP_ID_TELEMETRY_RADIO = 68,
    MAV_COMP_ID_USER45 = 69,
    MAV_COMP_ID_USER46 = 70,
    MAV_COMP_ID_USER47 = 71,
    MAV_COMP_ID_USER48 = 72,
    MAV_COMP_ID_USER49 = 73,
    MAV_COMP_ID_USER50 = 74,
    MAV_COMP_ID_USER51 = 75,
    MAV_COMP_ID_USER52 = 76,
    MAV_COMP_ID_USER53 = 77,
    MAV_COMP_ID_USER54 = 78,
    MAV_COMP_ID_USER55 = 79,
    MAV_COMP_ID_USER56 = 80,
    MAV_COMP_ID_USER57 = 81,
    MAV_COMP_ID_USER58 = 82,
    MAV_COMP_ID_USER59 = 83,
    MAV_COMP_ID_USER60 = 84,
    MAV_COMP_ID_USER61 = 85,
    MAV_COMP_ID_USER62 = 86,
    MAV_COMP_ID_USER63 = 87,
    MAV_COMP_ID_USER64 = 88,
    MAV_COMP_ID_USER65 = 89,
    MAV_COMP_ID_USER66 = 90,
    MAV_COMP_ID_USER67 = 91,
    MAV_COMP_ID_USER68 = 92,
    MAV_COMP_ID_USER69 = 93,
    MAV_COMP_ID_USER70 = 94,
    MAV_COMP_ID_USER71 = 95,
    MAV_COMP_ID_USER72 = 96,
    MAV_COMP_ID_USER73 = 97,
    MAV_COMP_ID_USER74 = 98,
    MAV_COMP_ID_USER75 = 99,
    MAV_COMP_ID_CAMERA = 100,
    MAV_COMP_ID_CAMERA2 = 101,
    MAV_COMP_ID_CAMERA3 = 102,
    MAV_COMP_ID_CAMERA4 = 103,
    MAV_COMP_ID_CAMERA5 = 104,
    MAV_COMP_ID_CAMERA6 = 105,
    MAV_COMP_ID_SERVO1 = 140,
    MAV_COMP_ID_SERVO2 = 141,
    MAV_COMP_ID_SERVO3 = 142,
    MAV_COMP_ID_SERVO4 = 143,
    MAV_COMP_ID_SERVO5 = 144,
    MAV_COMP_ID_SERVO6 = 145,
    MAV_COMP_ID_SERVO7 = 146,
    MAV_COMP_ID_SERVO8 = 147,
    MAV_COMP_ID_SERVO9 = 148,
    MAV_COMP_ID_SERVO10 = 149,
    MAV_COMP_ID_SERVO11 = 150,
    MAV_COMP_ID_SERVO12 = 151,
    MAV_COMP_ID_SERVO13 = 152,
    MAV_COMP_ID_SERVO14 = 153,
    MAV_COMP_ID_GIMBAL = 154,
    MAV_COMP_ID_LOG = 155,
    MAV_COMP_ID_ADSB = 156,
    MAV_COMP_ID_OSD = 157,
    MAV_COMP_ID_PERIPHERAL = 158,
    MAV_COMP_ID_QX1_GIMBAL = 159,
    MAV_COMP_ID_FLARM = 160,
    MAV_COMP_ID_GIMBAL2 = 171,
    MAV_COMP_ID_GIMBAL3 = 172,
    MAV_COMP_ID_GIMBAL4 = 173,
    MAV_COMP_ID_GIMBAL5 = 174,
    MAV_COMP_ID_GIMBAL6 = 175,
    MAV_COMP_ID_MISSIONPLANNER = 190,
    MAV_COMP_ID_ONBOARD_COMPUTER = 191,
    MAV_COMP_ID_PATHPLANNER = 195,
    MAV_COMP_ID_OBSTACLE_AVOIDANCE = 196,
    MAV_COMP_ID_VISUAL_INERTIAL_ODOMETRY = 197,
    MAV_COMP_ID_PAIRING_MANAGER = 198,
    MAV_COMP_ID_IMU = 200,
    MAV_COMP_ID_IMU_2 = 201,
    MAV_COMP_ID_IMU_3 = 202,
    MAV_COMP_ID_GPS = 220,
    MAV_COMP_ID_GPS2 = 221,
    MAV_COMP_ID_ODID_TXRX_1 = 236,
    MAV_COMP_ID_ODID_TXRX_2 = 237,
    MAV_COMP_ID_ODID_TXRX_3 = 238,
    MAV_COMP_ID_UDP_BRIDGE = 240,
    MAV_COMP_ID_UART_BRIDGE = 241,
    MAV_COMP_ID_TUNNEL_NODE = 242,
    MAV_COMP_ID_SYSTEM_CONTROL = 250
};

template<>
QString enum_cast<MAV_COMPONENT>(MAV_COMPONENT value) {
    switch(value) {
        case MAV_COMPONENT::MAV_COMP_ID_ALL :
            return "all components";
        case MAV_COMPONENT::MAV_COMP_ID_AUTOPILOT1 :
            return "autopilot";
        case MAV_COMPONENT::MAV_COMP_ID_USER1 :
            return "private network component 1";
        case MAV_COMPONENT::MAV_COMP_ID_USER2 :
            return "private network component 2";
        case MAV_COMPONENT::MAV_COMP_ID_USER3 :
            return "private network component 3";
        case MAV_COMPONENT::MAV_COMP_ID_USER4 :
            return "private network component 4";
        case MAV_COMPONENT::MAV_COMP_ID_USER5 :
            return "private network component 5";
        case MAV_COMPONENT::MAV_COMP_ID_USER6 :
            return "private network component 6";
        case MAV_COMPONENT::MAV_COMP_ID_USER7 :
            return "private network component 7";
        case MAV_COMPONENT::MAV_COMP_ID_USER8 :
            return "private network component 8";
        case MAV_COMPONENT::MAV_COMP_ID_USER9 :
            return "private network component 9";
        case MAV_COMPONENT::MAV_COMP_ID_USER10 :
            return "private network component 10";
        case MAV_COMPONENT::MAV_COMP_ID_USER11 :
            return "private network component 11";
        case MAV_COMPONENT::MAV_COMP_ID_USER12 :
            return "private network component 12";
        case MAV_COMPONENT::MAV_COMP_ID_USER13 :
            return "private network component 13";
        case MAV_COMPONENT::MAV_COMP_ID_USER14 :
            return "private network component 14";
        case MAV_COMPONENT::MAV_COMP_ID_USER15 :
            return "private network component 15";
        case MAV_COMPONENT::MAV_COMP_ID_USER16 :
            return "private network component 16";
        case MAV_COMPONENT::MAV_COMP_ID_USER17 :
            return "private network component 17";
        case MAV_COMPONENT::MAV_COMP_ID_USER18 :
            return "private network component 18";
        case MAV_COMPONENT::MAV_COMP_ID_USER19 :
            return "private network component 19";
        case MAV_COMPONENT::MAV_COMP_ID_USER20 :
            return "private network component 20";
        case MAV_COMPONENT::MAV_COMP_ID_USER21 :
            return "private network component 21";
        case MAV_COMPONENT::MAV_COMP_ID_USER22 :
            return "private network component 22";
        case MAV_COMPONENT::MAV_COMP_ID_USER23 :
            return "private network component 23";
        case MAV_COMPONENT::MAV_COMP_ID_USER24 :
            return "private network component 24";
        case MAV_COMPONENT::MAV_COMP_ID_USER25 :
            return "private network component 25";
        case MAV_COMPONENT::MAV_COMP_ID_USER26 :
            return "private network component 26";
        case MAV_COMPONENT::MAV_COMP_ID_USER27 :
            return "private network component 27";
        case MAV_COMPONENT::MAV_COMP_ID_USER28 :
            return "private network component 28";
        case MAV_COMPONENT::MAV_COMP_ID_USER29 :
            return "private network component 29";
        case MAV_COMPONENT::MAV_COMP_ID_USER30 :
            return "private network component 30";
        case MAV_COMPONENT::MAV_COMP_ID_USER31 :
            return "private network component 31";
        case MAV_COMPONENT::MAV_COMP_ID_USER32 :
            return "private network component 32";
        case MAV_COMPONENT::MAV_COMP_ID_USER33 :
            return "private network component 33";
        case MAV_COMPONENT::MAV_COMP_ID_USER34 :
            return "private network component 34";
        case MAV_COMPONENT::MAV_COMP_ID_USER35 :
            return "private network component 35";
        case MAV_COMPONENT::MAV_COMP_ID_USER36 :
            return "private network component 36";
        case MAV_COMPONENT::MAV_COMP_ID_USER37 :
            return "private network component 37";
        case MAV_COMPONENT::MAV_COMP_ID_USER38 :
            return "private network component 38";
        case MAV_COMPONENT::MAV_COMP_ID_USER39 :
            return "private network component 39";
        case MAV_COMPONENT::MAV_COMP_ID_USER40 :
            return "private network component 40";
        case MAV_COMPONENT::MAV_COMP_ID_USER41 :
            return "private network component 41";
        case MAV_COMPONENT::MAV_COMP_ID_USER42 :
            return "private network component 42";
        case MAV_COMPONENT::MAV_COMP_ID_USER43 :
            return "private network component 43";
        case MAV_COMPONENT::MAV_COMP_ID_TELEMETRY_RADIO :
            return "telemetry radio";
        case MAV_COMPONENT::MAV_COMP_ID_USER45 :
            return "private network component 45";
        case MAV_COMPONENT::MAV_COMP_ID_USER46 :
            return "private network component 46";
        case MAV_COMPONENT::MAV_COMP_ID_USER47 :
            return "private network component 47";
        case MAV_COMPONENT::MAV_COMP_ID_USER48 :
            return "private network component 48";
        case MAV_COMPONENT::MAV_COMP_ID_USER49 :
            return "private network component 49";
        case MAV_COMPONENT::MAV_COMP_ID_USER50 :
            return "private network component 50";
        case MAV_COMPONENT::MAV_COMP_ID_USER51 :
            return "private network component 51";
        case MAV_COMPONENT::MAV_COMP_ID_USER52 :
            return "private network component 52";
        case MAV_COMPONENT::MAV_COMP_ID_USER53 :
            return "private network component 53";
        case MAV_COMPONENT::MAV_COMP_ID_USER54 :
            return "private network component 54";
        case MAV_COMPONENT::MAV_COMP_ID_USER55 :
            return "private network component 55";
        case MAV_COMPONENT::MAV_COMP_ID_USER56 :
            return "private network component 56";
        case MAV_COMPONENT::MAV_COMP_ID_USER57 :
            return "private network component 57";
        case MAV_COMPONENT::MAV_COMP_ID_USER58 :
            return "private network component 58";
        case MAV_COMPONENT::MAV_COMP_ID_USER59 :
            return "private network component 59";
        case MAV_COMPONENT::MAV_COMP_ID_USER60 :
            return "private network component 60";
        case MAV_COMPONENT::MAV_COMP_ID_USER61 :
            return "private network component 61";
        case MAV_COMPONENT::MAV_COMP_ID_USER62 :
            return "private network component 62";
        case MAV_COMPONENT::MAV_COMP_ID_USER63 :
            return "private network component 63";
        case MAV_COMPONENT::MAV_COMP_ID_USER64 :
            return "private network component 64";
        case MAV_COMPONENT::MAV_COMP_ID_USER65 :
            return "private network component 65";
        case MAV_COMPONENT::MAV_COMP_ID_USER66 :
            return "private network component 66";
        case MAV_COMPONENT::MAV_COMP_ID_USER67 :
            return "private network component 67";
        case MAV_COMPONENT::MAV_COMP_ID_USER68 :
            return "private network component 68";
        case MAV_COMPONENT::MAV_COMP_ID_USER69 :
            return "private network component 69";
        case MAV_COMPONENT::MAV_COMP_ID_USER70 :
            return "private network component 70";
        case MAV_COMPONENT::MAV_COMP_ID_USER71 :
            return "private network component 71";
        case MAV_COMPONENT::MAV_COMP_ID_USER72 :
            return "private network component 72";
        case MAV_COMPONENT::MAV_COMP_ID_USER73 :
            return "private network component 73";
        case MAV_COMPONENT::MAV_COMP_ID_USER74 :
            return "private network component 74";
        case MAV_COMPONENT::MAV_COMP_ID_USER75 :
            return "private network component 75";
        case MAV_COMPONENT::MAV_COMP_ID_CAMERA :
            return "camera 1";
        case MAV_COMPONENT::MAV_COMP_ID_CAMERA2 :
            return "camera 2";
        case MAV_COMPONENT::MAV_COMP_ID_CAMERA3 :
            return "camera 3";
        case MAV_COMPONENT::MAV_COMP_ID_CAMERA4 :
            return "camera 4";
        case MAV_COMPONENT::MAV_COMP_ID_CAMERA5 :
            return "camera 5";
        case MAV_COMPONENT::MAV_COMP_ID_CAMERA6 :
            return "camera 6";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO1 :
            return "servo 1";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO2 :
            return "servo 2";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO3 :
            return "servo 3";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO4 :
            return "servo 4";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO5 :
            return "servo 5";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO6 :
            return "servo 6";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO7 :
            return "servo 7";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO8 :
            return "servo 8";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO9 :
            return "servo 9";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO10 :
            return "serov 10";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO11 :
            return "servo 11";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO12 :
            return "servo 12";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO13 :
            return "servo 13";
        case MAV_COMPONENT::MAV_COMP_ID_SERVO14 :
            return "servo 14";
        case MAV_COMPONENT::MAV_COMP_ID_GIMBAL :
            return "gimbal 1";
        case MAV_COMPONENT::MAV_COMP_ID_LOG :
            return "logging component";
        case MAV_COMPONENT::MAV_COMP_ID_ADSB :
            return "ADS-B (automatic dependent surveillance-broadcast)";
        case MAV_COMPONENT::MAV_COMP_ID_OSD :
            return "OSD (on screen display)";
        case MAV_COMPONENT::MAV_COMP_ID_PERIPHERAL :
            return "autopilot peripheral component";
        case MAV_COMPONENT::MAV_COMP_ID_QX1_GIMBAL :
            return "gimbal id for QX1";
        case MAV_COMPONENT::MAV_COMP_ID_FLARM :
            return "FLARM collision alert";
        case MAV_COMPONENT::MAV_COMP_ID_GIMBAL2 :
            return "gimbal 2";
        case MAV_COMPONENT::MAV_COMP_ID_GIMBAL3 :
            return "gimbal 3";
        case MAV_COMPONENT::MAV_COMP_ID_GIMBAL4 :
            return "gimbal 4";
        case MAV_COMPONENT::MAV_COMP_ID_GIMBAL5 :
            return "gimbal 5";
        case MAV_COMPONENT::MAV_COMP_ID_GIMBAL6 :
            return "gimbal 6";
        case MAV_COMPONENT::MAV_COMP_ID_MISSIONPLANNER :
            return "mission planner";
        case MAV_COMPONENT::MAV_COMP_ID_ONBOARD_COMPUTER :
            return "onboard computer";
        case MAV_COMPONENT::MAV_COMP_ID_PATHPLANNER :
            return "path finding component";
        case MAV_COMPONENT::MAV_COMP_ID_OBSTACLE_AVOIDANCE :
            return "collision avoidance component";
        case MAV_COMPONENT::MAV_COMP_ID_VISUAL_INERTIAL_ODOMETRY :
            return "VIO (visual inertial odometry)";
        case MAV_COMPONENT::MAV_COMP_ID_PAIRING_MANAGER :
            return "GCS to vehicle pairing component";
        case MAV_COMPONENT::MAV_COMP_ID_IMU :
            return "inertial measurement unit 1";
        case MAV_COMPONENT::MAV_COMP_ID_IMU_2 :
            return "inertial measurement unit 2";
        case MAV_COMPONENT::MAV_COMP_ID_IMU_3 :
            return "inertial measurement unit 3";
        case MAV_COMPONENT::MAV_COMP_ID_GPS :
            return "gps 1";
        case MAV_COMPONENT::MAV_COMP_ID_GPS2 :
            return "gps 2";
        case MAV_COMPONENT::MAV_COMP_ID_ODID_TXRX_1 :
            return "Open Drone ID T/R 1";
        case MAV_COMPONENT::MAV_COMP_ID_ODID_TXRX_2 :
            return "Open Drone ID T/R 2";
        case MAV_COMPONENT::MAV_COMP_ID_ODID_TXRX_3 :
            return "Open Drone ID T/R 3";
        case MAV_COMPONENT::MAV_COMP_ID_UDP_BRIDGE :
            return "MAVLink to UDP bridge";
        case MAV_COMPONENT::MAV_COMP_ID_UART_BRIDGE :
            return "MAVLink to UART bridge";
        case MAV_COMPONENT::MAV_COMP_ID_TUNNEL_NODE :
            return "TUNNEL messages handling component";
        case MAV_COMPONENT::MAV_COMP_ID_SYSTEM_CONTROL :
            return "system messages handling component";
    }
}

enum class MAV_SYS_STATUS_SENSOR {
    MAV_SYS_STATUS_SENSOR_3D_GYRO = 0x00000001,
    MAV_SYS_STATUS_SENSOR_3D_ACCEL = 0x00000002,
    MAV_SYS_STATUS_SENSOR_3D_MAG = 0x00000004,
    MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE = 0x00000008,
    MAV_SYS_STATUS_SENSOR_DIFFERENTIAL_PRESSURE = 0x00000010,
    MAV_SYS_STATUS_SENSOR_GPS = 0x00000020,
    MAV_SYS_STATUS_SENSOR_OPTICAL_FLOW = 0x00000040,
    MAV_SYS_STATUS_SENSOR_VISION_POSITION = 0x00000080,
    MAV_SYS_STATUS_SENSOR_LASER_POSITION = 0x00000100,
    MAV_SYS_STATUS_SENSOR_EXTERNAL_GROUND_TRUTH = 0x00000200,
    MAV_SYS_STATUS_SENSOR_ANGULAR_RATE_CONTROL = 0x00000400,
    MAV_SYS_STATUS_SENSOR_ATTITUDE_STABILIZATION = 0x00000800,
    MAV_SYS_STATUS_SENSOR_YAW_POSITION = 0x00001000,
    MAV_SYS_STATUS_SENSOR_Z_ALTITUDE_CONTROL = 0x00002000,
    MAV_SYS_STATUS_SENSOR_XY_POSITION_CONTROL = 0x00004000,
    MAV_SYS_STATUS_SENSOR_MOTOR_OUTPUTS = 0x00008000,
    MAV_SYS_STATUS_SENSOR_RC_RECEIVER = 0x00010000,
    MAV_SYS_STATUS_SENSOR_3D_GYRO2 = 0x00020000,
    MAV_SYS_STATUS_SENSOR_3D_ACCEL2 = 0x00040000,
    MAV_SYS_STATUS_SENSOR_3D_MAG2 = 0x00080000,
    MAV_SYS_STATUS_SENSOR_GEOFENCE = 0x00100000,
    MAV_SYS_STATUS_SENSOR_AHRS = 0x00200000,
    MAV_SYS_STATUS_SENSOR_TERRAIN = 0x00400000,
    MAV_SYS_STATUS_SENSOR_REVERSE_MOTOR = 0x00800000,
    MAV_SYS_STATUS_SENSOR_LOGGING = 0x01000000,
    MAV_SYS_STATUS_SENSOR_BATTERY = 0x02000000,
    MAV_SYS_STATUS_SENSOR_PROXIMITY = 0x04000000,
    MAV_SYS_STATUS_SENSOR_SATCOM = 0x08000000,
    MAV_SYS_STATUS_SENSOR_PREARM_CHECK = 0x10000000,
    MAV_SYS_STATUS_SENSOR_OBSTACLE_AVOIDANCE = 0x20000000
};

template<>
QString enum_cast<MAV_SYS_STATUS_SENSOR>(MAV_SYS_STATUS_SENSOR value) {
    switch(value) {
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_3D_GYRO :
            return "3D gyro";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_3D_ACCEL :
            return "3D accelerometer";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_3D_MAG :
            return "3D magnetometer";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE :
            return "absolute barometer";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_DIFFERENTIAL_PRESSURE :
            return "differential barometer";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_GPS :
            return "gps";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_OPTICAL_FLOW :
            return "optical flow";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_VISION_POSITION :
            return "CV position";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_LASER_POSITION :
            return "laser based position";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_EXTERNAL_GROUND_TRUTH :
            return "external ground truth";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_ANGULAR_RATE_CONTROL :
            return "3D angualr rate control";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_ATTITUDE_STABILIZATION :
            return "attitude stabilization";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_YAW_POSITION :
            return "yaw position";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_Z_ALTITUDE_CONTROL :
            return "altitude control";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_XY_POSITION_CONTROL :
            return "x/y position control";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_MOTOR_OUTPUTS :
            return "motor outputs/control";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_RC_RECEIVER :
            return "rc receiver";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_3D_GYRO2 :
            return "3D gyro 2";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_3D_ACCEL2 :
            return "3D accelerometer 2";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_3D_MAG2 :
            return "3D magnetometer 2";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_GEOFENCE :
            return "geofence";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_AHRS :
            return "AHRS (attitude and heading reference system)";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_TERRAIN :
            return "terrain subsystem";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_REVERSE_MOTOR :
            return "motors are reversed";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_LOGGING :
            return "logging";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_BATTERY :
            return "battery";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_PROXIMITY :
            return "proximity";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_SATCOM :
            return "satellite communication";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_PREARM_CHECK :
            return "pre-arm check status";
        case MAV_SYS_STATUS_SENSOR::MAV_SYS_STATUS_SENSOR_OBSTACLE_AVOIDANCE :
            return "collision prevention";
    }
}

enum class MAV_FRAME {
    MAV_FRAME_GLOABAL = 0,
    MAV_FRAME_LOCAL_NED = 1,
    MAV_FRAME_FRAME_MISSION = 2,
    MAV_FRAME_GLOBAL_RELATIVE_ALT = 3,
    MAV_FRAME_LOCAL_ENU = 4,
    MAV_FRAME_GLOVAL_INT = 5,
    MAV_FRAME_GLOBAL_RELATIVE_ALT_INT = 6,
    MAV_FRAME_LOCAL_OFFSET_NED = 7,
    MAV_FRAME_BODY_NED = 8,
    MAV_FRAME_BODY_OFFSET_NED = 9,
    MAV_FRAME_GLOBAL_TERRAIN_ALT = 10,
    MAV_FRAME_GLOBAL_TERRAIN_ALT_INT = 11,
    MAV_FRAME_BODY_FRD = 12,
    MAV_FRAME_BODY_FLU = 13,
    MAV_FRAME_MOCAP_NED = 14,
    MAV_FRAME_MOCAP_ENU = 15,
    MAV_FRAME_VISION_NED = 16,
    MAV_FRAME_VISION_ENU = 17,
    MAV_FRAME_ESTIM_NED = 18,
    MAV_FRAME_ESTIM_ENU = 19,
    MAV_FRAME_LOCAL_FRD = 20,
    MAV_FRAME_LOCAL_FLU = 21
};

template<>
QString enum_cast<MAV_FRAME>(MAV_FRAME value) {
    switch(value) {
        case MAV_FRAME::MAV_FRAME_GLOABAL :
            return "global coordinates with MSL altitude";
        case MAV_FRAME::MAV_FRAME_LOCAL_NED :
            return "local NED coordinates";
        case MAV_FRAME::MAV_FRAME_FRAME_MISSION :
            return "mission command";
        case MAV_FRAME::MAV_FRAME_GLOBAL_RELATIVE_ALT :
            return "global coordinates with home relative altitude";
        case MAV_FRAME::MAV_FRAME_LOCAL_ENU :
            return "local ENU coordinates";
        case MAV_FRAME::MAV_FRAME_GLOVAL_INT :
            return "global coordinates INT with MSL altitude";
        case MAV_FRAME::MAV_FRAME_GLOBAL_RELATIVE_ALT_INT :
            return "global coordinates INT with home relative altitude";
        case MAV_FRAME::MAV_FRAME_LOCAL_OFFSET_NED :
            return "offset to the current local frame";
        case MAV_FRAME::MAV_FRAME_BODY_NED :
            return "body reference NED coordinates";
        case MAV_FRAME::MAV_FRAME_BODY_OFFSET_NED :
            return "offset in body NED coordinates";
        case MAV_FRAME::MAV_FRAME_GLOBAL_TERRAIN_ALT :
            return "global coordinates with AGL altitude";
        case MAV_FRAME::MAV_FRAME_GLOBAL_TERRAIN_ALT_INT :
            return "global coordinates INT with AGL altitude";
        case MAV_FRAME::MAV_FRAME_BODY_FRD :
            return "body reference FRD coordinates";
        case MAV_FRAME::MAV_FRAME_BODY_FLU :
            return "body reference FLU coordinates";
        case MAV_FRAME::MAV_FRAME_MOCAP_NED :
            return "odometry local NED coordinates from motion capture";
        case MAV_FRAME::MAV_FRAME_MOCAP_ENU :
            return "odometry local ENU coordinates from motion capture";
        case MAV_FRAME::MAV_FRAME_VISION_NED :
            return "odometry local NED coordinates from vision estimation";
        case MAV_FRAME::MAV_FRAME_VISION_ENU :
            return "odometry local ENU coordinates from vision estimation";
        case MAV_FRAME::MAV_FRAME_ESTIM_NED :
            return "odometry local NED coordinates from estimator";
        case MAV_FRAME::MAV_FRAME_ESTIM_ENU :
            return "odometry local ENU coordinates from estimator";
        case MAV_FRAME::MAV_FRAME_LOCAL_FRD :
            return "local FRD coordinates";
        case MAV_FRAME::MAV_FRAME_LOCAL_FLU :
            return "local FLU coordinates";
    }
}

enum class MAVLINK_DATA_STREAM_TYPE {
    MAVLINK_DATA_STREAM_IMG_JPEG,
    MAVLINK_DATA_STREAM_IMG_BMP,
    MAVLINK_DATA_STREAM_IMG_RAW8U,
    MAVLINK_DATA_STREAM_IMG_RAW32U,
    MAVLINK_DATA_STREAM_IMG_PGM,
    MAVLINK_DATA_STREAM_IMG_PNG
};

template<>
QString enum_cast<MAVLINK_DATA_STREAM_TYPE>(MAVLINK_DATA_STREAM_TYPE value) {
    switch(value) {
        case MAVLINK_DATA_STREAM_TYPE::MAVLINK_DATA_STREAM_IMG_JPEG :
            return "JPEG";
        case MAVLINK_DATA_STREAM_TYPE::MAVLINK_DATA_STREAM_IMG_BMP :
            return "BMP";
        case MAVLINK_DATA_STREAM_TYPE::MAVLINK_DATA_STREAM_IMG_RAW8U :
            return "raw unsigned 8";
        case MAVLINK_DATA_STREAM_TYPE::MAVLINK_DATA_STREAM_IMG_RAW32U :
            return "raw unsigned 32";
        case MAVLINK_DATA_STREAM_TYPE::MAVLINK_DATA_STREAM_IMG_PGM :
            return "PGM";
        case MAVLINK_DATA_STREAM_TYPE::MAVLINK_DATA_STREAM_IMG_PNG :
            return "PNG";
    }
}

enum class FENCE_ACTION {
    FENCE_ACTION_NODE = 0,
    FENCE_ACTION_GUIDED = 1,
    FENCE_ACTION_REPORT = 2,
    FENCE_ACTION_GUIDED_THR_PASS = 3,
    FENCE_ACTION_RTL = 4
};

template<>
QString enum_cast<FENCE_ACTION>(FENCE_ACTION value) {
    switch(value) {
        case FENCE_ACTION::FENCE_ACTION_NODE :
            return "disable fence mode";
        case FENCE_ACTION::FENCE_ACTION_GUIDED :
            return "guided mode";
        case FENCE_ACTION::FENCE_ACTION_REPORT :
            return "report fence breach";
        case FENCE_ACTION::FENCE_ACTION_GUIDED_THR_PASS :
            return "guided mode with manual throttle";
        case FENCE_ACTION::FENCE_ACTION_RTL :
            return "RTL (return to launch) mode";
    }
}

enum class FENCE_BREACH {
    FENCE_BREACH_NODE = 0,
    FENCE_BREACH_MINALT = 1,
    FENCE_BREACH_MAXALT = 2,
    FENCE_BREACH_BOUNDARY = 3
};

template<>
QString enum_cast<FENCE_BREACH>(FENCE_BREACH value) {
    switch(value) {
        case FENCE_BREACH::FENCE_BREACH_NODE :
            return "no last breach";
        case FENCE_BREACH::FENCE_BREACH_MINALT :
            return "breached min altitude";
        case FENCE_BREACH::FENCE_BREACH_MAXALT :
            return "breached max altitude";
        case FENCE_BREACH::FENCE_BREACH_BOUNDARY :
            return "breached boundary";
    }
}

enum class FENCE_MITIGATE {
    FENCE_MITIGATE_UNKNOWN = 0,
    FENCE_MITIGATE_NONE = 1,
    FENCE_MITIGATE_VEL_LIMIT = 2
};

template<>
QString enum_cast<FENCE_MITIGATE>(FENCE_MITIGATE value) {
    switch(value) {
        case FENCE_MITIGATE::FENCE_MITIGATE_UNKNOWN :
            return "unknown";
        case FENCE_MITIGATE::FENCE_MITIGATE_NONE :
            return "no actions being taken";
        case FENCE_MITIGATE::FENCE_MITIGATE_VEL_LIMIT :
            return "velocity limit is active to prevent breach";
    }
}

enum class MAV_MOUNT_MODE {
    MAV_MOUNT_MODE_RETRACT = 0,
    MAV_MOUNT_MODE_NEUTRAL = 1,
    MAV_MOUNT_MODE_MAVLINK_TARGETING = 2,
    MAV_MOUNT_MODE_RC_TARGETING = 3,
    MAV_MOUNT_MODE_GPS_POINT = 4,
    MAV_MOUNT_MODE_SYSID_TARGET = 5
};

template<>
QString enum_cast<MAV_MOUNT_MODE>(MAV_MOUNT_MODE value) {
    switch(value) {
        case MAV_MOUNT_MODE::MAV_MOUNT_MODE_RETRACT :
            return "load and keep safe position";
        case MAV_MOUNT_MODE::MAV_MOUNT_MODE_NEUTRAL :
            return "load and keep neutral position";
        case MAV_MOUNT_MODE::MAV_MOUNT_MODE_MAVLINK_TARGETING :
            return "load neutral position and start MAVLink control with stabilization";
        case MAV_MOUNT_MODE::MAV_MOUNT_MODE_RC_TARGETING :
            return "load neutral position and start RC control with stabilization";
        case MAV_MOUNT_MODE::MAV_MOUNT_MODE_GPS_POINT :
            return "load neutral position and start point to LatLonAlt";
        case MAV_MOUNT_MODE::MAV_MOUNT_MODE_SYSID_TARGET :
            return "gimbal tracks system with specified system ID";
    }
}

enum class GIMBAL_DEVICE_CAP_FLAGS {
    GIMBAL_DEVICE_CAP_FLAGS_HAS_RETRACT = 0x0001,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_NEUTRAL = 0x0002,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_ROLL_AXIS = 0x0004,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_ROLL_FOLLOW = 0x0008,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_ROLL_LOCK = 0x0010,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_PITCH_AXIS = 0x0020,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_PITCH_FOLLOW = 0x0040,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_PITCH_LOCK = 0x0080,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_YAW_AXIS = 0x0100,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_YAW_FOLLOW = 0x0200,
    GIMBAL_DEVICE_CAP_FLAGS_HAS_YAW_LOCK = 0x04000,
    GIMBAL_DEVICE_CAP_FLAGS_SUPPORTS_INFINITE_YAW = 0x0800
};

template<>
QString enum_cast<GIMBAL_DEVICE_CAP_FLAGS>(GIMBAL_DEVICE_CAP_FLAGS value) {
    switch(value) {
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_RETRACT :
			return "gimbal supports a retracted position";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_NEUTRAL :
            return "gimbal supports horizontal position";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_ROLL_AXIS :
            return "gimbal supports roll rotating";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_ROLL_FOLLOW :
            return "gimbal supports follow a roll angle";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_ROLL_LOCK :
            return "gimbal supports locking to a roll angle";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_PITCH_AXIS :
            return "gimbal supports pitch rotating";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_PITCH_FOLLOW :
            return "gimbal supports follow a pitch angle";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_PITCH_LOCK :
            return "gimbal supports locking to a pitch angle";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_YAW_AXIS :
            return "gimbal supports follow yaw rotating";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_YAW_FOLLOW :
            return "gimbal supports follow a yaw angle";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_HAS_YAW_LOCK :
            return "gimbal supports locking to a yaw angle";
    	case GIMBAL_DEVICE_CAP_FLAGS::GIMBAL_DEVICE_CAP_FLAGS_SUPPORTS_INFINITE_YAW :
            return "gimbal supports yawing/panning infinetely";
    }
}

enum class GIMBAL_MANAGER_CAP_FLAGS {
    GIMBAL_MANAGER_CAP_FLAGS_HAS_RETRACT = 0x00000001,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_NEUTRAL = 0x00000002,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_ROLL_AXIS = 0x00000004,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_ROLL_FOLLOW = 0x00000008,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_ROLL_LOCK = 0x00000010,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_AXIS = 0x00000020,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_FOLLOW = 0x00000040,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_LOCK = 0x00000080,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_AXIS = 0x00000100,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_FOLLOW = 0x00000200,
    GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_LOCK = 0x00000400,
    GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_INFINITE_YAW = 0x00000800,
    GIMBAL_MANAGER_CAP_FLAGS_CAN_POINT_LOCATION_LOCAL = 0x00010000,
    GIMBAL_MANAGER_CAP_FLAGS_CAN_POINT_LOCATION_GLOBAL = 0x00020000,
    GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_FOCAL_LENGTH_SCALE = 0x00100000,
    GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_NUDGING = 0x00200000,
    GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_OVERRIDE = 0x00400000
};

template<>
QString enum_cast<GIMBAL_MANAGER_CAP_FLAGS>(GIMBAL_MANAGER_CAP_FLAGS value) {
    switch(value) {
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_RETRACT :
            return "gimbal manager supports a retracted position";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_NEUTRAL :
            return "gimbal manager supports horizontal position";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_ROLL_AXIS :
            return "gimbal manager supports roll rotating";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_ROLL_FOLLOW :
            return "gimbal manager supports follow a roll angle";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_ROLL_LOCK :
            return "gimbal manager supports locking to a roll angle";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_AXIS :
            return "gimbal manager supports pitch rotating";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_FOLLOW :
            return "gimbal manager supports follow a pitch angle";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_PITCH_LOCK :
            return "gimbal manager supports locking to a pitch angle";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_AXIS :
            return "gimbal manager supports yaw rotating";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_FOLLOW :
            return "gimbal manager supports follow a yaw angle";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_HAS_YAW_LOCK :
            return "gimbal manager supports locking to a yaw angle";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_INFINITE_YAW :
            return "gimbal manager supports yawing/panning infinetely";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_CAN_POINT_LOCATION_LOCAL :
            return "gimbal manager supports to point to a local position";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_CAN_POINT_LOCATION_GLOBAL :
            return "gimbal manager supports to point to a global position";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_FOCAL_LENGTH_SCALE :
            return "gimbal manager supports pitching/yawing at an angular velocity scaled by focal length";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_NUDGING :
            return "gimbal manager supports nudging when pointing to a location or tracking";
        case GIMBAL_MANAGER_CAP_FLAGS::GIMBAL_MANAGER_CAP_FLAGS_SUPPORTS_OVERRIDE :
            return "gimbal manager supports overriding when pointing to a location or tracking";
    }
}

enum class GIMBAL_DEVICE_FLAGS {
    GIMBAL_DEVICE_FLAGS_RETRACT = 0x01,
    GIMBAL_DEVICE_FLAGS_NEUTRAL = 0x02,
    GIMBAL_DEVICE_FLAGS_ROLL_LOCK = 0x04,
    GIMBAL_DEVICE_FLAGS_PITCH_LOCK = 0x08,
    GIMBAL_DEVICE_FLAGS_YAW_LOCK = 0x10
};

template<>
QString enum_cast<GIMBAL_DEVICE_FLAGS>(GIMBAL_DEVICE_FLAGS value) {
    switch(value) {
        case GIMBAL_DEVICE_FLAGS::GIMBAL_DEVICE_FLAGS_RETRACT :
            return "set to retracted safe position";
        case GIMBAL_DEVICE_FLAGS::GIMBAL_DEVICE_FLAGS_NEUTRAL :
            return "set to neutral";
        case GIMBAL_DEVICE_FLAGS::GIMBAL_DEVICE_FLAGS_ROLL_LOCK :
            return "lock roll angle to absolute angle relative to horizon";
        case GIMBAL_DEVICE_FLAGS::GIMBAL_DEVICE_FLAGS_PITCH_LOCK :
            return "lock pitch angle to absolute angle relative to horizon";
        case GIMBAL_DEVICE_FLAGS::GIMBAL_DEVICE_FLAGS_YAW_LOCK :
            return "lock yaw angle to absolute angle relative to north";
    }
}

enum class GIMBAL_MANAGER_FLAGS {
    GIMBAL_MANAGER_FLAGS_RETRACT = 0x00000001,
    GIMBAL_MANAGER_FLAGS_NEUTRAL = 0x00000002,
    GIMBAL_MANAGER_FLAGS_ROLL_LOCK = 0x00000004,
    GIMBAL_MANAGER_FLAGS_PITCH_LOCK = 0x00000008,
    GIMBAL_MANAGER_FLAGS_YAW_LOCK = 0x00000010,
    GIMBAL_MANAGER_FLAGS_ANGULAR_VELOCITY_RELATIVE_TO_FOCAL_LENGTH = 0x00100000,
    GIMBAL_MANAGER_FLAGS_NUDGE = 0x00200000,
    GIMBAL_MANAGER_FLAGS_OVERRIDE = 0x00400000,
    GIMBAL_MANAGER_FLAGS_NONE = 0x00800000
};

template<>
QString enum_cast<GIMBAL_MANAGER_FLAGS>(GIMBAL_MANAGER_FLAGS value) {
    switch(value) {
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_RETRACT :
            return "set to safe position";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_NEUTRAL :
            return "set to neutral";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_ROLL_LOCK :
            return "lock roll angle to absolute angle relative to horizon";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_PITCH_LOCK :
            return "lock pitch angle to absolute angle relative to horizon";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_YAW_LOCK :
            return "lock yaw angle to absolute angle relative to north";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_ANGULAR_VELOCITY_RELATIVE_TO_FOCAL_LENGTH :
            return "scale angular velocity to focal length";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_NUDGE :
            return "interpret attitude control on top of pointing to a location/tracking";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_OVERRIDE :
            return "completely override pointing to a location/tracking";
        case GIMBAL_MANAGER_FLAGS::GIMBAL_MANAGER_FLAGS_NONE :
            return "gimbal manager remove all flags";
    }
}

enum class GIMBAL_DEVICE_ERROR_FLAGS {
    GIMBAL_DEVICE_ERROR_FLAGS_AT_ROLL_LIMIT = 0x0001,
    GIMBAL_DEVICE_ERROR_FLAGS_AT_PITCH_LIMIT = 0x0002,
    GIMBAL_DEVICE_ERROR_FLAGS_AT_YAW_LIMIT = 0x0004,
    GIMBAL_DEVICE_ERROR_FLAGS_ENCODER_ERROR = 0x0008,
    GIMBAL_DEVICE_ERROR_FLAGS_POWER_ERROR = 0x0010,
    GIMBAL_DEVICE_ERROR_FLAGS_MOTOR_ERROR = 0x0020,
    GIMBAL_DEVICE_ERROR_FLAGS_SOFTWARE_ERROR = 0x0040,
    GIMBAL_DEVICE_ERROR_FLAGS_COMMS_ERROR = 0x0080,
    GIMBAL_DEVICE_ERROR_FLAGS_CALIBRATION_RUNNING = 0x1000
};

template<>
QString enum_cast<GIMBAL_DEVICE_ERROR_FLAGS>(GIMBAL_DEVICE_ERROR_FLAGS value) {
    switch(value) {
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_AT_ROLL_LIMIT :
            return "limited by hardware roll limit";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_AT_PITCH_LIMIT :
            return "limited by hardware pitch limit";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_AT_YAW_LIMIT :
            return "limited by hardware yaw limit";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_ENCODER_ERROR :
            return "gimbal encoders failed";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_POWER_ERROR :
            return "gimbal power failed";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_MOTOR_ERROR :
            return "gimbal motor failed";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_SOFTWARE_ERROR :
            return "gimbal software failed";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_COMMS_ERROR :
            return "gimbal communication is down";
        case GIMBAL_DEVICE_ERROR_FLAGS::GIMBAL_DEVICE_ERROR_FLAGS_CALIBRATION_RUNNING :
            return "calibrating";
    }
}

enum class UAVCAN_NODE_HEALTH {
    UAVCAN_NODE_HEALTH_OK = 0,
    UAVCAN_NODE_HEALTH_WARNING = 1,
    UAVCAN_NODE_HEALTH_ERROR = 2,
    UAVCAN_NODE_HEALTH_CRITICAL = 3
};

template<>
QString enum_cast<UAVCAN_NODE_HEALTH>(UAVCAN_NODE_HEALTH value) {
    switch(value) {
       case UAVCAN_NODE_HEALTH::UAVCAN_NODE_HEALTH_OK :
            return "funtioning properly";
       case UAVCAN_NODE_HEALTH::UAVCAN_NODE_HEALTH_WARNING :
            return "minor failure";
       case UAVCAN_NODE_HEALTH::UAVCAN_NODE_HEALTH_ERROR :
            return "major failure";
       case UAVCAN_NODE_HEALTH::UAVCAN_NODE_HEALTH_CRITICAL :
            return "fatal malfunction";
    }
}

enum class UAVCAN_NODE_MODE {
    UAVCAN_NODE_MODE_OPERATIONAL = 0,
    UAVCAN_NODE_MODE_INITIALIZATION = 1,
    UAVCAN_NODE_MODE_MAINTENANCE = 2,
    UAVCAN_NODE_MODE_SOFTWARE_UPDATE = 3,
    UAVCAN_NODE_MODE_OFFLINE = 7
};

template<>
QString enum_cast<UAVCAN_NODE_MODE>(UAVCAN_NODE_MODE value) {
    switch(value) {
       case UAVCAN_NODE_MODE::UAVCAN_NODE_MODE_OPERATIONAL :
            return "operational";
       case UAVCAN_NODE_MODE::UAVCAN_NODE_MODE_INITIALIZATION :
            return "initializing";
       case UAVCAN_NODE_MODE::UAVCAN_NODE_MODE_MAINTENANCE :
            return "under maintenance";
       case UAVCAN_NODE_MODE::UAVCAN_NODE_MODE_SOFTWARE_UPDATE :
            return "updating software";
       case UAVCAN_NODE_MODE::UAVCAN_NODE_MODE_OFFLINE :
            return "offline";
    }
}

enum class ESC_CONNECTION_TYPE {
    ESC_CONNECTION_TYPE_PPM = 0,
    ESC_CONNECTION_TYPE_SERIAL = 1,
    ESC_CONNECTION_TYPE_ONESHOT = 2,
    ESC_CONNECTION_TYPE_I2C = 3,
    ESC_CONNECTION_TYPE_CAN = 4,
    ESC_CONNECTION_TYPE_DSHOT = 5
};

template<>
QString enum_cast<ESC_CONNECTION_TYPE>(ESC_CONNECTION_TYPE value) {
    switch(value) {
        case ESC_CONNECTION_TYPE::ESC_CONNECTION_TYPE_PPM :
            return "PPM";
        case ESC_CONNECTION_TYPE::ESC_CONNECTION_TYPE_SERIAL :
            return "serial bus connected";
        case ESC_CONNECTION_TYPE::ESC_CONNECTION_TYPE_ONESHOT :
            return "one shot PPM";
        case ESC_CONNECTION_TYPE::ESC_CONNECTION_TYPE_I2C :
            return "I2C";
        case ESC_CONNECTION_TYPE::ESC_CONNECTION_TYPE_CAN :
            return "CAN-Bus";
        case ESC_CONNECTION_TYPE::ESC_CONNECTION_TYPE_DSHOT :
            return "DShot";
    }
}

enum class ESC_FAILURE_FLAGS {
    ESC_FAILURE_NONE = 0x00,
    ESC_FAILURE_OVER_CURRENT = 0x01,
    ESC_FAILURE_OVER_VOLTAGE = 0x02,
    ESC_FAILURE_OVER_TEMPERATURE = 0x04,
    ESC_FAILURE_OVER_RPM = 0x08,
    ESC_FAILURE_INCONSISTENT_CMD = 0x10,
    ESC_FAILURE_MOTOR_STUCK = 0x20,
    ESC_FAILURE_GENERIC = 0x40
};

template<>
QString enum_cast<ESC_FAILURE_FLAGS>(ESC_FAILURE_FLAGS value) {
    switch(value) {
        case ESC_FAILURE_FLAGS::ESC_FAILURE_NONE :
            return "no failure";
        case ESC_FAILURE_FLAGS::ESC_FAILURE_OVER_CURRENT :
            return "over current failure";
        case ESC_FAILURE_FLAGS::ESC_FAILURE_OVER_VOLTAGE :
            return "over voltage failure";
        case ESC_FAILURE_FLAGS::ESC_FAILURE_OVER_TEMPERATURE :
            return "over temperature failure";
        case ESC_FAILURE_FLAGS::ESC_FAILURE_OVER_RPM :
            return "over RPM failure";
        case ESC_FAILURE_FLAGS::ESC_FAILURE_INCONSISTENT_CMD :
            return "inconsistent command failure";
        case ESC_FAILURE_FLAGS::ESC_FAILURE_MOTOR_STUCK :
            return "motor stuck";
        case ESC_FAILURE_FLAGS::ESC_FAILURE_GENERIC :
            return "generic failure";
    }
}

enum class STORAGE_STATUS {
    STORAGE_STATUS_EMPTY = 0,
    STORAGE_STATUS_UNFORMATTED = 1,
    STORAGE_STATUS_READY = 2,
    STORAGE_STATUS_NOT_SUPPORTED = 3
};

template<>
QString enum_cast<STORAGE_STATUS>(STORAGE_STATUS value) {
    switch(value) {
       case STORAGE_STATUS::STORAGE_STATUS_EMPTY :
            return "missing";
       case STORAGE_STATUS::STORAGE_STATUS_UNFORMATTED :
            return "unformatted";
       case STORAGE_STATUS::STORAGE_STATUS_READY :
            return "ready";
       case STORAGE_STATUS::STORAGE_STATUS_NOT_SUPPORTED :
            return "camera doesn\'t supply storage status information";
    }
}

enum class ORBIT_YAW_BEHAVIOUR {
    ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TO_CIRCLE_CENTER = 0,
    ORBIT_YAW_BEHAVIOUR_HOLD_INITIAL_HEADING = 1,
    ORBIT_YAW_BEHAVIOUR_UNCONTROLLED = 2,
    ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TANGENT_TO_CIRCLE = 3,
    ORBIT_YAW_BEHAVIOUR_RC_CONTROLLED = 4
};

template<>
QString enum_cast<ORBIT_YAW_BEHAVIOUR>(ORBIT_YAW_BEHAVIOUR value) {
    switch(value) {
       case ORBIT_YAW_BEHAVIOUR::ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TO_CIRCLE_CENTER :
            return "front holds to the center";
       case ORBIT_YAW_BEHAVIOUR::ORBIT_YAW_BEHAVIOUR_HOLD_INITIAL_HEADING :
            return "front holds heading when message received";
       case ORBIT_YAW_BEHAVIOUR::ORBIT_YAW_BEHAVIOUR_UNCONTROLLED :
            return "yaw uncontrolled";
       case ORBIT_YAW_BEHAVIOUR::ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TANGENT_TO_CIRCLE :
            return "front follows flight path";
       case ORBIT_YAW_BEHAVIOUR::ORBIT_YAW_BEHAVIOUR_RC_CONTROLLED :
            return "yaw controlled by RC input";
    }
}

enum class WIFI_CONFIG_AP_RESPONSE {
    WIFI_CONFIG_AP_RESPONSE_UNDEFINED = 0,
    WIFI_CONFIG_AP_RESPONSE_ACCEPTED = 1,
    WIFI_CONFIG_AP_RESPONSE_REJECTED = 2,
    WIFI_CONFIG_AP_RESPONSE_MODE_ERROR = 3,
    WIFI_CONFIG_AP_RESPONSE_SSID_ERROR = 4,
    WIFI_CONFIG_AP_RESPONSE_PASSWORD_ERROR = 5
};

template<>
QString enum_cast<WIFI_CONFIG_AP_RESPONSE>(WIFI_CONFIG_AP_RESPONSE value) {
    switch(value) {
       case WIFI_CONFIG_AP_RESPONSE::WIFI_CONFIG_AP_RESPONSE_UNDEFINED :
            return "undefined response";
       case WIFI_CONFIG_AP_RESPONSE::WIFI_CONFIG_AP_RESPONSE_ACCEPTED :
            return "changes accepted";
       case WIFI_CONFIG_AP_RESPONSE::WIFI_CONFIG_AP_RESPONSE_REJECTED :
            return "changes rejected";
       case WIFI_CONFIG_AP_RESPONSE::WIFI_CONFIG_AP_RESPONSE_MODE_ERROR :
            return "invalid mode";
       case WIFI_CONFIG_AP_RESPONSE::WIFI_CONFIG_AP_RESPONSE_SSID_ERROR :
            return "invalid SSID";
       case WIFI_CONFIG_AP_RESPONSE::WIFI_CONFIG_AP_RESPONSE_PASSWORD_ERROR :
            return "invalid password";
    }
}

enum class CELLULAR_CONFIG_RESPONSE {
    CELLULAR_CONFIG_RESPONSE_ACCEPTED = 0,
    CELLULAR_CONFIG_RESPONSE_APN_ERROR = 1,
    CELLULAR_CONFIG_RESPONSE_PIN_ERROR = 2,
    CELLULAR_CONFIG_RESPONSE_REJECTED = 3,
    CELLULAR_CONFIG_BLOCKED_PUK_REQUIRED = 4
};

template<>
QString enum_cast<CELLULAR_CONFIG_RESPONSE>(CELLULAR_CONFIG_RESPONSE value) {
    switch(value) {
       case CELLULAR_CONFIG_RESPONSE::CELLULAR_CONFIG_RESPONSE_ACCEPTED :
            return "changes accepted";
       case CELLULAR_CONFIG_RESPONSE::CELLULAR_CONFIG_RESPONSE_APN_ERROR :
            return "invalid APN";
       case CELLULAR_CONFIG_RESPONSE::CELLULAR_CONFIG_RESPONSE_PIN_ERROR :
            return "invalid PIN";
       case CELLULAR_CONFIG_RESPONSE::CELLULAR_CONFIG_RESPONSE_REJECTED :
            return "changes rejected";
       case CELLULAR_CONFIG_RESPONSE::CELLULAR_CONFIG_BLOCKED_PUK_REQUIRED :
            return "PUK is required to unblock SIM card";
    }
}

enum class WIFI_CONFIG_AP_MODE {
    WIFI_CONFIG_AP_MODE_UNDEFINED = 0,
    WIFI_CONFIG_AP_MODE_AP = 1,
    WIFI_CONFIG_AP_MODE_STATION = 2,
    WIFI_CONFIG_AP_MODE_DISABLED = 3
};

template<>
QString enum_cast<WIFI_CONFIG_AP_MODE>(WIFI_CONFIG_AP_MODE value) {
    switch(value) {
       case WIFI_CONFIG_AP_MODE::WIFI_CONFIG_AP_MODE_UNDEFINED :
            return "undefined mode";
       case WIFI_CONFIG_AP_MODE::WIFI_CONFIG_AP_MODE_AP :
            return "configured as access point";
       case WIFI_CONFIG_AP_MODE::WIFI_CONFIG_AP_MODE_STATION :
            return "configured as station connected to local network";
       case WIFI_CONFIG_AP_MODE::WIFI_CONFIG_AP_MODE_DISABLED :
            return "disabled";
    }
}

enum class COMP_METADATA_TYPE {
    COMP_METADATA_TYPE_VERSION = 0,
    COMP_METADATA_TYPE_PARAMETER = 1
};

template<>
QString enum_cast<COMP_METADATA_TYPE>(COMP_METADATA_TYPE value) {
    switch(value) {
       case COMP_METADATA_TYPE::COMP_METADATA_TYPE_VERSION :
            return "version inforamtion";
       case COMP_METADATA_TYPE::COMP_METADATA_TYPE_PARAMETER :
            return "parameter meta data";
    }
}

enum class PARAM_TRANSACTION_RESPONSE {
    PARAM_TRANSACTION_RESPONSE_ACCEPTED = 0,
    PARAM_TRANSACTION_RESPONSE_FAILED = 1,
    PARAM_TRANSACTION_RESPONSE_UNSUPPORTED = 2,
    PARAM_TRANSACTION_RESPONSE_INPROGRESS = 3
};

template<>
QString enum_cast<PARAM_TRANSACTION_RESPONSE>(PARAM_TRANSACTION_RESPONSE value) {
    switch(value) {
        case PARAM_TRANSACTION_RESPONSE::PARAM_TRANSACTION_RESPONSE_ACCEPTED :
            return "accepted";
        case PARAM_TRANSACTION_RESPONSE::PARAM_TRANSACTION_RESPONSE_FAILED :
            return "failed";
        case PARAM_TRANSACTION_RESPONSE::PARAM_TRANSACTION_RESPONSE_UNSUPPORTED :
            return "unsupported";
        case PARAM_TRANSACTION_RESPONSE::PARAM_TRANSACTION_RESPONSE_INPROGRESS :
            return "in progress";
    }
}

enum class PARAM_TRANSACTION_TRANSPORT {
    PARAM_TRANSACTION_TRANSPORT_PARAM = 0,
    PARAM_TRANSACTION_TRANSPORT_PARAM_EXT = 1
};

template<>
QString enum_cast<PARAM_TRANSACTION_TRANSPORT>(PARAM_TRANSACTION_TRANSPORT value) {
    switch(value) {
       case PARAM_TRANSACTION_TRANSPORT::PARAM_TRANSACTION_TRANSPORT_PARAM :
            return "transaction over param transport";
       case PARAM_TRANSACTION_TRANSPORT::PARAM_TRANSACTION_TRANSPORT_PARAM_EXT :
            return "transaction over param_ext transport";
    }
}

enum class PARAM_TRANSACTION_ACTION {
    PARAM_TRANSACTION_ACTION_COMMIT = 0,
    PARAM_TRANSACTION_ACTION_CANCEl = 1
};

template<>
QString enum_cast<PARAM_TRANSACTION_ACTION>(PARAM_TRANSACTION_ACTION value) {
    switch(value) {
       case PARAM_TRANSACTION_ACTION::PARAM_TRANSACTION_ACTION_COMMIT :
            return "commit current transaction";
       case PARAM_TRANSACTION_ACTION::PARAM_TRANSACTION_ACTION_CANCEl :
            return "cancel current transaction";
    }
}

enum class MAV_DATA_STREAM {
    MAV_DATA_STREAM_ALL = 0,
    MAV_DATA_STREAM_RAW_SENSORS = 1,
    MAV_DATA_STREAM_EXTENDED_STATUS = 2,
    MAV_DATA_STREAM_RC_CHANNELS = 3,
    MAV_DATA_STREAM_RAW_CONTROLLER = 4,
    MAV_DATA_STREAM_POSITION = 5,
    MAV_DATA_STREAM_EXTRA1 = 10,
    MAV_DATA_STREAM_EXTRA2 = 11,
    MAV_DATA_STREAM_EXTRA3 = 12
};

template<>
QString enum_cast<MAV_DATA_STREAM>(MAV_DATA_STREAM value) {
    switch(value) {
       case MAV_DATA_STREAM::MAV_DATA_STREAM_ALL :
            return "enable all data streams";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_RAW_SENSORS :
            return "enable IMU_RAW, GPS_RAW, GPS_STATUS packets";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_EXTENDED_STATUS :
            return "enable GPS_STATUS, CONTROL_STATUS, AUX_STATUS packets";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_RC_CHANNELS :
            return "enable RC_CHANNELS_SCALED, RC_CHANNELS_RAW, SERVO_OUTPUT_RAW packets";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_RAW_CONTROLLER :
            return "enable ATTITUDE_CONTROLLER_OUTPUT, POSITION_CONTROLLER_OUTPUT, NAV_CONTROLLER_OUTPUT packets";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_POSITION :
            return "enable LOCAL_POSITION, GLOBAL_POSITION/GLOBAL_POSITION_INT messages";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_EXTRA1 :
            return "autopilot extra messages 1";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_EXTRA2 :
            return "autopilot extra messages 2";
       case MAV_DATA_STREAM::MAV_DATA_STREAM_EXTRA3 :
            return "autopilot extra messages 3";
    }
}

enum class MAV_ROI {
    MAV_ROI_NONE = 0,
    MAV_ROI_WPNEXT = 1,
    MAV_ROI_WPINDEX = 2,
    MAV_ROI_LOCATION = 3,
    MAV_ROI_TARGET = 4
};

template<>
QString enum_cast<MAV_ROI>(MAV_ROI value) {
    switch(value) {
        case MAV_ROI::MAV_ROI_NONE :
            return "no region of interest";
        case MAV_ROI::MAV_ROI_WPNEXT :
            return "point toward next waypoint";
        case MAV_ROI::MAV_ROI_WPINDEX :
            return "point toward given waypoint";
        case MAV_ROI::MAV_ROI_LOCATION :
            return "point toward fixed location";
        case MAV_ROI::MAV_ROI_TARGET :
            return "point toward given id";
    }
}

enum class MAV_CMD_ACK {
    MAV_CMD_ACK_OK,
    MAV_CMD_ACK_ERR_FAIL,
    MAV_CMD_ACK_ERR_ACCESS_DENIED,
    MAV_CMD_ACK_ERR_NOT_SUPPORTED,
    MAV_CMD_ACK_ERR_COORDINATE_FRAME_NOT_SUPPORTED,
    MAV_CMD_ACK_ERR_COORDINATES_OUT_OF_RANGE,
    MAV_CMD_ACK_ERR_X_LAT_OUT_OF_RANGE,
    MAV_CMD_ACK_ERR_Y_LON_OUT_OF_RANGE,
    MAV_CMD_ACK_ERR_Z_ALT_OUT_OF_RANGE
};

template<>
QString enum_cast<MAV_CMD_ACK>(MAV_CMD_ACK value) {
    switch(value) {
        case MAV_CMD_ACK::MAV_CMD_ACK_OK :
            return "command/mission ok";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_FAIL :
            return "generic error";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_ACCESS_DENIED :
            return "command denied from this source";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_NOT_SUPPORTED :
            return "command/mission is not supported";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_COORDINATE_FRAME_NOT_SUPPORTED :
            return "coordinate frame of command/mission is not supported";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_COORDINATES_OUT_OF_RANGE :
            return "coordinates are out of range";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_X_LAT_OUT_OF_RANGE :
            return "x/latitude is out of range";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_Y_LON_OUT_OF_RANGE :
            return "y/longitude is out of range";
        case MAV_CMD_ACK::MAV_CMD_ACK_ERR_Z_ALT_OUT_OF_RANGE :
            return "z/altitude is out of range";
    }
}

enum class MAV_PARAM_TYPE {
    MAV_PARAM_TYPE_UINT8 = 1,
    MAV_PARAM_TYPE_INT8 = 2,
    MAV_PARAM_TYPE_UINT16 = 3,
    MAV_PARAM_TYPE_INT16 = 4,
    MAV_PARAM_TYPE_UINT32 = 5,
    MAV_PARAM_TYPE_INT32 = 6,
    MAV_PARAM_TYPE_UINT64 = 7,
    MAV_PARAM_TYPE_INT64 = 8,
    MAV_PARAM_TYPE_REAL32 = 9,
    MAV_PARAM_TYPE_REAL64 = 10
};

template<>
QString enum_cast<MAV_PARAM_TYPE>(MAV_PARAM_TYPE value) {
    switch(value) {
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_UINT8 :
            return "8-bit unsigned integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_INT8 :
            return "8-bit signed integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_UINT16 :
            return "16-bit unsigned integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_INT16 :
            return "16-bit signed integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_UINT32 :
            return "32-bit unsigned integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_INT32 :
            return "32-bit signed integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_UINT64 :
            return "64-bit unsigned integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_INT64 :
            return "64-bit signed integer";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_REAL32 :
            return "32-bit floating-point";
        case MAV_PARAM_TYPE::MAV_PARAM_TYPE_REAL64 :
            return "64-bit floating-point";
    }
}

enum class MAV_PARAM_EXT_TYPE {
    MAV_PARAM_EXT_TYPE_UINT8 = 1,
    MAV_PARAM_EXT_TYPE_INT8 = 2,
    MAV_PARAM_EXT_TYPE_UINT16 = 3,
    MAV_PARAM_EXT_TYPE_INT16 = 4,
    MAV_PARAM_EXT_TYPE_UINT32 = 5,
    MAV_PARAM_EXT_TYPE_INT32 = 6,
    MAV_PARAM_EXT_TYPE_UINT64 = 7,
    MAV_PARAM_EXT_TYPE_INT64 = 8,
    MAV_PARAM_EXT_TYPE_REAL32 = 9,
    MAV_PARAM_EXT_TYPE_REAL64 = 10,
    MAV_PARAM_EXT_TYPE_CUSTOM = 11
};

template<>
QString enum_cast<MAV_PARAM_EXT_TYPE>(MAV_PARAM_EXT_TYPE value) {
    switch(value) {
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_UINT8 :
            return "8-bit unsigned integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_INT8 :
            return "8-bit signed integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_UINT16 :
            return "16-bit unsigned integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_INT16 :
            return "16-bit signed integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_UINT32 :
            return "32-bit unsigned integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_INT32 :
            return "32-bit signed integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_UINT64 :
            return "64-bit unsigned integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_INT64 :
            return "64-bit signed integer";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_REAL32 :
            return "32-bit floating-point";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_REAL64 :
            return "64-bit floating-point";
        case MAV_PARAM_EXT_TYPE::MAV_PARAM_EXT_TYPE_CUSTOM :
            return "custom type";
    }
}

enum class MAV_RESULT {
    MAV_RESULT_ACCEPTED = 0,
    MAV_RESULT_TEMPORARILY_REJECTED = 1,
    MAV_RESULT_DENIED = 2,
    MAV_RESULT_UNSUPPORTED = 3,
    MAV_RESULT_FAILED = 4,
    MAV_RESULT_IN_PROGRESS = 5,
    MAV_RESULT_CANCELED = 6
};

template<>
QString enum_cast<MAV_RESULT>(MAV_RESULT value) {
    switch(value) {
        case MAV_RESULT::MAV_RESULT_ACCEPTED :
            return "accepted";
        case MAV_RESULT::MAV_RESULT_TEMPORARILY_REJECTED :
            return "rejected";
        case MAV_RESULT::MAV_RESULT_DENIED :
            return "denied";
        case MAV_RESULT::MAV_RESULT_UNSUPPORTED :
            return "unsupported";
        case MAV_RESULT::MAV_RESULT_FAILED :
            return "failed";
        case MAV_RESULT::MAV_RESULT_IN_PROGRESS :
            return "in progress";
        case MAV_RESULT::MAV_RESULT_CANCELED :
            return "canceled";
    }
}

enum class MAV_MISSION_RESULT {
    MAV_MISSION_ACCEPTED = 0,
    MAV_MISSION_ERROR = 1,
    MAV_MISSION_UNSUPPORTED_FRAME = 2,
    MAV_MISSION_UNSUPPORTED = 3,
    MAV_MISSION_NO_SPACE = 4,
    MAV_MISSION_INVALID = 5,
    MAV_MISSION_INVALID_PARAM1 = 6,
    MAV_MISSION_INVALID_PARAM2 = 7,
    MAV_MISSION_INVALID_PARAM3 = 8,
    MAV_MISSION_INVALID_PARAM4 = 9,
    MAV_MISSION_INVALID_PARAM5_X = 10,
    MAV_MISSION_INVALID_PARAM6_Y = 11,
    MAV_MISSION_INVALID_PARAM7 = 12,
    MAV_MISSION_INVALID_SEQUENCE = 13,
    MAV_MISSION_DENIED = 14,
    MAV_MISSION_OPERATION_CANCELLED = 15
};

template<>
QString enum_cast<MAV_MISSION_RESULT>(MAV_MISSION_RESULT value) {
    switch(value) {
       case MAV_MISSION_RESULT::MAV_MISSION_ACCEPTED :
            return "accepted";
       case MAV_MISSION_RESULT::MAV_MISSION_ERROR :
            return "generic error";
       case MAV_MISSION_RESULT::MAV_MISSION_UNSUPPORTED_FRAME :
            return "coordinate frame is unsupported";
       case MAV_MISSION_RESULT::MAV_MISSION_UNSUPPORTED :
            return "command is unspupported";
       case MAV_MISSION_RESULT::MAV_MISSION_NO_SPACE :
            return "no storage left";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID :
            return "invalid parameter(s)";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_PARAM1 :
            return "param 1 is invalid";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_PARAM2 :
            return "param 2 is invalid";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_PARAM3 :
            return "param 3 is invalid";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_PARAM4 :
            return "param 4 is invalid";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_PARAM5_X :
            return "x/param 5 is invalid";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_PARAM6_Y :
            return "y/param 6 is invalid";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_PARAM7 :
            return "z/param 7 is invalid";
       case MAV_MISSION_RESULT::MAV_MISSION_INVALID_SEQUENCE :
            return "item received out of sequence";
       case MAV_MISSION_RESULT::MAV_MISSION_DENIED :
            return "denied";
       case MAV_MISSION_RESULT::MAV_MISSION_OPERATION_CANCELLED :
            return "canceled";
    }
}

enum class MAV_SEVERITY {
    MAV_SEVERITY_EMERGENCY = 0,
    MAV_SEVERITY_ALERT = 1,
    MAV_SEVERITY_CRITICAL = 2,
    MAV_SEVERITY_ERROR = 3,
    MAV_SEVERITY_WARNING = 4,
    MAV_SEVERITY_NOTICE = 5,
    MAV_SEVERITY_INFO = 6,
    MAV_SEVERITY_DEBUG = 7
};

template<>
QString enum_cast<MAV_SEVERITY>(MAV_SEVERITY value) {
    switch(value) {
       case MAV_SEVERITY::MAV_SEVERITY_EMERGENCY :
            return "emergency";
       case MAV_SEVERITY::MAV_SEVERITY_ALERT :
            return "alert";
       case MAV_SEVERITY::MAV_SEVERITY_CRITICAL :
            return "critical";
       case MAV_SEVERITY::MAV_SEVERITY_ERROR :
            return "error";
       case MAV_SEVERITY::MAV_SEVERITY_WARNING :
            return "warning";
       case MAV_SEVERITY::MAV_SEVERITY_NOTICE :
            return "notice";
       case MAV_SEVERITY::MAV_SEVERITY_INFO :
            return "info";
       case MAV_SEVERITY::MAV_SEVERITY_DEBUG :
            return "debug";
    }
}

enum class MAV_POWER_STATUS {
    MAV_POWER_STATUS_BRICK_VALID = 0x01,
    MAV_POWER_STATUS_SERVO_VALID = 0x02,
    MAV_POWER_STATUS_USB_CONNECTED = 0x04,
    MAV_POWER_STATUS_PERIPH_OVERCURRENT = 0x08,
    MAV_POWER_STATUS_PERIPH_HIPOWER_OVERCURRENT = 0x10,
    MAV_POWER_STATUS_STATUS_CHANGED = 0x20
};

template<>
QString enum_cast<MAV_POWER_STATUS>(MAV_POWER_STATUS value) {
    switch(value) {
       case MAV_POWER_STATUS::MAV_POWER_STATUS_BRICK_VALID :
            return "main power is valid";
       case MAV_POWER_STATUS::MAV_POWER_STATUS_SERVO_VALID :
            return "main servo power is valid";
       case MAV_POWER_STATUS::MAV_POWER_STATUS_USB_CONNECTED :
            return "USB power is connected";
       case MAV_POWER_STATUS::MAV_POWER_STATUS_PERIPH_OVERCURRENT :
            return "peripheral supply is in over-current state";
       case MAV_POWER_STATUS::MAV_POWER_STATUS_PERIPH_HIPOWER_OVERCURRENT :
            return "hi-power peripheral supply is in over-current state";
       case MAV_POWER_STATUS::MAV_POWER_STATUS_STATUS_CHANGED :
            return "poer status has changed";
    }
}

enum class SERIAL_CONTROL_DEV {
    SERIAL_CONTROL_DEV_TELEM1 = 0,
    SERIAL_CONTROL_DEV_TELEM2 = 1,
    SERIAL_CONTROL_DEV_GPS1 = 2,
    SERIAL_CONTROL_DEV_GPS2 = 3,
    SERIAL_CONTROL_DEV_SHELL = 10,
    SERIAL_CONTROL_SERIAL0 = 100,
    SERIAL_CONTROL_SERIAL1 = 101,
    SERIAL_CONTROL_SERIAL2 = 102,
    SERIAL_CONTROL_SERIAL3 = 103,
    SERIAL_CONTROL_SERIAL4 = 104,
    SERIAL_CONTROL_SERIAL5 = 105,
    SERIAL_CONTROL_SERIAL6 = 106,
    SERIAL_CONTROL_SERIAL7 = 107,
    SERIAL_CONTROL_SERIAL8 = 108,
    SERIAL_CONTROL_SERIAL9 = 109
};

template<>
QString enum_cast<SERIAL_CONTROL_DEV>(SERIAL_CONTROL_DEV value) {
    switch(value) {
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_DEV_TELEM1 :
            return "telemetry port 1";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_DEV_TELEM2 :
            return "telemetry port 2";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_DEV_GPS1 :
            return "GPS port 1";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_DEV_GPS2 :
            return "GPS port 2";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_DEV_SHELL :
            return "system shell";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL0 :
            return "serial 0";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL1 :
            return "serial 1";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL2 :
            return "serial 2";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL3 :
            return "serial 3";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL4 :
            return "serial 4";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL5 :
            return "serial 5";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL6 :
            return "serial 6";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL7 :
            return "serial 7";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL8 :
            return "serial 8";
       case SERIAL_CONTROL_DEV::SERIAL_CONTROL_SERIAL9 :
            return "serial 9";
    }
}

enum class SERIAL_CONTROL_FLAG {
    SERIAL_CONTROL_FLAG_REPLY = 0x01,
    SERIAL_CONTROL_FLAG_RESPOND = 0x02,
    SERIAL_CONTROL_FLAG_EXCLUSIVE = 0x04,
    SERIAL_CONTROL_FLAG_BLOCKING = 0x08,
    SERIAL_CONTROL_FLAG_MULTI = 0x10
};

template<>
QString enum_cast<SERIAL_CONTROL_FLAG>(SERIAL_CONTROL_FLAG value) {
    switch(value) {
       case SERIAL_CONTROL_FLAG::SERIAL_CONTROL_FLAG_REPLY :
            return "reply";
       case SERIAL_CONTROL_FLAG::SERIAL_CONTROL_FLAG_RESPOND :
            return "response";
       case SERIAL_CONTROL_FLAG::SERIAL_CONTROL_FLAG_EXCLUSIVE :
            return "exclusive";
       case SERIAL_CONTROL_FLAG::SERIAL_CONTROL_FLAG_BLOCKING :
            return "block";
       case SERIAL_CONTROL_FLAG::SERIAL_CONTROL_FLAG_MULTI :
            return "multiple replies";
    }
}

enum class MAV_DISTANCE_SENSOR {
    MAV_DISTANCE_SENSOR_LASER = 0,
    MAV_DISTANCE_SENSOR_ULTRASOUND = 1,
    MAV_DISTANCE_SENSOR_INFRARED = 2,
    MAV_DISTANCE_SENSOR_RADAR = 3,
    MAV_DISTANCE_SENSOR_UNKNOWN = 4
};

template<>
QString enum_cast<MAV_DISTANCE_SENSOR>(MAV_DISTANCE_SENSOR value) {
    switch(value) {
        case MAV_DISTANCE_SENSOR::MAV_DISTANCE_SENSOR_LASER :
            return "laser";
        case MAV_DISTANCE_SENSOR::MAV_DISTANCE_SENSOR_ULTRASOUND :
            return "ultrasound";
        case MAV_DISTANCE_SENSOR::MAV_DISTANCE_SENSOR_INFRARED :
            return "infrared";
        case MAV_DISTANCE_SENSOR::MAV_DISTANCE_SENSOR_RADAR :
            return "radar";
        case MAV_DISTANCE_SENSOR::MAV_DISTANCE_SENSOR_UNKNOWN :
            return "unknown/broken";
    }
}

enum class MAV_SENSOR_ORIENTATION {
    MAV_SENSOR_ROTATION_NONE = 0,
    MAV_SENSOR_ROTATION_YAW_45 = 1,
    MAV_SENSOR_ROTATION_YAW_90 = 2,
    MAV_SENSOR_ROTATION_YAW_135 = 3,
    MAV_SENSOR_ROTATION_YAW_180 = 4,
    MAV_SENSOR_ROTATION_YAW_225 = 5,
    MAV_SENSOR_ROTATION_YAW_270 = 6,
    MAV_SENSOR_ROTATION_YAW_315 = 7,
    MAV_SENSOR_ROTATION_ROLL_180 = 8,
    MAV_SENSOR_ROTATION_ROLL_180_YAW_45 = 9,
    MAV_SENSOR_ROTATION_ROLL_180_YAW_90 = 10,
    MAV_SENSOR_ROTATION_ROLL_180_YAW_135 = 11,
    MAV_SENSOR_ROTATION_PITCH_180 = 12,
    MAV_SENSOR_ROTATION_ROLL_180_YAW_225 = 13,
    MAV_SENSOR_ROTATION_ROLL_180_YAW_270 = 14,
    MAV_SENSOR_ROTATION_ROLL_180_YAW_315 = 15,
    MAV_SENSOR_ROTATION_ROLL_90 = 16,
    MAV_SENSOR_ROTATION_ROLL_90_YAW_45 = 17,
    MAV_SENSOR_ROTATION_ROLL_90_YAW_90 = 18,
    MAV_SENSOR_ROTATION_ROLL_90_YAW_135 = 19,
    MAV_SENSOR_ROTATION_ROLL_270 = 20,
    MAV_SENSOR_ROTATION_ROLL_270_YAW_45 = 21,
    MAV_SENSOR_ROTATION_ROLL_270_YAW_90 = 22,
    MAV_SENSOR_ROTATION_ROLL_270_YAW_135 = 23,
    MAV_SENSOR_ROTATION_PITCH_90 = 24,
    MAV_SENSOR_ROTATION_PITCH_270 = 25,
    MAV_SENSOR_ROTATION_PITCH_180_YAW_90 = 26,
    MAV_SENSOR_ROTATION_PITCH_180_YAW_270 = 27,
    MAV_SENSOR_ROTATION_ROLL_90_PITCH_90 = 28,
    MAV_SENSOR_ROTATION_ROLL_180_PITCH_90 = 29,
    MAV_SENSOR_ROTATION_ROLL_270_PITCH_90 = 30,
    MAV_SENSOR_ROTATION_ROLL_90_PITCH_180 = 31,
    MAV_SENSOR_ROTATION_ROLL_270_PITCH_180 = 32,
    MAV_SENSOR_ROTATION_ROLL_90_PITCH_270 = 33,
    MAV_SENSOR_ROTATION_ROLL_180_PITCH_270 = 34,
    MAV_SENSOR_ROTATION_ROLL_270_PITCH_270 = 35,
    MAV_SENSOR_ROTATION_ROLL_90_PITCH_180_YAW_90 = 36,
    MAV_SENSOR_ROTATION_ROLL_90_YAW_270 = 37,
    MAV_SENSOR_ROTATION_ROLL_90_PITCH_68_YAW_293 = 38,
    MAV_SENSOR_ROTATION_PITCH_315 = 39,
    MAV_SENSOR_ROTATION_ROLL_90_PITCH_315 = 40,
    MAV_SENSOR_ROTATION_ROLL_270_YAW_180 = 41,
    MAV_SENSOR_ROTATION_CUSTOM = 100
};

template<>
QString enum_cast<MAV_SENSOR_ORIENTATION>(MAV_SENSOR_ORIENTATION value) {
    switch(value) {
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_NONE :
            return "Roll: 0, Pitch: 0, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_YAW_45 :
            return "Roll: 0, Pitch: 0, Yaw: 45";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_YAW_90 :
            return "Roll: 0, Pitch: 0, Yaw: 90";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_YAW_135 :
            return "Roll: 0, Pitch: 0, Yaw: 135";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_YAW_180 :
            return "Roll: 0, Pitch: 0, Yaw: 180";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_YAW_225 :
            return "Roll: 0, Pitch: 0, Yaw: 225";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_YAW_270 :
            return "Roll: 0, Pitch: 0, Yaw: 270";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_YAW_315 :
            return "Roll: 0, Pitch: 0, Yaw: 315";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180 :
            return "Roll: 180, Pitch: 0, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_YAW_45 :
            return "Roll: 180, Pitch: 0, Yaw: 45";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_YAW_90 :
            return "Roll: 180, Pitch: 0, Yaw: 90";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_YAW_135 :
            return "Roll: 180, Pitch: 0, Yaw: 135";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_PITCH_180 :
            return "Roll: 0, Pitch: 180, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_YAW_225 :
            return "Roll: 180, Pitch: 0, Yaw: 225";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_YAW_270 :
            return "Roll: 180, Pitch: 0, Yaw: 270";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_YAW_315 :
            return "Roll: 180, Pitch: 0, Yaw: 315";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90 :
            return "Roll: 90, Pitch: 0, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_YAW_45 :
            return "Roll: 90, Pitch: 0, Yaw: 45";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_YAW_90 :
            return "Roll: 90, Pitch: 0, Yaw: 90";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_YAW_135 :
            return "Roll: 90, Pitch: 0, Yaw: 135";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270 :
            return "Roll: 270, Pitch: 0, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270_YAW_45 :
            return "Roll: 270, Pitch: 0, Yaw: 45";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270_YAW_90 :
            return "Roll: 270, Pitch: 0, Yaw: 90";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270_YAW_135 :
            return "Roll: 270, Pitch: 0, Yaw: 135";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_PITCH_90 :
            return "Roll: 0, Pitch: 90, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_PITCH_270 :
            return "Roll: 0, Pitch: 270, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_PITCH_180_YAW_90 :
            return "Roll: 0, Pitch: 180, Yaw: 90";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_PITCH_180_YAW_270 :
            return "Roll: 0, Pitch: 180, Yaw: 270";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_PITCH_90 :
            return "Roll: 90, Pitch: 90, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_PITCH_90 :
            return "Roll: 180, Pitch: 90, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270_PITCH_90 :
            return "Roll: 270, Pitch: 90, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_PITCH_180 :
            return "Roll: 90, Pitch: 180, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270_PITCH_180 :
            return "Roll: 270, Pitch: 180, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_PITCH_270 :
            return "Roll: 90, Pitch: 270, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_180_PITCH_270 :
            return "Roll: 180, Pitch: 270, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270_PITCH_270 :
            return "Roll: 270, Pitch: 270, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_PITCH_180_YAW_90 :
            return "Roll: 90, Pitch: 180, Yaw: 90";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_YAW_270 :
            return "Roll: 90, Pitch: 0, Yaw: 270";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_PITCH_68_YAW_293 :
            return "Roll: 90, Pitch: 68, Yaw: 293";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_PITCH_315 :
            return "Roll: 0, Pitch: 315, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_90_PITCH_315 :
            return "Roll: 90, Pitch: 315, Yaw: 0";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_ROLL_270_YAW_180 :
            return "Roll: 270, Pitch: 0, Yaw: 180";
        case MAV_SENSOR_ORIENTATION::MAV_SENSOR_ROTATION_CUSTOM :
            return "custom";
    }
}

enum class MAV_PROTOCOL_CAPABILITY {
    MAV_PROTOCOL_CAPABILITY_MISSION_FLOAT = 0x00000001,
    MAV_PROTOCOL_CAPABILITY_PARAM_FLOAT = 0x00000002,
    MAV_PROTOCOL_CAPABILITY_MISSION_INT = 0x00000004,
    MAV_PROTOCOL_CAPABILITY_COMMAND_INT = 0x00000008,
    MAV_PROTOCOL_CAPABILITY_PARAM_UNION = 0x00000010,
    MAV_PROTOCOL_CAPABILITY_FTP = 0x00000020,
    MAV_PROTOCOL_CAPABILITY_SET_ATTITUDE_TARGET = 0x00000040,
    MAV_PROTOCOL_CAPABILITY_SET_POSITION_TARGET_LOCAL_NED = 0x00000080,
    MAV_PROTOCOL_CAPABILITY_SET_POSITION_TARGET_GLOBAL_INT = 0x00000100,
    MAV_PROTOCOL_CAPABILITY_TERRAIN = 0x00000200,
    MAV_PROTOCOL_CAPABILITY_SET_ACTUATOR_TARGET = 0x00000400,
    MAV_PROTOCOL_CAPABILITY_FLIGHT_TERMINATION = 0x00000800,
    MAV_PROTOCOL_CAPABILITY_COMPASS_CALIBRATION = 0x00001000,
    MAV_PROTOCOL_CAPABILITY_MAVLINK2 = 0x00002000,
    MAV_PROTOCOL_CAPABILITY_MISSION_FENCE = 0x00004000,
    MAV_PROTOCOL_CAPABILITY_MISSION_RALLY = 0x00008000,
    MAV_PROTOCOL_CAPABILITY_FLIGHT_INFORMATION = 0x00010000
};

template<>
QString enum_cast<MAV_PROTOCOL_CAPABILITY>(MAV_PROTOCOL_CAPABILITY value) {
    switch(value) {
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_MISSION_FLOAT :
            return "MISSION float message";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_PARAM_FLOAT :
            return "param float message";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_MISSION_INT :
            return "MISSION_ITEM_INT caled integer message";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_COMMAND_INT :
            return "COMMAND_INT scaled integer message";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_PARAM_UNION :
            return "param union message";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_FTP :
            return "FILE_TRANSFER_PROTOCOL message";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_SET_ATTITUDE_TARGET :
            return "commanding attitude offboard";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_SET_POSITION_TARGET_LOCAL_NED :
            return "commanding position and velocity targets in local NED frame";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_SET_POSITION_TARGET_GLOBAL_INT :
            return "commanding position and velocity targets in global scaled integers";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_TERRAIN :
            return "terrain protocol/data";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_SET_ACTUATOR_TARGET :
            return "direct actuator control";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_FLIGHT_TERMINATION :
            return "flight termination";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_COMPASS_CALIBRATION :
            return "compass calibration";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_MAVLINK2 :
            return "MAVLink version 2";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_MISSION_FENCE :
            return "mission fence protocol ";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_MISSION_RALLY :
            return "mission rally point protocol";
        case MAV_PROTOCOL_CAPABILITY::MAV_PROTOCOL_CAPABILITY_FLIGHT_INFORMATION :
            return "flight information protocol";
    }
}

enum class MAV_ESTIMATOR_TYPE {
    MAV_ESTIMATOR_TYPE_UNKNOWN = 0,
    MAV_ESTIMATOR_TYPE_NAIVE = 1,
    MAV_ESTIMATOR_TYPE_VISION = 2,
    MAV_ESTIMATOR_TYPE_VIO = 3,
    MAV_ESTIMATOR_TYPE_GPS = 4,
    MAV_ESTIMATOR_TYPE_GPS_INS = 5,
    MAV_ESTIMATOR_TYPE_MOCAP = 6,
    MAV_ESTIMATOR_TYPE_LIDAR = 7,
    MAV_ESTIMATOR_TYPE_AUTOPILOT = 8
};

template<>
QString enum_cast<MAV_ESTIMATOR_TYPE>(MAV_ESTIMATOR_TYPE value) {
    switch(value) {
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_UNKNOWN :
            return "unknown";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_NAIVE :
            return "naive";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_VISION :
            return "computer vision based";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_VIO :
            return "visual-inertial based";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_GPS :
            return "plain GPS based";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_GPS_INS :
            return "GPS and inertial sensing based";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_MOCAP :
            return "external motion caturing (MOCAP) system based";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_LIDAR :
            return "lidar sensor based";
        case MAV_ESTIMATOR_TYPE::MAV_ESTIMATOR_TYPE_AUTOPILOT :
            return "on autopilot";
    }
}

enum class MAV_BATTERY_TYPE {
    MAV_BATTERY_TYPE_UNKNOWN = 0,
    MAV_BATTERY_TYPE_LIPO = 1,
    MAV_BATTERY_TYPE_LIFE = 2,
    MAV_BATTERY_TYPE_LION = 3,
    MAV_BATTERY_TYPE_NIMH = 4
};

template<>
QString enum_cast<MAV_BATTERY_TYPE>(MAV_BATTERY_TYPE value) {
    switch(value) {
       case MAV_BATTERY_TYPE::MAV_BATTERY_TYPE_UNKNOWN :
            return "unknown";
       case MAV_BATTERY_TYPE::MAV_BATTERY_TYPE_LIPO :
            return "lithium polymer";
       case MAV_BATTERY_TYPE::MAV_BATTERY_TYPE_LIFE :
            return "lithium iron phosphate";
       case MAV_BATTERY_TYPE::MAV_BATTERY_TYPE_LION :
            return "lithium ion";
       case MAV_BATTERY_TYPE::MAV_BATTERY_TYPE_NIMH :
            return "nickel metal hydride";
    }
}

enum class MAV_BATTERY_FUNCTION {
    MAV_BATTERY_FUNCTION_UNKNOWN = 0,
    MAV_BATTERY_FUNCTION_ALL = 1,
    MAV_BATTERY_FUNCTION_PROPULSION = 2,
    MAV_BATTERY_FUNCTION_AVIONICS = 3,
    MAV_BATTERY_FUNCTION_PAYLOAD = 4
};

template<>
QString enum_cast<MAV_BATTERY_FUNCTION>(MAV_BATTERY_FUNCTION value) {
    switch(value) {
        case MAV_BATTERY_FUNCTION::MAV_BATTERY_FUNCTION_UNKNOWN :
            return "unknown";
        case MAV_BATTERY_FUNCTION::MAV_BATTERY_FUNCTION_ALL :
            return "all flight systems";
        case MAV_BATTERY_FUNCTION::MAV_BATTERY_FUNCTION_PROPULSION :
            return "propulsion system";
        case MAV_BATTERY_FUNCTION::MAV_BATTERY_FUNCTION_AVIONICS :
            return "aviaonics";
        case MAV_BATTERY_FUNCTION::MAV_BATTERY_FUNCTION_PAYLOAD :
            return "payload";
    }
}

enum class MAV_BATTERY_CHARGE_STATE {
    MAV_BATTERY_CHARGE_STATE_UNDEFINED = 0,
    MAV_BATTERY_CHARGE_STATE_OK = 1,
    MAV_BATTERY_CHARGE_STATE_LOW = 2,
    MAV_BATTERY_CHARGE_STATE_CRITICAL = 3,
    MAV_BATTERY_CHARGE_STATE_EMERGENCY = 4,
    MAV_BATTERY_CHARGE_STATE_FAILED = 5,
    MAV_BATTERY_CHARGE_STATE_UNHEALTHY = 6,
    MAV_BATTERY_CHARGE_STATE_CHARGING = 7
};

template<>
QString enum_cast<MAV_BATTERY_CHARGE_STATE>(MAV_BATTERY_CHARGE_STATE value) {
    switch(value) {
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_UNDEFINED :
            return "unknown";
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_OK :
            return "normal";
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_LOW :
            return "low";
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_CRITICAL :
            return "critical";
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_EMERGENCY :
            return "emergency";
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_FAILED :
            return "failed";
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_UNHEALTHY :
            return "defective";
        case MAV_BATTERY_CHARGE_STATE::MAV_BATTERY_CHARGE_STATE_CHARGING :
            return "charging";
    }
}

enum class MAV_SMART_BATTERY_FAULT {
    MAV_SMART_BATTERY_FAULT_DEEP_DISCHARGE = 0x01,
    MAV_SMART_BATTERY_FAULT_SPIKES = 0x02,
    MAV_SMART_BATTERY_FAULT_SINGLE_CELL_FAIL = 0x04,
    MAV_SMART_BATTERY_FAULT_OVER_CURRENT = 0x08,
    MAV_SMART_BATTERY_FAULT_OVER_TEMPERATURE = 0x10,
    MAV_SMART_BATTERY_FAULT_UNDER_TEMPERATURE = 0x20
};

template<>
QString enum_cast<MAV_SMART_BATTERY_FAULT>(MAV_SMART_BATTERY_FAULT value) {
    switch(value) {
        case MAV_SMART_BATTERY_FAULT::MAV_SMART_BATTERY_FAULT_DEEP_DISCHARGE :
            return "has deep discharged";
        case MAV_SMART_BATTERY_FAULT::MAV_SMART_BATTERY_FAULT_SPIKES :
            return "volatge spikes";
        case MAV_SMART_BATTERY_FAULT::MAV_SMART_BATTERY_FAULT_SINGLE_CELL_FAIL :
            return "single cell has failed";
        case MAV_SMART_BATTERY_FAULT::MAV_SMART_BATTERY_FAULT_OVER_CURRENT :
            return "over-current fault";
        case MAV_SMART_BATTERY_FAULT::MAV_SMART_BATTERY_FAULT_OVER_TEMPERATURE :
            return "over-temperature fault";
        case MAV_SMART_BATTERY_FAULT::MAV_SMART_BATTERY_FAULT_UNDER_TEMPERATURE :
            return "under-temperature fault";
    }
}

enum class MAV_GENERATOR_STATUS_FLAG {
    MAV_GENERATOR_STATUS_FLAG_OFF = 0x00000001,
    MAV_GENERATOR_STATUS_FLAG_READY = 0x00000002,
    MAV_GENERATOR_STATUS_FLAG_GENERATING = 0x00000004,
    MAV_GENERATOR_STATUS_FLAG_CHARGING = 0x00000008,
    MAV_GENERATOR_STATUS_FLAG_REDUCED_POWER = 0x00000010,
    MAV_GENERATOR_STATUS_FLAG_MAXPOWER = 0x00000020,
    MAV_GENERATOR_STATUS_FLAG_OVERTEMP_WARNING = 0x00000040,
    MAV_GENERATOR_STATUS_FLAG_OVERTEMP_FAULT = 0x00000080,
    MAV_GENERATOR_STATUS_FLAG_ELECTRONICS_OVERTEMP_WARNING = 0x00000100,
    MAV_GENERATOR_STATUS_FLAG_ELECTRONICS_OVERTEMP_FAULT = 0x00000200,
    MAV_GENERATOR_STATUS_FLAG_ELECTRONICS_FAULT = 0x00000400,
    MAV_GENERATOR_STATUS_FLAG_POWERSOURCE_FAULT = 0x00000800,
    MAV_GENERATOR_STATUS_FLAG_COMMUNICATION_WARNING = 0x00001000,
    MAV_GENERATOR_STATUS_FLAG_COOLING_WARNING = 0x00002000,
    MAV_GENERATOR_STATUS_FLAG_POWER_RAIL_FAULT = 0x00004000,
    MAV_GENERATOR_STATUS_FLAG_OVERCURRENT_FAULT = 0x00008000,
    MAV_GENERATOR_STATUS_FLAG_BATTERY_OVERCHARGE_CURRENT_FAULT = 0x00010000,
    MAV_GENERATOR_STATUS_FLAG_OVERVOLTAGE_FAULT = 0x00020000,
    MAV_GENERATOR_STATUS_FLAG_BATTERY_UNDERVOLT_FAULT = 0x00040000,
    MAV_GENERATOR_STATUS_FLAG_START_INHIBITED = 0x00080000,
    MAV_GENERATOR_STATUS_FLAG_MAINTENANCE_REQUIRED = 0x00100000,
    MAV_GENERATOR_STATUS_FLAG_WARMING_UP = 0x00200000,
    MAV_GENERATOR_STATUS_FLAG_IDLE = 0x00400000,
};

template<>
QString enum_cast<MAV_GENERATOR_STATUS_FLAG>(MAV_GENERATOR_STATUS_FLAG value) {
    switch(value) {
        case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_OFF :
            return "off";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_READY :
            return "ready";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_GENERATING :
            return "generating";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_CHARGING :
            return "charging batteries";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_REDUCED_POWER :
            return "operating at reduced max power";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_MAXPOWER :
            return "providing max output";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_OVERTEMP_WARNING :
            return "near max operating temperature";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_OVERTEMP_FAULT :
            return "hit max operating temperature";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_ELECTRONICS_OVERTEMP_WARNING :
            return "electronics are near max operating temperature";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_ELECTRONICS_OVERTEMP_FAULT :
            return "electronics hit max operating temperature";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_ELECTRONICS_FAULT :
            return "electornics failed and shutdown";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_POWERSOURCE_FAULT :
            return "power source failed";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_COMMUNICATION_WARNING :
            return "controller has communication problems";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_COOLING_WARNING :
            return "cooling system failed";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_POWER_RAIL_FAULT :
            return "controller power rail failed";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_OVERCURRENT_FAULT :
            return "controller overcurrent and shutdown";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_BATTERY_OVERCHARGE_CURRENT_FAULT :
            return "charging high current and shutdown";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_OVERVOLTAGE_FAULT :
            return "controller overvoltage and shutdown";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_BATTERY_UNDERVOLT_FAULT :
            return "batteries under voltage";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_START_INHIBITED :
            return "start is inhibited";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_MAINTENANCE_REQUIRED :
            return "requires maintenance";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_WARMING_UP :
            return "not ready";
       case MAV_GENERATOR_STATUS_FLAG::MAV_GENERATOR_STATUS_FLAG_IDLE :
            return "idle";
    }
}

enum class MAV_VTOL_STATE {
    MAV_VTOL_STATE_UNDEFINED = 0,
    MAV_VTOL_STATE_TRANSITION_TO_FW = 1,
    MAV_VTOL_STATE_TRANSITION_TO_MC = 2,
    MAV_VTOL_STATE_MC = 3,
    MAV_VTOL_STATE_FW = 4
};

template<>
QString enum_cast<MAV_VTOL_STATE>(MAV_VTOL_STATE value) {
    switch(value) {
        case MAV_VTOL_STATE::MAV_VTOL_STATE_UNDEFINED :
            return "not configured as VTOL";
        case MAV_VTOL_STATE::MAV_VTOL_STATE_TRANSITION_TO_FW :
            return "transition from multicopter to fixed-wing";
        case MAV_VTOL_STATE::MAV_VTOL_STATE_TRANSITION_TO_MC :
            return "transition from fixed-wing to multicopter";
        case MAV_VTOL_STATE::MAV_VTOL_STATE_MC :
            return "multicopter state";
        case MAV_VTOL_STATE::MAV_VTOL_STATE_FW :
            return "fixed-wing state";
    }
}

enum class MAV_LANDED_STATE {
    MAV_LANDED_STATE_UNDEFINED = 0,
    MAV_LANDED_STATE_ON_GROUND = 1,
    MAV_LANDED_STATE_IN_AIR = 2,
    MAV_LANDED_STATE_TAKEOFF = 3,
    MAV_LANDED_STATE_LANDING = 4
};

template<>
QString enum_cast<MAV_LANDED_STATE>(MAV_LANDED_STATE value) {
    switch(value) {
       case MAV_LANDED_STATE::MAV_LANDED_STATE_UNDEFINED :
            return "unknown";
       case MAV_LANDED_STATE::MAV_LANDED_STATE_ON_GROUND :
            return "landed";
       case MAV_LANDED_STATE::MAV_LANDED_STATE_IN_AIR :
            return "in air";
       case MAV_LANDED_STATE::MAV_LANDED_STATE_TAKEOFF :
            return "taking off";
       case MAV_LANDED_STATE::MAV_LANDED_STATE_LANDING :
            return "landing";
    }
}

enum class ADSB_EMITTER_TYPE {
    ADSB_EMITTER_TYPE_NO_INFO = 0,
    ADSB_EMITTER_TYPE_LIGHT = 1,
    ADSB_EMITTER_TYPE_SMALL = 2,
    ADSB_EMITTER_TYPE_LARGE = 3,
    ADSB_EMITTER_TYPE_HIGH_VORTEX_LARGE = 4,
    ADSB_EMITTER_TYPE_HEAVY = 5,
    ADSB_EMITTER_TYPE_HIGHLY_MANUV = 6,
    ADSB_EMITTER_TYPE_ROTOCRAFT = 7,
    ADSB_EMITTER_TYPE_UNASSIGNED = 8,
    ADSB_EMITTER_TYPE_GLIDER = 9,
    ADSB_EMITTER_TYPE_LIGHTER_AIR = 10,
    ADSB_EMITTER_TYPE_PARACHUTE = 11,
    ADSB_EMITTER_TYPE_ULTRA_LIGHT = 12,
    ADSB_EMITTER_TYPE_UNASSIGNED2 = 13,
    ADSB_EMITTER_TYPE_UAV = 14,
    ADSB_EMITTER_TYPE_SPACE = 15,
    ADSB_EMITTER_TYPE_UNASSIGNED3 = 16,
    ADSB_EMITTER_TYPE_EMERGENCY_SURFACE = 17,
    ADSB_EMITTER_TYPE_SERVICE_SURFACE = 18,
    ADSB_EMITTER_TYPE_POINT_OBSTACLE = 19
};

template<>
QString enum_cast<ADSB_EMITTER_TYPE>(ADSB_EMITTER_TYPE value) {
    switch(value) {
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_NO_INFO :
            return "no info";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_LIGHT :
            return "light";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_SMALL :
            return "small";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_LARGE :
            return "large";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_HIGH_VORTEX_LARGE :
            return "high vortex large";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_HEAVY :
            return "heavy";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_HIGHLY_MANUV :
            return "highly maneuverable";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_ROTOCRAFT :
            return "rotorcraft";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_UNASSIGNED :
            return "unassigned";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_GLIDER :
            return "glider";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_LIGHTER_AIR :
            return "lighter air";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_PARACHUTE :
            return "parachute";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_ULTRA_LIGHT :
            return "ultra light";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_UNASSIGNED2 :
            return "unassigned 2";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_UAV :
            return "UAV";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_SPACE :
            return "space";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_UNASSIGNED3 :
            return "unassingned 3";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_EMERGENCY_SURFACE :
            return "emergency surface";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_SERVICE_SURFACE :
            return "service surface";
       case ADSB_EMITTER_TYPE::ADSB_EMITTER_TYPE_POINT_OBSTACLE :
            return "point obstacle";
    }
}

enum class ADSB_FLAGS {
    ADSB_FLAGS_VALID_COORDS = 0x0001,
    ADSB_FLAGS_VALID_ALTITUDE = 0x0002,
    ADSB_FLAGS_VALID_HEADING = 0x0004,
    ADSB_FLAGS_VALID_VELOCITY = 0x0008,
    ADSB_FLAGS_VALID_CALLSIGN = 0x0010,
    ADSB_FLAGS_VALID_SQUAWK = 0x0020,
    ADSB_FLAGS_SIMULATED = 0x0040,
    ADSB_FLAGS_VERTICAL_VELOCITY_VALID = 0x0080,
    ADSB_FLAGS_BARO_VALID = 0x0100,
    ADSB_FLAGS_SOURCE_UAT = 0x8000
};

template<>
QString enum_cast<ADSB_FLAGS>(ADSB_FLAGS value) {
    switch(value) {
       case ADSB_FLAGS::ADSB_FLAGS_VALID_COORDS :
            return "valid coordinates";
       case ADSB_FLAGS::ADSB_FLAGS_VALID_ALTITUDE :
            return "valid altitude";
       case ADSB_FLAGS::ADSB_FLAGS_VALID_HEADING :
            return "valid heading";
       case ADSB_FLAGS::ADSB_FLAGS_VALID_VELOCITY :
            return "valid velocity";
       case ADSB_FLAGS::ADSB_FLAGS_VALID_CALLSIGN :
            return "valid CALLSIGN";
       case ADSB_FLAGS::ADSB_FLAGS_VALID_SQUAWK :
            return "valid SQUAWK";
       case ADSB_FLAGS::ADSB_FLAGS_SIMULATED :
            return "simulated";
       case ADSB_FLAGS::ADSB_FLAGS_VERTICAL_VELOCITY_VALID :
            return "valid vertical velocity";
       case ADSB_FLAGS::ADSB_FLAGS_BARO_VALID :
            return "valid BARO";
       case ADSB_FLAGS::ADSB_FLAGS_SOURCE_UAT :
            return "SOURCE_UAT";
    }
}

enum class MAV_DO_REPOSITION_FLAGS {
    MAV_DO_REPOSITION_FLAGS_CHANGE_MODE = 1
};

template<>
QString enum_cast<MAV_DO_REPOSITION_FLAGS>(MAV_DO_REPOSITION_FLAGS value) {
    switch(value) {
        case MAV_DO_REPOSITION_FLAGS::MAV_DO_REPOSITION_FLAGS_CHANGE_MODE :
            return "aircraft should immediately transition into guided";
    }
}

enum class ESTIMATOR_STATUS_FLAGS {
    ESTIMATOR_ATTITUDE = 0x0001,
    ESTIMATOR_VELOCITY_HORIZ = 0x0002,
    ESTIMATOR_VELOCITY_VERT = 0x0004,
    ESTIMATOR_POS_HORIZ_REL = 0x0008,
    ESTIMATOR_POS_HORIZ_ABS = 0x0010,
    ESTIMATOR_POS_VERT_ABS = 0x0020,
    ESTIMATOR_POS_VERT_AGL = 0x0040,
    ESTIMATOR_CONST_POS_MODE = 0x0080,
    ESTIMATOR_PRED_POS_HORIZ_REL = 0x0100,
    ESTIMATOR_PRED_POS_HORIZ_ABS = 0x0200,
    ESTIMATOR_GPS_GLITCH = 0x0400,
    ESTIMATOR_ACCEL_ERROR = 0x0800
};

template<>
QString enum_cast<ESTIMATOR_STATUS_FLAGS>(ESTIMATOR_STATUS_FLAGS value) {
    switch(value) {
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_ATTITUDE :
            return "attitude";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_VELOCITY_HORIZ :
            return "horizontal velocity";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_VELOCITY_VERT :
            return "vertical velocity";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_POS_HORIZ_REL :
            return "relative horizontal position";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_POS_HORIZ_ABS :
            return "absolute horizontal position";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_POS_VERT_ABS :
            return "absolute vertical position";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_POS_VERT_AGL :
            return "above ground vertical position";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_CONST_POS_MODE :
            return "EKF is in a constant position";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_PRED_POS_HORIZ_REL :
            return "EKF can provide relative horizontal position";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_PRED_POS_HORIZ_ABS :
            return "EKF can provide absolute horizontal position";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_GPS_GLITCH :
            return "EKF detected a GPS glitch";
        case ESTIMATOR_STATUS_FLAGS::ESTIMATOR_ACCEL_ERROR :
            return "EKF detected bad accelerometer data";
    }
}

enum class MOTOR_TEST_ORDER {
    MOTOR_TEST_ORDER_DEFAULT = 0,
    MOTOR_TEST_ORDER_SEQUENCE = 1,
    MOTOR_TEST_ORDER_BOARD = 2
};

template<>
QString enum_cast<MOTOR_TEST_ORDER>(MOTOR_TEST_ORDER value) {
    switch(value) {
        case MOTOR_TEST_ORDER::MOTOR_TEST_ORDER_DEFAULT :
            return "default test method";
        case MOTOR_TEST_ORDER::MOTOR_TEST_ORDER_SEQUENCE :
            return "vehicle-specific sequence";
        case MOTOR_TEST_ORDER::MOTOR_TEST_ORDER_BOARD :
            return "as labeled on the board";
    }
}

enum class MOTOR_TEST_THROTTLE_TYPE {
    MOTOR_TEST_THROTTLE_TYPE_PERCENT = 0,
    MOTOR_TEST_THROTTLE_TYPE_PWM = 1,
    MOTOR_TEST_THROTTLE_TYPE_PILOT = 2,
    MOTOR_TEST_THROTTLE_TYPE_CAL = 3
};

template<>
QString enum_cast<MOTOR_TEST_THROTTLE_TYPE>(MOTOR_TEST_THROTTLE_TYPE value) {
    switch(value) {
        case MOTOR_TEST_THROTTLE_TYPE::MOTOR_TEST_THROTTLE_TYPE_PERCENT :
            return "percentage";
        case MOTOR_TEST_THROTTLE_TYPE::MOTOR_TEST_THROTTLE_TYPE_PWM :
            return "PWM value";
        case MOTOR_TEST_THROTTLE_TYPE::MOTOR_TEST_THROTTLE_TYPE_PILOT :
            return "pass-through from pilot\'s transmitter";
        case MOTOR_TEST_THROTTLE_TYPE::MOTOR_TEST_THROTTLE_TYPE_CAL :
            return "per-motor compass calibration test";
    }
}

enum class GPS_INPUT_IGNORE_FLAGS {
    GPS_INPUT_IGNORE_FLAG_ALT = 0x01,
    GPS_INPUT_IGNORE_FLAG_HDOP = 0x02,
    GPS_INPUT_IGNORE_FLAG_VDOP = 0x04,
    GPS_INPUT_IGNORE_FLAG_VEL_HORIZ = 0x08,
    GPS_INPUT_IGNORE_FLAGS_VEL_VERT = 0x10,
    GPS_INPUT_IGNORE_FLAG_SPEED_ACCURACY = 0x20,
    GPS_INPUT_IGNORE_FLAG_HORIZONTAL_ACCURACY = 0x40,
    GPS_INPUT_IGNORE_FLAG_VERTICAL_ACCURACY = 0x80
};

template<>
QString enum_cast<GPS_INPUT_IGNORE_FLAGS>(GPS_INPUT_IGNORE_FLAGS value) {
    switch(value) {
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAG_ALT :
            return "ignore altitude";
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAG_HDOP :
            return "ignore hdop";
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAG_VDOP :
            return "ignore vdop";
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAG_VEL_HORIZ :
            return "ignore horizontal velocity (vn and ve)";
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAGS_VEL_VERT :
            return "ignore vertical velocity (vd)";
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAG_SPEED_ACCURACY :
            return "ignore speed accuracy";
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAG_HORIZONTAL_ACCURACY :
            return "ignore horizontal accuracy";
        case GPS_INPUT_IGNORE_FLAGS::GPS_INPUT_IGNORE_FLAG_VERTICAL_ACCURACY :
            return "ignore vertical accuracy";
    }
}

enum class MAV_COLLISION_ACTION {
    MAV_COLLISION_ACTION_NONE = 0,
    MAV_COLLISION_ACTION_REPORT = 1,
    MAV_COLLISION_ACTION_ASCEND_OR_DESCEND = 2,
    MAV_COLLISION_ACTION_MOVE_HORIZONTALLY = 3,
    MAV_COLLISION_ACTION_MOVE_PERPENDICULAR = 4,
    MAV_COLLISION_ACTION_RTL = 5,
    MAV_COLLISION_ACTION_HOVER = 6
};

template<>
QString enum_cast<MAV_COLLISION_ACTION>(MAV_COLLISION_ACTION value) {
    switch(value) {
        case MAV_COLLISION_ACTION::MAV_COLLISION_ACTION_NONE :
            return "ignore all collisions";
        case MAV_COLLISION_ACTION::MAV_COLLISION_ACTION_REPORT :
            return "report collisions";
        case MAV_COLLISION_ACTION::MAV_COLLISION_ACTION_ASCEND_OR_DESCEND :
            return "ascend or descend";
        case MAV_COLLISION_ACTION::MAV_COLLISION_ACTION_MOVE_HORIZONTALLY :
            return "move horizontally";
        case MAV_COLLISION_ACTION::MAV_COLLISION_ACTION_MOVE_PERPENDICULAR :
            return "move perpendicular to collision\'s velocity vector";
        case MAV_COLLISION_ACTION::MAV_COLLISION_ACTION_RTL :
            return "fly to launch point";
        case MAV_COLLISION_ACTION::MAV_COLLISION_ACTION_HOVER :
            return "stop in place";
    }
}

enum class MAV_COLLISION_THREAT_LEVEL {
    MAV_COLLISION_THREAT_LEVEL_NONE = 0,
    MAV_COLLISION_THREAT_LEVEL_LOW = 1,
    MAV_COLLISION_THREAT_LEVEL_HIGH = 2
};

template<>
QString enum_cast<MAV_COLLISION_THREAT_LEVEL>(MAV_COLLISION_THREAT_LEVEL value) {
    switch(value) {
        case MAV_COLLISION_THREAT_LEVEL::MAV_COLLISION_THREAT_LEVEL_NONE :
            return "not a threat";
        case MAV_COLLISION_THREAT_LEVEL::MAV_COLLISION_THREAT_LEVEL_LOW :
            return "low";
        case MAV_COLLISION_THREAT_LEVEL::MAV_COLLISION_THREAT_LEVEL_HIGH :
            return "high";
    }
}

enum class MAV_COLLISION_SRC {
    MAV_COLLISION_SRC_ADSB = 0,
    MAV_COLLISION_SRC_MAVLINK_GPS_GLOBAL_INT = 1
};

template<>
QString enum_cast<MAV_COLLISION_SRC>(MAV_COLLISION_SRC value) {
    switch(value) {
        case MAV_COLLISION_SRC::MAV_COLLISION_SRC_ADSB :
            return "ID field references ADSB_VEHICLE packets";
        case MAV_COLLISION_SRC::MAV_COLLISION_SRC_MAVLINK_GPS_GLOBAL_INT :
            return "ID field references MAVLink SRC ID";
    }
}

enum class GPS_FIX_TYPE {
    GPS_FIX_TYPE_NO_GPS = 0,
    GPS_FIX_TYPE_NO_FIX = 1,
    GPS_FIX_TYPE_2D_FIX = 2,
    GPS_FIX_TYPE_3D_FIX = 3,
    GPS_FIX_TYPE_DGPS = 4,
    GPS_FIX_TYPE_RTK_FLOAT = 5,
    GPS_FIX_TYPE_RTK_FIXED = 6,
    GPS_FIX_TYPE_STATIC = 7,
    GPS_FIX_TYPE_PPP = 8
};

template<>
QString enum_cast<GPS_FIX_TYPE>(GPS_FIX_TYPE value) {
    switch(value) {
        case GPS_FIX_TYPE::GPS_FIX_TYPE_NO_GPS :
            return "no GPS";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_NO_FIX :
            return "no fix";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_2D_FIX :
            return "2D position";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_3D_FIX :
            return "3D position";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_DGPS :
            return "DGPS/SBAS aided 3D position";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_RTK_FLOAT :
            return "RTK float, 3D position";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_RTK_FIXED :
            return "RTK fixed, 3D position";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_STATIC :
            return "static fixed";
        case GPS_FIX_TYPE::GPS_FIX_TYPE_PPP :
            return "PPP, 3D position";
    }
}

enum class RTK_BASELINE_COORDINATE_SYSTEM {
    RTK_BASELINE_COORDINATE_SYSTEM_ECEF = 0,
    RTK_BASELINE_COORDINATE_SYSTEM_NED = 1
};

template<>
QString enum_cast<RTK_BASELINE_COORDINATE_SYSTEM>(RTK_BASELINE_COORDINATE_SYSTEM value) {
    switch(value) {
        case RTK_BASELINE_COORDINATE_SYSTEM::RTK_BASELINE_COORDINATE_SYSTEM_ECEF :
            return "earth-centered, earth-fixed";
        case RTK_BASELINE_COORDINATE_SYSTEM::RTK_BASELINE_COORDINATE_SYSTEM_NED :
            return "RTK basestation centered, north, east, down";
    }
}

enum class LANDING_TARGET_TYPE {
    LANDING_TARGET_TYPE_LIGHT_BEACON = 0,
    LANDING_TARGET_TYPE_RADIO_BEACON = 1,
    LANDING_TARGET_TYPE_VISION_FIDUCIAL = 2,
    LANDING_TARGET_TYPE_VISION_OTHER = 3
};

template<>
QString enum_cast<LANDING_TARGET_TYPE>(LANDING_TARGET_TYPE value) {
    switch(value) {
        case LANDING_TARGET_TYPE::LANDING_TARGET_TYPE_LIGHT_BEACON :
            return "light beacon";
        case LANDING_TARGET_TYPE::LANDING_TARGET_TYPE_RADIO_BEACON :
            return "radio beacon";
        case LANDING_TARGET_TYPE::LANDING_TARGET_TYPE_VISION_FIDUCIAL :
            return "fiducial marker";
        case LANDING_TARGET_TYPE::LANDING_TARGET_TYPE_VISION_OTHER :
            return "pre-defined visual feature";
    }
}

enum class VTOL_TRANSITION_HEADING {
    VTOL_TRANSITION_HEADING_VEHICLE_DEFAULT = 0,
    VTOL_TRANSITION_HEADING_NEXT_WAYPOINT = 1,
    VTOL_TRANSITION_HEADING_TAKEOFF = 2,
    VTOL_TRANSITION_HEADING_SPECIFIED = 3,
    VTOL_TRANSITION_HEADING_ANY = 4
};

template<>
QString enum_cast<VTOL_TRANSITION_HEADING>(VTOL_TRANSITION_HEADING value) {
    switch(value) {
        case VTOL_TRANSITION_HEADING::VTOL_TRANSITION_HEADING_VEHICLE_DEFAULT :
            return "vehicle default";
        case VTOL_TRANSITION_HEADING::VTOL_TRANSITION_HEADING_NEXT_WAYPOINT :
            return "toward next waypoint";
        case VTOL_TRANSITION_HEADING::VTOL_TRANSITION_HEADING_TAKEOFF :
            return "use takeoff heading";
        case VTOL_TRANSITION_HEADING::VTOL_TRANSITION_HEADING_SPECIFIED :
            return "specified in parameter 4";
        case VTOL_TRANSITION_HEADING::VTOL_TRANSITION_HEADING_ANY :
            return "current heading";
    }
}

enum class CAMERA_CAP_FLAGS {
    CAMERA_CAP_FLAGS_CAPTURE_VIDEO = 0x0001,
    CAMERA_CAP_FLAGS_CAPTURE_IMAGE = 0x0002,
    CAMERA_CAP_FLAGS_HAS_MODES = 0x0004,
    CAMERA_CAP_FLAGS_CAN_CAPTURE_IMAGE_IN_VIDEO_MODE = 0x0008,
    CAMERA_CAP_FLAGS_CAN_CAPTURE_VIDEO_IN_IMAGE_MODE = 0x0010,
    CAMERA_CAP_FLAGS_HAS_IMAGE_SURVEY_MODE = 0x0020,
    CAMERA_CAP_FLAGS_HAS_BASIC_ZOOM = 0x0040,
    CAMERA_CAP_FLAGS_HAS_BASIC_FOCUS = 0x0080,
    CAMERA_CAP_FLAGS_HAS_VIDEO_STREAM = 0x0100,
    CAMERA_CAP_FLAGS_HAS_TRACKING_POINT = 0x0200,
    CAMERA_CAP_FLAGS_HAS_TRACKING_RECTANGLE = 0x0400,
    CAMERA_CAP_FLAGS_HAS_TRACKING_GEO_STATUS = 0x0800
};

template<>
QString enum_cast<CAMERA_CAP_FLAGS>(CAMERA_CAP_FLAGS value) {
    switch(value) {
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_CAPTURE_VIDEO :
            return "can capture video";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_CAPTURE_IMAGE :
            return "can capture images";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_MODES :
            return "has separate video and image modes";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_CAN_CAPTURE_IMAGE_IN_VIDEO_MODE :
            return "can capture images in video mode";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_CAN_CAPTURE_VIDEO_IN_IMAGE_MODE :
            return "can capture video in image mode";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_IMAGE_SURVEY_MODE :
            return "has image survey mode";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_BASIC_ZOOM :
            return "has basic zoom control";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_BASIC_FOCUS :
            return "has basic focus control";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_VIDEO_STREAM :
            return "can stream video";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_TRACKING_POINT :
            return "supports point tracking";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_TRACKING_RECTANGLE :
            return "supports rectangle tracking";
        case CAMERA_CAP_FLAGS::CAMERA_CAP_FLAGS_HAS_TRACKING_GEO_STATUS :
            return "supports tracking geo status";
    }
}

enum class VIDEO_STREAM_STATUS_FLAGS {
    VIDEO_STREAM_STATUS_FLAGS_RUNNING = 1,
    VIDEO_STREAM_STATUS_FLAGS_THERMAL = 2
};

template<>
QString enum_cast<VIDEO_STREAM_STATUS_FLAGS>(VIDEO_STREAM_STATUS_FLAGS value) {
    switch(value) {
        case VIDEO_STREAM_STATUS_FLAGS::VIDEO_STREAM_STATUS_FLAGS_RUNNING :
            return "active";
        case VIDEO_STREAM_STATUS_FLAGS::VIDEO_STREAM_STATUS_FLAGS_THERMAL :
            return "thermal imaging";
    }
}

enum class VIDEO_STREAM_TYPE {
    VIDEO_STREAM_TYPE_RTSP = 0,
    VIDEO_STREAM_TYPE_RTPUDP = 1,
    VIDEO_STREAM_TYPE_TCP_MPEG = 2,
    VIDEO_STREAM_TYPE_MPEG_TS_H264 = 3
};

template<>
QString enum_cast<VIDEO_STREAM_TYPE>(VIDEO_STREAM_TYPE value) {
    switch(value) {
        case VIDEO_STREAM_TYPE::VIDEO_STREAM_TYPE_RTSP :
            return "RTSP";
        case VIDEO_STREAM_TYPE::VIDEO_STREAM_TYPE_RTPUDP :
            return "RTP UDP";
        case VIDEO_STREAM_TYPE::VIDEO_STREAM_TYPE_TCP_MPEG :
            return "MPEG on TCP";
        case VIDEO_STREAM_TYPE::VIDEO_STREAM_TYPE_MPEG_TS_H264 :
            return "h.264 on MPEG TS";
    }
}

enum class CAMERA_TRACKING_STATUS_FLAGS {
    CAMERA_TRACKING_STATUS_FLAGS_IDLE = 0x00,
    CAMERA_TRACKING_STATUS_FLAGS_ACTIVE = 0x01,
    CAMERA_TRACKING_STATUS_FLAGS_ERROR = 0x02
};

template<>
QString enum_cast<CAMERA_TRACKING_STATUS_FLAGS>(CAMERA_TRACKING_STATUS_FLAGS value) {
    switch(value) {
        case CAMERA_TRACKING_STATUS_FLAGS::CAMERA_TRACKING_STATUS_FLAGS_IDLE :
            return "not tracking";
        case CAMERA_TRACKING_STATUS_FLAGS::CAMERA_TRACKING_STATUS_FLAGS_ACTIVE :
            return "target is a point";
        case CAMERA_TRACKING_STATUS_FLAGS::CAMERA_TRACKING_STATUS_FLAGS_ERROR :
            return "target is a rectangle";
    }
}

enum class CAMERA_TRACKING_TARGET_DATA {
    CAMERA_TRACKING_TARGET_NONE = 0x00,
    CAMERA_TRACKING_TARGET_EMBEDDED = 0x01,
    CAMERA_TRACKING_TARGET_RENDERED = 0x02,
    CAMERA_TRACKING_TARGET_IN_STATUS = 0x04
};

template<>
QString enum_cast<CAMERA_TRACKING_TARGET_DATA>(CAMERA_TRACKING_TARGET_DATA value) {
    switch(value) {
        case CAMERA_TRACKING_TARGET_DATA::CAMERA_TRACKING_TARGET_NONE :
            return "no target data";
        case CAMERA_TRACKING_TARGET_DATA::CAMERA_TRACKING_TARGET_EMBEDDED :
            return "embedded in image";
        case CAMERA_TRACKING_TARGET_DATA::CAMERA_TRACKING_TARGET_RENDERED :
            return "rendered in image";
        case CAMERA_TRACKING_TARGET_DATA::CAMERA_TRACKING_TARGET_IN_STATUS :
            return "within status message";
    }
}

enum class CAMERA_ZOOM_TYPE {
    ZOOM_TYPE_STEP = 0,
    ZOOM_TYPE_CONTINUOUS = 1,
    ZOOM_TYPE_RANGE = 2,
    ZOOM_TYPE_FOCAL_LENGTH = 3
};

template<>
QString enum_cast<CAMERA_ZOOM_TYPE>(CAMERA_ZOOM_TYPE value) {
    switch(value) {
        case CAMERA_ZOOM_TYPE::ZOOM_TYPE_STEP :
            return "one step increment";
        case CAMERA_ZOOM_TYPE::ZOOM_TYPE_CONTINUOUS :
            return "continuous zoom";
        case CAMERA_ZOOM_TYPE::ZOOM_TYPE_RANGE :
            return "proportional of full camera range";
        case CAMERA_ZOOM_TYPE::ZOOM_TYPE_FOCAL_LENGTH :
            return "zoom value focal length in mm";
    }
}

enum class SET_FOCUS_TYPE {
    FOCUS_TYPE_STEP = 0,
    FOCUS_TYPE_CONTINUOUS = 1,
    FOCUS_TYPE_RANGE = 2,
    FOCUS_TYPE_METERS = 3
};

template<>
QString enum_cast<SET_FOCUS_TYPE>(SET_FOCUS_TYPE value) {
    switch(value) {
        case SET_FOCUS_TYPE::FOCUS_TYPE_STEP :
            return "one step increment";
        case SET_FOCUS_TYPE::FOCUS_TYPE_CONTINUOUS :
            return "continuous focus";
        case SET_FOCUS_TYPE::FOCUS_TYPE_RANGE :
            return "proportional of full camera range";
        case SET_FOCUS_TYPE::FOCUS_TYPE_METERS :
            return "focus value in meters";
    }
}

enum class PARAM_ACK {
    PARAM_ACK_ACCEPTED = 0,
    PARAM_ACK_VALUE_UNSUPPORTED = 1,
    PARAM_ACK_FAILED = 2,
    PARAM_ACK_IN_PROGRESS = 3
};

template<>
QString enum_cast<PARAM_ACK>(PARAM_ACK value) {
    switch(value) {
        case PARAM_ACK::PARAM_ACK_ACCEPTED :
            return "vlue accepted and set";
        case PARAM_ACK::PARAM_ACK_VALUE_UNSUPPORTED :
            return "value unknown";
        case PARAM_ACK::PARAM_ACK_FAILED :
            return "failed to set";
        case PARAM_ACK::PARAM_ACK_IN_PROGRESS :
            return "in progress";
    }
}

enum class CAMERA_MODE {
    CAMERA_MODE_IMAGE = 0,
    CAMERA_MODE_VIDEO = 1,
    CAMERA_MODE_IMAGE_SURVEY = 2
};

template<>
QString enum_cast<CAMERA_MODE>(CAMERA_MODE value) {
    switch(value) {
        case CAMERA_MODE::CAMERA_MODE_IMAGE :
            return "image capture";
        case CAMERA_MODE::CAMERA_MODE_VIDEO :
            return "video capture";
        case CAMERA_MODE::CAMERA_MODE_IMAGE_SURVEY :
            return "image survey capture";
    }
}

enum class MAV_ARM_AUTH_DENIED_REASON {
    MAV_ARM_AUTH_DENIED_REASON_GENERIC = 0,
    MAV_ARM_AUTH_DENIED_REASON_NONE = 1,
    MAV_ARM_AUTH_DENIED_REASON_INVALID_WAYPOINT = 2,
    MAV_ARM_AUTH_DENIED_REASON_TIMEOUT = 3,
    MAV_ARM_AUTH_DENIED_REASON_AIRSPACE_IN_USE = 4,
    MAV_ARM_AUTH_DENIED_REASON_BAD_WEATHER = 5
};

template<>
QString enum_cast<MAV_ARM_AUTH_DENIED_REASON>(MAV_ARM_AUTH_DENIED_REASON value) {
    switch(value) {
        case MAV_ARM_AUTH_DENIED_REASON::MAV_ARM_AUTH_DENIED_REASON_GENERIC :
            return "not specific reason";
        case MAV_ARM_AUTH_DENIED_REASON::MAV_ARM_AUTH_DENIED_REASON_NONE :
            return "error will be sent to GCS";
        case MAV_ARM_AUTH_DENIED_REASON::MAV_ARM_AUTH_DENIED_REASON_INVALID_WAYPOINT :
            return "at least one waypoint has an invalid value";
        case MAV_ARM_AUTH_DENIED_REASON::MAV_ARM_AUTH_DENIED_REASON_TIMEOUT :
            return "timeout in the authorizer process";
        case MAV_ARM_AUTH_DENIED_REASON::MAV_ARM_AUTH_DENIED_REASON_AIRSPACE_IN_USE :
            return "airspace is used by other vehicle";
        case MAV_ARM_AUTH_DENIED_REASON::MAV_ARM_AUTH_DENIED_REASON_BAD_WEATHER :
            return "extreme weather conditions";
    }
}

enum class RC_TYPE {
    RC_TYPE_SPEKTRUM_DSM2 = 0,
    RC_TYPE_SPEKTRUM_DSMX = 1
};

template<>
QString enum_cast<RC_TYPE>(RC_TYPE value) {
    switch(value) {
        case RC_TYPE::RC_TYPE_SPEKTRUM_DSM2 :
            return "Spektrum DSM2";
        case RC_TYPE::RC_TYPE_SPEKTRUM_DSMX :
            return "Spektrum DSMX";
    }
}

enum class POSITION_TARGET_TYPEMASK {
    POSITION_TARGET_TYPEMASK_X_IGNORE = 0x0001,
    POSITION_TARGET_TYPEMASK_Y_IGNORE = 0x0002,
    POSITION_TARGET_TYPEMASK_Z_IGNORE = 0x0004,
    POSITION_TARGET_TYPEMASK_VX_IGNORE = 0x0008,
    POSITION_TARGET_TYPEMASK_VY_IGNORE = 0x0010,
    POSITION_TARGET_TYPEMASK_VZ_IGNORE = 0x0020,
    POSITION_TARGET_TYPEMASK_AX_IGNORE = 0x0040,
    POSITION_TARGET_TYPEMASK_AY_IGNORE = 0x0080,
    POSITION_TARGET_TYPEMASK_AZ_IGNORE = 0x0100,
    POSITION_TARGET_TYPEMASK_FORCE_SET = 0x0200,
    POSITION_TARGET_TYPEMASK_YAW_IGNORE = 0x0400,
    POSITION_TARGET_TYPEMASK_YAW_RATE_IGNORE = 0x0800
};

template<>
QString enum_cast<POSITION_TARGET_TYPEMASK>(POSITION_TARGET_TYPEMASK value) {
    switch(value) {
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_X_IGNORE :
            return "ignore position x";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_Y_IGNORE :
            return "ignore position y";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_Z_IGNORE :
            return "ignore position z";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_VX_IGNORE :
            return "ignore velocity x";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_VY_IGNORE :
            return "ignore velocity y";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_VZ_IGNORE :
            return "ignore velocity z";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_AX_IGNORE :
            return "ignore acceleration x";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_AY_IGNORE :
            return "ignore acceleration y";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_AZ_IGNORE :
            return "ignore acceleration z";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_FORCE_SET :
            return "use force instead of acceleration";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_YAW_IGNORE :
            return "ignore yaw";
        case POSITION_TARGET_TYPEMASK::POSITION_TARGET_TYPEMASK_YAW_RATE_IGNORE :
            return "ignore yaw rate";
    }
}

enum class UTM_FLIGHT_STATE {
    UTM_FLIGHT_STATE_UNKNOWN = 0x01,
    UTM_FLIGHT_STATE_GROUND = 0x02,
    UTM_FLIGHT_STATE_AIRBORNE = 0x04,
    UTM_FLIGHT_STATE_EMERGENCY = 0x08,
    UTM_FLIGHT_STATE_NOCTRL = 0x10
};

template<>
QString enum_cast<UTM_FLIGHT_STATE>(UTM_FLIGHT_STATE value) {
    switch(value) {
        case UTM_FLIGHT_STATE::UTM_FLIGHT_STATE_UNKNOWN :
            return "unknown";
        case UTM_FLIGHT_STATE::UTM_FLIGHT_STATE_GROUND :
            return "on the ground";
        case UTM_FLIGHT_STATE::UTM_FLIGHT_STATE_AIRBORNE :
            return "in the air";
        case UTM_FLIGHT_STATE::UTM_FLIGHT_STATE_EMERGENCY :
            return "in an emergency flight";
        case UTM_FLIGHT_STATE::UTM_FLIGHT_STATE_NOCTRL :
            return "no active controls";
    }
}

enum class UTM_DATA_AVAIL_FLAGS {
    UTM_DATA_AVAIL_FLAGS_TIME_VALID = 0x01,
    UTM_DATA_AVAIL_FLAGS_UAS_ID_AVAILABLE = 0x02,
    UTM_DATA_AVAIL_FLAGS_POSITION_AVAILABLE = 0x04,
    UTM_DATA_AVAIL_FLAGS_ALTITUDE_AVAILABLE = 0x08,
    UTM_DATA_AVAIL_FLAGS_RELATIVE_ALTITUDE_AVAILABLE = 0x10,
    UTM_DATA_AVAIL_FLAGS_HORIZONTAL_VELO_AVAILABLE = 0x20,
    UTM_DATA_AVAIL_FLAGS_VERTICAL_VELO_AVAILABLE = 0x40,
    UTM_DATA_AVAIL_FLAGS_NEXT_WAYPOINT_AVAILABLE = 0x80
};

template<>
QString enum_cast<UTM_DATA_AVAIL_FLAGS>(UTM_DATA_AVAIL_FLAGS value) {
    switch(value) {
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_TIME_VALID :
            return "time is invalid";
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_UAS_ID_AVAILABLE :
            return "uas_is is invalid";
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_POSITION_AVAILABLE :
            return "lat, lon, h_acc are invalid";
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_ALTITUDE_AVAILABLE :
            return "alt and v_acc are invalid";
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_RELATIVE_ALTITUDE_AVAILABLE :
            return "relative_alt is invalid";
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_HORIZONTAL_VELO_AVAILABLE :
            return "vx and vy are invalid";
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_VERTICAL_VELO_AVAILABLE :
            return "vz is invalid";
        case UTM_DATA_AVAIL_FLAGS::UTM_DATA_AVAIL_FLAGS_NEXT_WAYPOINT_AVAILABLE :
            return "next_lat, next_lon and next_alt are invalid";
    }
}

enum class  CELLULAR_NETWORK_RADIO_TYPE {
    CELLULAR_NETWORK_RADIO_TYPE_NONE = 0,
    CELLULAR_NETWORK_RADIO_TYPE_GSM = 1,
    CELLULAR_NETWORK_RADIO_TYPE_CDMA = 2,
    CELLULAR_NETWORK_RADIO_TYPE_WCDMA = 3,
    CELLULAR_NETWORK_RADIO_TYPE_LTE = 4
};

template<>
QString enum_cast<CELLULAR_NETWORK_RADIO_TYPE>(CELLULAR_NETWORK_RADIO_TYPE value) {
    switch(value) {
        case CELLULAR_NETWORK_RADIO_TYPE::CELLULAR_NETWORK_RADIO_TYPE_NONE :
            return "none";
        case CELLULAR_NETWORK_RADIO_TYPE::CELLULAR_NETWORK_RADIO_TYPE_GSM :
            return "GSM";
        case CELLULAR_NETWORK_RADIO_TYPE::CELLULAR_NETWORK_RADIO_TYPE_CDMA :
            return "CDMA";
        case CELLULAR_NETWORK_RADIO_TYPE::CELLULAR_NETWORK_RADIO_TYPE_WCDMA :
            return "WCDMA";
        case CELLULAR_NETWORK_RADIO_TYPE::CELLULAR_NETWORK_RADIO_TYPE_LTE :
            return "LTE";
    }
}

enum class CELLULAR_STATUS_FLAG {
    CELLULAR_STATUS_FLAG_UNKNOWN = 0,
    CELLULAR_STATUS_FLAG_FAILED = 1,
    CELLULAR_STATUS_FLAG_INITIALIZING = 2,
    CELLULAR_STATUS_FLAG_LOCKED = 3,
    CELLULAR_STATUS_FLAG_DISABLED = 4,
    CELLULAR_STATUS_FLAG_DISABLING = 5,
    CELLULAR_STATUS_FLAG_ENABLING = 6,
    CELLULAR_STATUS_FLAG_ENABLED = 7,
    CELLULAR_STATUS_FLAG_SEARCHING = 8,
    CELLULAR_STATUS_FLAG_REGISTERED = 9,
    CELLULAR_STATUS_FLAG_DISCONNECTING = 10,
    CELLULAR_STATUS_FLAG_CONNECTING = 11,
    CELLULAR_STATUS_FLAG_CONNECTED = 12
};

template<>
QString enum_cast<CELLULAR_STATUS_FLAG>(CELLULAR_STATUS_FLAG value) {
    switch(value) {
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_UNKNOWN :
            return "unknown";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_FAILED :
            return "unusable";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_INITIALIZING :
            return "initializing";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_LOCKED :
            return "locked";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_DISABLED :
            return "powered down";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_DISABLING :
            return "powering down";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_ENABLING :
            return "powering on";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_ENABLED :
            return "powered on";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_SEARCHING :
            return "searching";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_REGISTERED :
            return "registered";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_DISCONNECTING :
            return "disconnecting";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_CONNECTING :
            return "connecting";
        case CELLULAR_STATUS_FLAG::CELLULAR_STATUS_FLAG_CONNECTED :
            return "connected";
    }
}

enum class CELLULAR_NETWORK_FAILED_REASON {
    CELLULAR_NETWORK_FAILED_REASON_NONE = 0,
    CELLULAR_NETWORK_FAILED_REASON_UNKNOWN = 1,
    CELLULAR_NETWORK_FAILED_REASON_SIM_MISSING = 2,
    CELLULAR_NETWORK_FAILED_REASON_SIM_ERROR = 3
};

template<>
QString enum_cast<CELLULAR_NETWORK_FAILED_REASON>(CELLULAR_NETWORK_FAILED_REASON value) {
    switch(value) {
        case CELLULAR_NETWORK_FAILED_REASON::CELLULAR_NETWORK_FAILED_REASON_NONE :
            return "no error";
        case CELLULAR_NETWORK_FAILED_REASON::CELLULAR_NETWORK_FAILED_REASON_UNKNOWN :
            return "unknown error";
        case CELLULAR_NETWORK_FAILED_REASON::CELLULAR_NETWORK_FAILED_REASON_SIM_MISSING :
            return "SIM is missing";
        case CELLULAR_NETWORK_FAILED_REASON::CELLULAR_NETWORK_FAILED_REASON_SIM_ERROR :
            return "SIM not usable";
    }
}

enum class PRECISION_LAND_MODE {
    PRECISION_LAND_MODE_DISABLED = 0,
    PRECISION_LAND_MODE_OPPORTUNISTIC = 1,
    PRECISION_LAND_MODE_REQUIRED = 2
};

template<>
QString enum_cast<PRECISION_LAND_MODE>(PRECISION_LAND_MODE value) {
    switch(value) {
        case PRECISION_LAND_MODE::PRECISION_LAND_MODE_DISABLED :
            return "normal landing";
        case PRECISION_LAND_MODE::PRECISION_LAND_MODE_OPPORTUNISTIC :
            return "precision landing if beacon detected";
        case PRECISION_LAND_MODE::PRECISION_LAND_MODE_REQUIRED :
            return "precision landing, searching for beacon";
    }
}

enum class PARACHUTE_ACTION {
    PARACHUTE_DISABLED = 0,
    PARACHUTE_ENABLED = 1,
    PARACHUTE_RELEASE = 2
};

template<>
QString enum_cast<PARACHUTE_ACTION>(PARACHUTE_ACTION value) {
    switch(value) {
        case PARACHUTE_ACTION::PARACHUTE_DISABLED :
            return "disable auto-release";
        case PARACHUTE_ACTION::PARACHUTE_ENABLED :
            return "enable auto-release";
        case PARACHUTE_ACTION::PARACHUTE_RELEASE :
            return "release and kill motors";
    }
}

enum class MAV_TUNNEL_PAYLOAD_TYPE {
    MAV_TUNNEL_PAYLOAD_TYPE_UNKNOWN = 0,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED0 = 200,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED1 = 201,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED2 = 202,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED3 = 203,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED4 = 204,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED5 = 205,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED6 = 206,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED7 = 207,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED8 = 208,
    MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED9 = 209
};

template<>
QString enum_cast<MAV_TUNNEL_PAYLOAD_TYPE>(MAV_TUNNEL_PAYLOAD_TYPE value) {
    switch(value) {
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_UNKNOWN :
            return "unknown";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED0 :
            return "registered 0 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED1 :
            return "registered 1 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED2 :
            return "registered 2 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED3 :
            return "registered 3 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED4 :
            return "registered 4 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED5 :
            return "registered 5 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED6 :
            return "registered 6 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED7 :
            return "registered 7 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED8 :
            return "registered 8 for STorM32 gimbal";
        case MAV_TUNNEL_PAYLOAD_TYPE::MAV_TUNNEL_PAYLOAD_TYPE_STORM32_RESERVED9 :
            return "registered 9 for STorM32 gimbal";
    }
}

enum class MAV_ODID_ID_TYPE {
    MAV_ODID_ID_TYPE_NONE = 0,
    MAV_ODID_ID_TYPE_SERIAL_NUMBER = 1,
    MAV_ODID_ID_TYPE_CAA_REGISTRATION_ID = 2,
    MAV_ODID_ID_TYPE_UTM_ASSIGNED_UUID = 3
};

template<>
QString enum_cast<MAV_ODID_ID_TYPE>(MAV_ODID_ID_TYPE value) {
    switch(value) {
        case MAV_ODID_ID_TYPE::MAV_ODID_ID_TYPE_NONE :
            return "type not specified";
        case MAV_ODID_ID_TYPE::MAV_ODID_ID_TYPE_SERIAL_NUMBER :
            return "manufacturer serial number";
        case MAV_ODID_ID_TYPE::MAV_ODID_ID_TYPE_CAA_REGISTRATION_ID :
            return "CAA registered ID";
        case MAV_ODID_ID_TYPE::MAV_ODID_ID_TYPE_UTM_ASSIGNED_UUID :
            return "UTM assigned UUID";
    }
}

enum class MAV_ODID_UA_TYPE {
    MAV_ODID_UA_TYPE_NONE = 0,
    MAV_ODID_UA_TYPE_AEROPLANE = 1,
    MAV_ODID_UA_TYPE_HELICOPTER_OR_MULTICOPTER = 2,
    MAV_ODID_UA_TYPE_GYROPLANE = 3,
    MAV_ODID_UA_TYPE_HYBRID_LIFT = 4,
    MAV_ODID_UA_TYPE_ORNITHOPTER = 5,
    MAV_ODID_UA_TYPE_GLIDER = 6,
    MAV_ODID_UA_TYPE_KITE = 7,
    MAV_ODID_UA_TYPE_FREE_BALLOON = 8,
    MAV_ODID_UA_TYPE_CAPTIVE_BALLOON = 9,
    MAV_ODID_UA_TYPE_AIRSHIP = 10,
    MAV_ODID_UA_TYPE_FREE_FALL_PARACHUTE = 11,
    MAV_ODID_UA_TYPE_ROCKET = 12,
    MAV_ODID_UA_TYPE_TETHERED_POWERED_AIRCRAFT = 13,
    MAV_ODID_UA_TYPE_GROUND_OBSTACLE = 14,
    MAV_ODID_UA_TYPE_OTHER = 15
};

template<>
QString enum_cast<MAV_ODID_UA_TYPE>(MAV_ODID_UA_TYPE value) {
    switch(value) {
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_NONE :
            return "type not specified";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_AEROPLANE :
            return "airplane";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_HELICOPTER_OR_MULTICOPTER :
            return "helicopter/multicopter";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_GYROPLANE :
            return "gyroplane";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_HYBRID_LIFT :
            return "VTOL";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_ORNITHOPTER :
            return "ornithopter";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_GLIDER :
            return "glider";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_KITE :
            return "kite";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_FREE_BALLOON :
            return "free baloon";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_CAPTIVE_BALLOON :
            return "captive balloon";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_AIRSHIP :
            return "airship";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_FREE_FALL_PARACHUTE :
            return "free fall/parachute";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_ROCKET :
            return "rocket";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_TETHERED_POWERED_AIRCRAFT :
            return "tethered powered aircraft";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_GROUND_OBSTACLE :
            return "ground obstacle";
        case MAV_ODID_UA_TYPE::MAV_ODID_UA_TYPE_OTHER :
            return "other type";
    }
}

enum class MAV_ODID_STATUS {
    MAV_ODID_STATUS_UNDECLARED = 0,
    MAV_ODID_STATUS_GROUND = 1,
    MAV_ODID_STATUS_AIRBORNE = 2,
    MAV_ODID_STATUS_EMERGENCY = 3
};

template<>
QString enum_cast<MAV_ODID_STATUS>(MAV_ODID_STATUS value) {
    switch(value) {
        case MAV_ODID_STATUS::MAV_ODID_STATUS_UNDECLARED :
            return "undeclared";
        case MAV_ODID_STATUS::MAV_ODID_STATUS_GROUND :
            return "on the ground";
        case MAV_ODID_STATUS::MAV_ODID_STATUS_AIRBORNE :
            return "in the air";
        case MAV_ODID_STATUS::MAV_ODID_STATUS_EMERGENCY :
            return "having an emergency";
    }
}

enum class MAV_ODID_HEIGHT_REF {
    MAV_ODID_HEIGHT_REF_OVER_TAKEOFF = 0,
    MAV_ODID_HEIGHT_REF_OVER_GROUND = 1
};

template<>
QString enum_cast<MAV_ODID_HEIGHT_REF>(MAV_ODID_HEIGHT_REF value) {
    switch(value) {
        case MAV_ODID_HEIGHT_REF::MAV_ODID_HEIGHT_REF_OVER_TAKEOFF :
            return "height is relative to the take-off location";
        case MAV_ODID_HEIGHT_REF::MAV_ODID_HEIGHT_REF_OVER_GROUND :
            return "height field is relative to ground";
    }
}

enum class MAV_ODID_HOR_ACC {
    MAV_ODID_HOR_ACC_UNKNOWN = 0,
    MAV_ODID_HOR_ACC_10NM = 1,
    MAV_ODID_HOR_ACC_4NM = 2,
    MAV_ODID_HOR_ACC_2NM = 3,
    MAV_ODID_HOR_ACC_1NM = 4,
    MAV_ODID_HOR_ACC_0_5NM = 5,
    MAV_ODID_HOR_ACC_0_3NM = 6,
    MAV_ODID_HOR_ACC_0_1NM = 7,
    MAV_ODID_HOR_ACC_0_05NM = 8,
    MAV_ODID_HOR_ACC_30_METER = 9,
    MAV_ODID_HOR_ACC_10_METER = 10,
    MAV_ODID_HOR_ACC_3_METER = 11,
    MAV_ODID_HOR_ACC_1_METER = 12
};

template<>
QString enum_cast<MAV_ODID_HOR_ACC>(MAV_ODID_HOR_ACC value) {
    switch(value) {
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_UNKNOWN :
            return "unknown";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_10NM :
            return "less than 10 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_4NM :
            return "less than 4 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_2NM :
            return "less than 2 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_1NM :
            return "less than 1 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_0_5NM :
            return "less than 0.5 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_0_3NM :
            return "less than 0.3 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_0_1NM :
            return "less than 0.1 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_0_05NM :
            return "less than 0.05 Nautical Miles";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_30_METER :
            return "less than 30 meters";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_10_METER :
            return "less than 10 meters";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_3_METER :
            return "less than 3 meters";
        case MAV_ODID_HOR_ACC::MAV_ODID_HOR_ACC_1_METER :
            return "less than 1 meter";
    }
}

enum class MAV_ODID_VER_ACC {
    MAV_ODID_VER_ACC_UNKNOWN = 0,
    MAV_ODID_VER_ACC_150_METER = 1,
    MAV_ODID_VER_ACC_45_METER = 2,
    MAV_ODID_VER_ACC_25_METER = 3,
    MAV_ODID_VER_ACC_10_METER = 4,
    MAV_ODID_VER_ACC_3_METER = 5,
    MAV_ODID_VER_ACC_1_METER = 6
};

template<>
QString enum_cast<MAV_ODID_VER_ACC>(MAV_ODID_VER_ACC value) {
    switch(value) {
        case MAV_ODID_VER_ACC::MAV_ODID_VER_ACC_UNKNOWN :
            return "unknown";
        case MAV_ODID_VER_ACC::MAV_ODID_VER_ACC_150_METER :
            return "less than 150 meters";
        case MAV_ODID_VER_ACC::MAV_ODID_VER_ACC_45_METER :
            return "less than 45 meters";
        case MAV_ODID_VER_ACC::MAV_ODID_VER_ACC_25_METER :
            return "less than 25 meters";
        case MAV_ODID_VER_ACC::MAV_ODID_VER_ACC_10_METER :
            return "less than 10 meters";
        case MAV_ODID_VER_ACC::MAV_ODID_VER_ACC_3_METER :
            return "less than 3 meters";
        case MAV_ODID_VER_ACC::MAV_ODID_VER_ACC_1_METER :
            return "less than 1 meter";
    }
}

enum class MAV_ODID_SPEED_ACC {
    MAV_ODID_SPEED_ACC_UNKNOWN = 0,
    MAV_ODID_SPEED_ACC_10_METERS_PER_SECOND = 1,
    MAV_ODID_SPEED_ACC_3_METERS_PER_SECOND = 2,
    MAV_ODID_SPEED_ACC_1_METERS_PER_SECOND = 3,
    MAV_ODID_SPEED_ACC_0_3_METERS_PER_SECOND = 4
};

template<>
QString enum_cast<MAV_ODID_SPEED_ACC>(MAV_ODID_SPEED_ACC value) {
    switch(value) {
        case MAV_ODID_SPEED_ACC::MAV_ODID_SPEED_ACC_UNKNOWN :
            return "unknown";
        case MAV_ODID_SPEED_ACC::MAV_ODID_SPEED_ACC_10_METERS_PER_SECOND :
            return "less than 10 m/s";
        case MAV_ODID_SPEED_ACC::MAV_ODID_SPEED_ACC_3_METERS_PER_SECOND :
            return "less than 3 m/s";
        case MAV_ODID_SPEED_ACC::MAV_ODID_SPEED_ACC_1_METERS_PER_SECOND :
            return "less than 1 m/s";
        case MAV_ODID_SPEED_ACC::MAV_ODID_SPEED_ACC_0_3_METERS_PER_SECOND :
            return "less than 0.3 m/s";
    }
}

enum class MAV_ODID_TIME_ACC {
    MAV_ODID_TIME_ACC_UNKNOWN = 0,
    MAV_ODID_TIME_ACC_0_1_SECOND = 1,
    MAV_ODID_TIME_ACC_0_2_SECOND = 2,
    MAV_ODID_TIME_ACC_0_3_SECOND = 3,
    MAV_ODID_TIME_ACC_0_4_SECOND = 4,
    MAV_ODID_TIME_ACC_0_5_SECOND = 5,
    MAV_ODID_TIME_ACC_0_6_SECOND = 6,
    MAV_ODID_TIME_ACC_0_7_SECOND = 7,
    MAV_ODID_TIME_ACC_0_8_SECOND = 8,
    MAV_ODID_TIME_ACC_0_9_SECOND = 9,
    MAV_ODID_TIME_ACC_1_0_SECOND = 10,
    MAV_ODID_TIME_ACC_1_1_SECOND = 11,
    MAV_ODID_TIME_ACC_1_2_SECOND = 12,
    MAV_ODID_TIME_ACC_1_3_SECOND = 13,
    MAV_ODID_TIME_ACC_1_4_SECOND = 14,
    MAV_ODID_TIME_ACC_1_5_SECOND = 15
};

template<>
QString enum_cast<MAV_ODID_TIME_ACC>(MAV_ODID_TIME_ACC value) {
    switch(value) {
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_UNKNOWN :
            return "unknown";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_1_SECOND :
            return "less than 0.1 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_2_SECOND :
            return "less than 0.2 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_3_SECOND :
            return "less than 0.3 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_4_SECOND :
            return "less than 0.4 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_5_SECOND :
            return "less than 0.5 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_6_SECOND :
            return "less than 0.6 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_7_SECOND :
            return "less than 0.7 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_8_SECOND :
            return "less than 0.8 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_0_9_SECOND :
            return "less than 0.9 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_1_0_SECOND :
            return "less than 1.0 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_1_1_SECOND :
            return "less than 1.1 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_1_2_SECOND :
            return "less than 1.2 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_1_3_SECOND :
            return "less than 1.3 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_1_4_SECOND :
            return "less than 1.4 s";
        case MAV_ODID_TIME_ACC::MAV_ODID_TIME_ACC_1_5_SECOND :
            return "less than 1.5 s";
    }
}

enum class MAV_ODID_AUTH_TYPE {
    MAV_ODID_AUTH_TYPE_NONE = 0,
    MAV_ODID_AUTH_TYPE_UAS_ID_SIGNATURE = 1,
    MAV_ODID_AUTH_TYPE_OPERATOR_ID_SIGNATURE = 2,
    MAV_ODID_AUTH_TYPE_MESSAGE_SET_SIGNATURE = 3,
    MAV_ODID_AUTH_TYPE_NETWORK_REMOTE_ID = 4
};

template<>
QString enum_cast<MAV_ODID_AUTH_TYPE>(MAV_ODID_AUTH_TYPE value) {
    switch(value) {
        case MAV_ODID_AUTH_TYPE::MAV_ODID_AUTH_TYPE_NONE :
            return "not specified";
        case MAV_ODID_AUTH_TYPE::MAV_ODID_AUTH_TYPE_UAS_ID_SIGNATURE :
            return "signature for UAS ID";
        case MAV_ODID_AUTH_TYPE::MAV_ODID_AUTH_TYPE_OPERATOR_ID_SIGNATURE :
            return "signature for the Operator ID";
        case MAV_ODID_AUTH_TYPE::MAV_ODID_AUTH_TYPE_MESSAGE_SET_SIGNATURE :
            return "signature for the entire message set";
        case MAV_ODID_AUTH_TYPE::MAV_ODID_AUTH_TYPE_NETWORK_REMOTE_ID :
            return "provided by Network Remote ID";
    }
}

enum class MAV_ODID_DESC_TYPE {
    MAV_ODID_DESC_TYPE_TEXT = 0
};

template<>
QString enum_cast<MAV_ODID_DESC_TYPE>(MAV_ODID_DESC_TYPE value) {
    switch(value) {
        case MAV_ODID_DESC_TYPE::MAV_ODID_DESC_TYPE_TEXT :
            return "free-form text";
    }
}

enum class MAV_ODID_OPERATOR_LOCATION_TYPE {
    MAV_ODID_OPERATOR_LOCATION_TYPE_TAKEOFF = 0,
    MAV_ODID_OPERATOR_LOCATION_TYPE_LIVE_GNSS = 1,
    MAV_ODID_OPERATOR_LOCATION_TYPE_FIXED = 2
};

template<>
QString enum_cast<MAV_ODID_OPERATOR_LOCATION_TYPE>(MAV_ODID_OPERATOR_LOCATION_TYPE value) {
    switch(value) {
        case MAV_ODID_OPERATOR_LOCATION_TYPE::MAV_ODID_OPERATOR_LOCATION_TYPE_TAKEOFF :
            return "the same as take-off location";
        case MAV_ODID_OPERATOR_LOCATION_TYPE::MAV_ODID_OPERATOR_LOCATION_TYPE_LIVE_GNSS :
            return "based on live GNSS data";
        case MAV_ODID_OPERATOR_LOCATION_TYPE::MAV_ODID_OPERATOR_LOCATION_TYPE_FIXED :
            return "fixed location";
    }
}

enum class MAV_ODID_CLASSIFICATION_TYPE {
    MAV_ODID_CLASSIFICATION_TYPE_UNDECLARED = 0,
    MAV_ODID_CLASSIFICATION_TYPE_EU = 1
};

template<>
QString enum_cast<MAV_ODID_CLASSIFICATION_TYPE>(MAV_ODID_CLASSIFICATION_TYPE value) {
    switch(value) {
        case MAV_ODID_CLASSIFICATION_TYPE::MAV_ODID_CLASSIFICATION_TYPE_UNDECLARED :
            return "undeclared";
        case MAV_ODID_CLASSIFICATION_TYPE::MAV_ODID_CLASSIFICATION_TYPE_EU :
            return "follows EU specifications";
    }
}

enum class MAV_ODID_CATEGORY_EU {
    MAV_ODID_CATEGORY_EU_UNDECLARED = 0,
    MAV_ODID_CATEGORY_EU_OPEN = 1,
    MAV_ODID_CATEGORY_EU_SPECIFIC = 2,
    MAV_ODID_CATEGORY_EU_CERTIFIED = 3
};

template<>
QString enum_cast<MAV_ODID_CATEGORY_EU>(MAV_ODID_CATEGORY_EU value) {
    switch(value) {
        case MAV_ODID_CATEGORY_EU::MAV_ODID_CATEGORY_EU_UNDECLARED :
            return "undeclared";
        case MAV_ODID_CATEGORY_EU::MAV_ODID_CATEGORY_EU_OPEN :
            return "EU Open";
        case MAV_ODID_CATEGORY_EU::MAV_ODID_CATEGORY_EU_SPECIFIC :
            return "EU Specific";
        case MAV_ODID_CATEGORY_EU::MAV_ODID_CATEGORY_EU_CERTIFIED :
            return "EU Certified";
    }
}

enum class MAV_ODID_CLASS_EU {
    MAV_ODID_CLASS_EU_UNDECLARED = 0,
    MAV_ODID_CLASS_EU_CLASS_0 = 1,
    MAV_ODID_CLASS_EU_CLASS_1 = 2,
    MAV_ODID_CLASS_EU_CLASS_2 = 3,
    MAV_ODID_CLASS_EU_CLASS_3 = 4,
    MAV_ODID_CLASS_EU_CLASS_4 = 5,
    MAV_ODID_CLASS_EU_CLASS_5 = 6,
    MAV_ODID_CLASS_EU_CLASS_6 = 7
};

template<>
QString enum_cast<MAV_ODID_CLASS_EU>(MAV_ODID_CLASS_EU value) {
    switch(value) {
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_UNDECLARED :
            return "undeclared";
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_CLASS_0 :
            return "EU Class 0";
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_CLASS_1 :
            return "EU Class 1";
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_CLASS_2 :
            return "EU Class 2";
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_CLASS_3 :
            return "EU Class 3";
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_CLASS_4 :
            return "EU Class 4";
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_CLASS_5 :
            return "EU Class 5";
        case MAV_ODID_CLASS_EU::MAV_ODID_CLASS_EU_CLASS_6 :
            return "EU Class 6";
    }
}

enum class MAV_ODID_OPERATOR_ID_TYPE {
    MAV_ODID_OPERATOR_ID_TYPE_CAA = 0
};

template<>
QString enum_cast<MAV_ODID_OPERATOR_ID_TYPE>(MAV_ODID_OPERATOR_ID_TYPE value) {
    switch(value) {
        case MAV_ODID_OPERATOR_ID_TYPE::MAV_ODID_OPERATOR_ID_TYPE_CAA :
            return "CAA registered operator ID";
    }
}

enum class TUNE_FORMAT {
    TUNE_FORMAT_QBASIC1_1 = 1,
    TUNE_FORMAT_MML_MODERN = 2
};

template<>
QString enum_cast<TUNE_FORMAT>(TUNE_FORMAT value) {
    switch(value) {
        case TUNE_FORMAT::TUNE_FORMAT_QBASIC1_1 :
            return "QBasic 1.1";
        case TUNE_FORMAT::TUNE_FORMAT_MML_MODERN :
            return "Modern Music Markup Language (MML)";
    }
}

enum class COMPONENT_CAP_FLAGS {
    COMPONENT_CAP_FLAGS_PARAM = 1,
    COMPONENT_CAP_FLAGS_PARAM_EXT = 2
};

template<>
QString enum_cast<COMPONENT_CAP_FLAGS>(COMPONENT_CAP_FLAGS value) {
    switch(value) {
        case COMPONENT_CAP_FLAGS::COMPONENT_CAP_FLAGS_PARAM :
            return "supports parameter protocol";
        case COMPONENT_CAP_FLAGS::COMPONENT_CAP_FLAGS_PARAM_EXT :
            return "supports extended parameter protocol";
    }
}

enum class AIS_TYPE {
    AIS_TYPE_UNKNOWN = 0,
    AIS_TYPE_RESERVED_1 = 1,
    AIS_TYPE_RESERVED_2 = 2,
    AIS_TYPE_RESERVED_3 = 3,
    AIS_TYPE_RESERVED_4 = 4,
    AIS_TYPE_RESERVED_5 = 5,
    AIS_TYPE_RESERVED_6 = 6,
    AIS_TYPE_RESERVED_7 = 7,
    AIS_TYPE_RESERVED_8 = 8,
    AIS_TYPE_RESERVED_9 = 9,
    AIS_TYPE_RESERVED_10 = 10,
    AIS_TYPE_RESERVED_11 = 11,
    AIS_TYPE_RESERVED_12 = 12,
    AIS_TYPE_RESERVED_13 = 13,
    AIS_TYPE_RESERVED_14 = 14,
    AIS_TYPE_RESERVED_15 = 15,
    AIS_TYPE_RESERVED_16 = 16,
    AIS_TYPE_RESERVED_17 = 17,
    AIS_TYPE_RESERVED_18 = 18,
    AIS_TYPE_RESERVED_19 = 19,
    AIS_TYPE_WIG = 20,
    AIS_TYPE_WIG_HAZARDOUS_A = 21,
    AIS_TYPE_WIG_HAZARDOUS_B = 22,
    AIS_TYPE_WIG_HAZARDOUS_C = 23,
    AIS_TYPE_WIG_HAZARDOUS_D = 24,
    AIS_TYPE_WIG_RESERVED_1 = 25,
    AIS_TYPE_WIG_RESERVED_2 = 26,
    AIS_TYPE_WIG_RESERVED_3 = 27,
    AIS_TYPE_WIG_RESERVED_4 = 28,
    AIS_TYPE_WIG_RESERVED_5 = 29,
    AIS_TYPE_FISHING = 30,
    AIS_TYPE_TOWING = 31,
    AIS_TYPE_TOWING_LARGE = 32,
    AIS_TYPE_DREDGING = 33,
    AIS_TYPE_DIVING = 34,
    AIS_TYPE_MILITARY = 35,
    AIS_TYPE_SAILING = 36,
    AIS_TYPE_PLEASURE = 37,
    AIS_TYPE_RESERVED_20 = 38,
    AIS_TYPE_RESERVED_21 = 39,
    AIS_TYPE_HSC = 40,
    AIS_TYPE_HSC_HAZARDOUS_A = 41,
    AIS_TYPE_HSC_HAZARDOUS_B = 42,
    AIS_TYPE_HSC_HAZARDOUS_C = 43,
    AIS_TYPE_HSC_HAZARDOUS_D = 44,
    AIS_TYPE_HSC_RESERVED_1 = 45,
    AIS_TYPE_HSC_RESERVED_2 = 46,
    AIS_TYPE_HSC_RESERVED_3 = 47,
    AIS_TYPE_HSC_RESERVED_4 = 48,
    AIS_TYPE_HSC_UNKNOWN = 49,
    AIS_TYPE_PILOT = 50,
    AIS_TYPE_SAR = 51,
    AIS_TYPE_TUG = 52,
    AIS_TYPE_PORT_TENDER = 53,
    AIS_TYPE_ANTI_POLLUTION = 54,
    AIS_TYPE_LAW_ENFORCEMENT = 55,
    AIS_TYPE_SPARE_LOCAL_1 = 56,
    AIS_TYPE_SPARE_LOCAL_2 = 57,
    AIS_TYPE_MEDICAL_TRANSOPORT = 58,
    AIS_TYPE_NONECOMBATANT = 59,
    AIS_TYPE_PASSENGER = 60,
    AIS_TYPE_PASSENGER_HAZARDOUS_A = 61,
    AIS_TYPE_PASSENGER_HAZARDOUS_B = 62,
    AIS_TYPE_PASSENGER_HAZARDOUS_C = 63,
    AIS_TYPE_PASSENGER_HAZARDOUS_D = 64,
    AIS_TYPE_PASSENGER_RESERVED_1 = 65,
    AIS_TYPE_PASSENGER_RESERVED_2 = 66,
    AIS_TYPE_PASSENGER_RESERVED_3 = 67,
    AIS_TYPE_PASSENGER_RESERVED_4 = 68,
    AIS_TYPE_PASSENGER_UNKNOWN = 69,
    AIS_TYPE_CARGO = 70,
    AIS_TYPE_CARGO_HAZARDOUS_A = 71,
    AIS_TYPE_CARGO_HAZARDOUS_B = 72,
    AIS_TYPE_CARGO_HAZARDOUS_C = 73,
    AIS_TYPE_CARGO_HAZARDOUS_D = 74,
    AIS_TYPE_CASRGO_RESERVED_1 = 75,
    AIS_TYPE_CASRGO_RESERVED_2 = 76,
    AIS_TYPE_CASRGO_RESERVED_3 = 77,
    AIS_TYPE_CASRGO_RESERVED_4 = 78,
    AIS_TYPE_CARGO_UNKNOW = 79,
    AIS_TYPE_TANKER = 80,
    AIS_TYPE_TANKER_HAZARDOUS_A = 81,
    AIS_TYPE_TANKER_HAZARDOUS_B = 82,
    AIS_TYPE_TANKER_HAZARDOUS_C = 83,
    AIS_TYPE_TANKER_HAZARDOUS_D = 84,
    AIS_TYPE_TANKER_RESERVED_1 = 85,
    AIS_TYPE_TANKER_RESERVED_2 = 86,
    AIS_TYPE_TANKER_RESERVED_3 = 87,
    AIS_TYPE_TANKER_RESERVED_4 = 88,
    AIS_TYPE_TANKER_UNKNOWN = 89,
    AIS_TYPE_OTHER = 90,
    AIS_TYPE_OTHER_HAZARDOUS_A = 91,
    AIS_TYPE_OTHER_HAZARDOUS_B = 92,
    AIS_TYPE_OTHER_HAZARDOUS_C = 93,
    AIS_TYPE_OTHER_HAZARDOUS_D = 94,
    AIS_TYPE_OTHER_RESERVED_1 = 95,
    AIS_TYPE_OTHER_RESERVED_2 = 96,
    AIS_TYPE_OTHER_RESERVED_3 = 97,
    AIS_TYPE_OTHER_RESERVED_4 = 98,
    AIS_TYPE_OTHER_UNKNOWN = 99
};

template<>
QString enum_cast<AIS_TYPE>(AIS_TYPE value) {
    switch(value) {
        case AIS_TYPE::AIS_TYPE_UNKNOWN :
            return "not available";
        case AIS_TYPE::AIS_TYPE_RESERVED_1 :
            return "reserved 1";
        case AIS_TYPE::AIS_TYPE_RESERVED_2 :
            return "reserved 2";
        case AIS_TYPE::AIS_TYPE_RESERVED_3 :
            return "reserved 3";
        case AIS_TYPE::AIS_TYPE_RESERVED_4 :
            return "reserved 4";
        case AIS_TYPE::AIS_TYPE_RESERVED_5 :
            return "reserved 5";
        case AIS_TYPE::AIS_TYPE_RESERVED_6 :
            return "reserved 6";
        case AIS_TYPE::AIS_TYPE_RESERVED_7 :
            return "reserved 7";
        case AIS_TYPE::AIS_TYPE_RESERVED_8 :
            return "reserved 8";
        case AIS_TYPE::AIS_TYPE_RESERVED_9 :
            return "reserved 9";
        case AIS_TYPE::AIS_TYPE_RESERVED_10 :
            return "reserved 10";
        case AIS_TYPE::AIS_TYPE_RESERVED_11 :
            return "reserved 11";
        case AIS_TYPE::AIS_TYPE_RESERVED_12 :
            return "reserved 12";
        case AIS_TYPE::AIS_TYPE_RESERVED_13 :
            return "reserved 13";
        case AIS_TYPE::AIS_TYPE_RESERVED_14 :
            return "reserved 14";
        case AIS_TYPE::AIS_TYPE_RESERVED_15 :
            return "reserved 15";
        case AIS_TYPE::AIS_TYPE_RESERVED_16 :
            return "reserved 16";
        case AIS_TYPE::AIS_TYPE_RESERVED_17 :
            return "reserved 17";
        case AIS_TYPE::AIS_TYPE_RESERVED_18 :
            return "reserved 18";
        case AIS_TYPE::AIS_TYPE_RESERVED_19 :
            return "reserved 19";
        case AIS_TYPE::AIS_TYPE_WIG :
            return "wing in ground";
        case AIS_TYPE::AIS_TYPE_WIG_HAZARDOUS_A :
            return "wing in ground, hazardous cat. A";
        case AIS_TYPE::AIS_TYPE_WIG_HAZARDOUS_B :
            return "wing in ground, hazardous cat. B";
        case AIS_TYPE::AIS_TYPE_WIG_HAZARDOUS_C :
            return "wing in ground, hazardous cat. C";
        case AIS_TYPE::AIS_TYPE_WIG_HAZARDOUS_D :
            return "wing in ground, hazardous cat. D";
        case AIS_TYPE::AIS_TYPE_WIG_RESERVED_1 :
            return "wing in ground, reserved 1";
        case AIS_TYPE::AIS_TYPE_WIG_RESERVED_2 :
            return "wing in ground, reserved 2";
        case AIS_TYPE::AIS_TYPE_WIG_RESERVED_3 :
            return "wing in ground, reserved 3";
        case AIS_TYPE::AIS_TYPE_WIG_RESERVED_4 :
            return "wing in ground, reserved 4";
        case AIS_TYPE::AIS_TYPE_WIG_RESERVED_5 :
            return "wing in ground, reserved 5";
        case AIS_TYPE::AIS_TYPE_FISHING :
            return "fishing";
        case AIS_TYPE::AIS_TYPE_TOWING :
            return "towing";
        case AIS_TYPE::AIS_TYPE_TOWING_LARGE :
            return "towing large";
        case AIS_TYPE::AIS_TYPE_DREDGING :
            return "dredging or underwater ops";
        case AIS_TYPE::AIS_TYPE_DIVING :
            return "diving";
        case AIS_TYPE::AIS_TYPE_MILITARY :
            return "military";
        case AIS_TYPE::AIS_TYPE_SAILING :
            return "sailing";
        case AIS_TYPE::AIS_TYPE_PLEASURE :
            return "pleasure craft";
        case AIS_TYPE::AIS_TYPE_RESERVED_20 :
            return "reserved 20";
        case AIS_TYPE::AIS_TYPE_RESERVED_21 :
            return "reserved 21";
        case AIS_TYPE::AIS_TYPE_HSC :
            return "high speed craft";
        case AIS_TYPE::AIS_TYPE_HSC_HAZARDOUS_A :
            return "high speed craft, hazardous cat. A";
        case AIS_TYPE::AIS_TYPE_HSC_HAZARDOUS_B :
            return "high speed craft, hazardous cat. B";
        case AIS_TYPE::AIS_TYPE_HSC_HAZARDOUS_C :
            return "high speed craft, hazardous cat. C";
        case AIS_TYPE::AIS_TYPE_HSC_HAZARDOUS_D :
            return "high speed craft, hazardous cat. D";
        case AIS_TYPE::AIS_TYPE_HSC_RESERVED_1 :
            return "high speed craft, reserved 1";
        case AIS_TYPE::AIS_TYPE_HSC_RESERVED_2 :
            return "high speed craft, reserved 2";
        case AIS_TYPE::AIS_TYPE_HSC_RESERVED_3 :
            return "high speed craft, reserved 3";
        case AIS_TYPE::AIS_TYPE_HSC_RESERVED_4 :
            return "high speed craft, reserved 4";
        case AIS_TYPE::AIS_TYPE_HSC_UNKNOWN :
            return "high speed craft, not specified";
        case AIS_TYPE::AIS_TYPE_PILOT :
            return "pilot vessel";
        case AIS_TYPE::AIS_TYPE_SAR :
            return "Search And Rescue";
        case AIS_TYPE::AIS_TYPE_TUG :
            return "tug";
        case AIS_TYPE::AIS_TYPE_PORT_TENDER :
            return "port tender";
        case AIS_TYPE::AIS_TYPE_ANTI_POLLUTION :
            return "anti-pollution";
        case AIS_TYPE::AIS_TYPE_LAW_ENFORCEMENT :
            return "law enforcement";
        case AIS_TYPE::AIS_TYPE_SPARE_LOCAL_1 :
            return "spare-local vessel 1";
        case AIS_TYPE::AIS_TYPE_SPARE_LOCAL_2 :
            return "spare-local vessel 2";
        case AIS_TYPE::AIS_TYPE_MEDICAL_TRANSOPORT :
            return "medical transport";
        case AIS_TYPE::AIS_TYPE_NONCOMBATANT :
            return "noncombatant according to RR resolution No. 18";
        case AIS_TYPE::AIS_TYPE_PASSENGER :
            return "passenger";
        case AIS_TYPE::AIS_TYPE_PASSENGER_HAZARDOUS_A :
            return "passenger, hazardous cat. A";
        case AIS_TYPE::AIS_TYPE_PASSENGER_HAZARDOUS_B :
            return "passenger, hazardous cat. B";
        case AIS_TYPE::AIS_TYPE_PASSENGER_HAZARDOUS_C :
            return "passenger, hazardous cat. C";
        case AIS_TYPE::AIS_TYPE_PASSENGER_HAZARDOUS_D :
            return "passenger, hazardous cat. D";
        case AIS_TYPE::AIS_TYPE_PASSENGER_RESERVED_1 :
            return "passenger, reserved 1";
        case AIS_TYPE::AIS_TYPE_PASSENGER_RESERVED_2 :
            return "passenger, reserved 2";
        case AIS_TYPE::AIS_TYPE_PASSENGER_RESERVED_3 :
            return "passenger, reserved 3";
        case AIS_TYPE::AIS_TYPE_PASSENGER_RESERVED_4 :
            return "passenger, reserved 4";
        case AIS_TYPE::AIS_TYPE_PASSENGER_UNKNOWN :
            return "passenger, not specified";
        case AIS_TYPE::AIS_TYPE_CARGO :
            return "cargo";
        case AIS_TYPE::AIS_TYPE_CARGO_HAZARDOUS_A :
            return "cargo, hazardous cat. A";
        case AIS_TYPE::AIS_TYPE_CARGO_HAZARDOUS_B :
            return "cargo, hazardous cat. B";
        case AIS_TYPE::AIS_TYPE_CARGO_HAZARDOUS_C :
            return "cargo, hazardous cat. C";
        case AIS_TYPE::AIS_TYPE_CARGO_HAZARDOUS_D :
            return "cargo, hazardous cat. D";
        case AIS_TYPE::AIS_TYPE_CASRGO_RESERVED_1 :
            return "cargo, reserved 1";
        case AIS_TYPE::AIS_TYPE_CASRGO_RESERVED_2 :
            return "cargo, reserved 2";
        case AIS_TYPE::AIS_TYPE_CASRGO_RESERVED_3 :
            return "cargo, reserved 3";
        case AIS_TYPE::AIS_TYPE_CASRGO_RESERVED_4 :
            return "cargo, reserved 4";
        case AIS_TYPE::AIS_TYPE_CARGO_UNKNOW :
            return "cargo, not specified";
        case AIS_TYPE::AIS_TYPE_TANKER :
            return "tanker";
        case AIS_TYPE::AIS_TYPE_TANKER_HAZARDOUS_A :
            return "tanker, hazardous cat. A";
        case AIS_TYPE::AIS_TYPE_TANKER_HAZARDOUS_B :
            return "tanker, hazardous cat. B";
        case AIS_TYPE::AIS_TYPE_TANKER_HAZARDOUS_C :
            return "tanker, hazardous cat. C";
        case AIS_TYPE::AIS_TYPE_TANKER_HAZARDOUS_D :
            return "tanker, hazardous cat. D";
        case AIS_TYPE::AIS_TYPE_TANKER_RESERVED_1 :
            return "tanker, reserved 1";
        case AIS_TYPE::AIS_TYPE_TANKER_RESERVED_2 :
            return "tanker, reserved 2";
        case AIS_TYPE::AIS_TYPE_TANKER_RESERVED_3 :
            return "tanker, reserved 3";
        case AIS_TYPE::AIS_TYPE_TANKER_RESERVED_4 :
            return "tanker, reserved 4";
        case AIS_TYPE::AIS_TYPE_TANKER_UNKNOWN :
            return "tanker, not specified";
        case AIS_TYPE::AIS_TYPE_OTHER :
            return "other";
        case AIS_TYPE::AIS_TYPE_OTHER_HAZARDOUS_A :
            return "other, hazardous cat. A";
        case AIS_TYPE::AIS_TYPE_OTHER_HAZARDOUS_B :
            return "other, hazardous cat. B";
        case AIS_TYPE::AIS_TYPE_OTHER_HAZARDOUS_C :
            return "other, hazardous cat. C";
        case AIS_TYPE::AIS_TYPE_OTHER_HAZARDOUS_D :
            return "other, hazardous cat. D";
        case AIS_TYPE::AIS_TYPE_OTHER_RESERVED_1 :
            return "other, reserved 1";
        case AIS_TYPE::AIS_TYPE_OTHER_RESERVED_2 :
            return "other, reserved 2";
        case AIS_TYPE::AIS_TYPE_OTHER_RESERVED_3 :
            return "other, reserved 3";
        case AIS_TYPE::AIS_TYPE_OTHER_RESERVED_4 :
            return "other, reserved 4";
        case AIS_TYPE::AIS_TYPE_OTHER_UNKNOWN :
            return "other, not specified";
    }
}

enum class AIS_NAV_STATUS {
    UNDER_WAY = 0,
    AIS_NAV_ANCHORED = 1,
    AIS_NAV_UN_COMMANDED = 2,
    AIS_NAV_RESTRICTED_MANOEUVERABILITY = 3,
    AIS_NAV_DRAUGHT_CONSTRAINED = 4,
    AIS_NAV_MOORED = 5,
    AIS_NAV_AGROUND = 6,
    AIS_NAV_FISHING = 7,
    AIS_NAV_SAILING = 8,
    AIS_NAV_RESERVED_HSC = 9,
    AIS_NAV_RESERVED_WIG = 10,
    AIS_NAV_RESERVED_1 = 11,
    AIS_NAV_RESERVED_2 = 12,
    AIS_NAV_RESERVED_3 = 13,
    AIS_NAV_AIS_SART = 14,
    AIS_NAV_UNKNOWN = 15
};

template<>
QString enum_cast<AIS_NAV_STATUS>(AIS_NAV_STATUS value) {
    switch(value) {
        case AIS_NAV_STATUS::UNDER_WAY :
            return "under way using engine";
        case AIS_NAV_STATUS::AIS_NAV_ANCHORED :
            return "at anchor";
        case AIS_NAV_STATUS::AIS_NAV_UN_COMMANDED :
            return "not under command";
        case AIS_NAV_STATUS::AIS_NAV_RESTRICTED_MANOEUVERABILITY :
            return "restricted manoeuverability";
        case AIS_NAV_STATUS::AIS_NAV_DRAUGHT_CONSTRAINED :
            return "constrained by her draught";
        case AIS_NAV_STATUS::AIS_NAV_MOORED :
            return "moored";
        case AIS_NAV_STATUS::AIS_NAV_AGROUND :
            return "aground";
        case AIS_NAV_STATUS::AIS_NAV_FISHING :
            return "engaged in fishing";
        case AIS_NAV_STATUS::AIS_NAV_SAILING :
            return "under way sailing";
        case AIS_NAV_STATUS::AIS_NAV_RESERVED_HSC :
            return "reserved for high speed craft";
        case AIS_NAV_STATUS::AIS_NAV_RESERVED_WIG :
            return "reserved for wing in ground";
        case AIS_NAV_STATUS::AIS_NAV_RESERVED_1 :
            return "reserved 1";
        case AIS_NAV_STATUS::AIS_NAV_RESERVED_2 :
            return "reserved 2";
        case AIS_NAV_STATUS::AIS_NAV_RESERVED_3 :
            return "reserved 3";
        case AIS_NAV_STATUS::AIS_NAV_AIS_SART :
            return "AIS-SART is active";
        case AIS_NAV_STATUS::AIS_NAV_UNKNOWN :
            return "not specified";
    }
}

enum class AIS_FLAGS {
    AIS_FLAGS_POSITION_ACCURACY = 0x0001,
    AIS_FLAGS_VALID_COG = 0x0002,
    AIS_FLAGS_VALID_VELOCITY = 0x0004,
    AIS_FLAGS_HIGH_VELOCITY = 0x0008,
    AIS_FLAGS_VALID_TURN_RATE = 0x0010,
    AIS_FLAGS_TURN_RATE_SIGN_ONLY = 0x0020,
    AIS_FLAGS_VALID_DIMENSIONS = 0x0040,
    AIS_FLAGS_LARGE_BOW_DIMENSION = 0x0080,
    AIS_FLAGS_LARGE_STERN_DIMENSION = 0x0100,
    AIS_FLAGS_LARGE_PORT_DIMENSION = 0x0200,
    AIS_FLAGS_LARGE_STARBOARD_DIMENSION = 0x0400,
    AIS_FLAGS_VALID_CALLSIGN = 0x0800,
    AIS_FLAGS_VALID_NAME = 0x1000
};

template<>
QString enum_cast<AIS_FLAGS>(AIS_FLAGS value) {
    switch(value) {
        case AIS_FLAGS::AIS_FLAGS_POSITION_ACCURACY :
            return "position accuracy"; // NOTE: 1 means < 10m
        case AIS_FLAGS::AIS_FLAGS_VALID_COG :
            return "course over ground";
        case AIS_FLAGS::AIS_FLAGS_VALID_VELOCITY :
            return "valid velocity";
        case AIS_FLAGS::AIS_FLAGS_HIGH_VELOCITY :
            return "high velocity"; // NOTE: 1 means > 102.2 knots
        case AIS_FLAGS::AIS_FLAGS_VALID_TURN_RATE :
            return "valid turn rate";
        case AIS_FLAGS::AIS_FLAGS_TURN_RATE_SIGN_ONLY :
            return "valid sign of turn rate";
        case AIS_FLAGS::AIS_FLAGS_VALID_DIMENSIONS :
            return "valid dimensions";
        case AIS_FLAGS::AIS_FLAGS_LARGE_BOW_DIMENSION :
            return "distance to bow is larger than 511 m";
        case AIS_FLAGS::AIS_FLAGS_LARGE_STERN_DIMENSION :
            return "distance to stern is larger than 511 m";
        case AIS_FLAGS::AIS_FLAGS_LARGE_PORT_DIMENSION :
            return "distance to port side is larger than 63 m";
        case AIS_FLAGS::AIS_FLAGS_LARGE_STARBOARD_DIMENSION :
            return "distance to starboard is larger than 63 m";
        case AIS_FLAGS::AIS_FLAGS_VALID_CALLSIGN :
            return "valid callsign";
        case AIS_FLAGS::AIS_FLAGS_VALID_NAME :
            return "valid name";
    }
}

enum class FAILURE_UNIT {
    FAILURE_UNIT_SENSOR_GYRO = 0,
    FAILURE_UNIT_SENSOR_ACCEL = 1,
    FAILURE_UNIT_SENSOR_MAG = 2,
    FAILURE_UNIT_SENSOR_BARO = 3,
    FAILURE_UNIT_SENSOR_GPS = 4,
    FAILURE_UNIT_SENSOR_OPTICAL_FLOW = 5,
    FAILURE_UNIT_SENSOR_VIO = 6,
    FAILURE_UNIT_SENSOR_DISTANCE_SENSOR = 7,
    FAILURE_UNIT_SENSOR_AIRSPEED = 8,
    FAILURE_UNIT_SYSTEM_BATTERY = 100,
    FAILURE_UNIT_SYSTEM_MOTOR = 101,
    FAILURE_UNIT_SYSTEM_SERVO = 102,
    FAILURE_UNIT_SYSTEM_AVOIDANCE = 103,
    FAILURE_UNIT_SYSTEM_RC_SIGNAL = 104,
    FAILURE_UNIT_SYSTEM_MAVLINK_SIGNAL = 105
};

template<>
QString enum_cast<FAILURE_UNIT>(FAILURE_UNIT value) {
    switch(value) {
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_GYRO :
            return "gyroscope";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_ACCEL :
            return "accelerometer";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_MAG :
            return "magnetometer";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_BARO :
            return "barometer";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_GPS :
            return "gps";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_OPTICAL_FLOW :
            return "optical flow sensor";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_VIO :
            return "visual internal odometry";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_DISTANCE_SENSOR :
            return "distance sensor";
        case FAILURE_UNIT::FAILURE_UNIT_SENSOR_AIRSPEED :
            return "airspeed sensor";
        case FAILURE_UNIT::FAILURE_UNIT_SYSTEM_BATTERY :
            return "battery";
        case FAILURE_UNIT::FAILURE_UNIT_SYSTEM_MOTOR :
            return "motor";
        case FAILURE_UNIT::FAILURE_UNIT_SYSTEM_SERVO :
            return "servo";
        case FAILURE_UNIT::FAILURE_UNIT_SYSTEM_AVOIDANCE :
            return "obstacle avoidance system";
        case FAILURE_UNIT::FAILURE_UNIT_SYSTEM_RC_SIGNAL :
            return "RC signal";
        case FAILURE_UNIT::FAILURE_UNIT_SYSTEM_MAVLINK_SIGNAL :
            return "mavlink signal";
    }
}

enum class FAILURE_TYPE {
    FAILURE_TYPE_OK = 0,
    FAILURE_TYPE_OFF = 1,
    FAILURE_TYPE_STUCK = 2,
    FAILURE_TYPE_GARBAGE = 3,
    FAILURE_TYPE_WRONG = 4,
    FAILURE_TYPE_SLOW = 5,
    FAILURE_TYPE_DELAYED = 6,
    FAILURE_TYPE_INTERMITTENT = 7
};

template<>
QString enum_cast<FAILURE_TYPE>(FAILURE_TYPE value) {
    switch(value) {
        case FAILURE_TYPE::FAILURE_TYPE_OK :
            return "no failure";
        case FAILURE_TYPE::FAILURE_TYPE_OFF :
            return "is non-responsive";
        case FAILURE_TYPE::FAILURE_TYPE_STUCK :
            return "is stuck";
        case FAILURE_TYPE::FAILURE_TYPE_GARBAGE :
            return "is reporting garbage";
        case FAILURE_TYPE::FAILURE_TYPE_WRONG :
            return "is consistently wrong";
        case FAILURE_TYPE::FAILURE_TYPE_SLOW :
            return "is slow";
        case FAILURE_TYPE::FAILURE_TYPE_DELAYED :
            return "is delayed in time";
        case FAILURE_TYPE::FAILURE_TYPE_INTERMITTENT :
            return "sometimes working";
    }
}

enum class MAV_WINCH_STATUS_FLAG {
    MAV_WINCH_STATUS_HEALTY = 0x01,
    MAV_WINCH_STATUS_FULLY_RETRACTED = 0x02,
    MAV_WINCH_STATUS_MOVING = 0x04,
    MAV_WINCH_STATUS_CLUTCH_ENGAGED = 0x08
};

template<>
QString enum_cast<MAV_WINCH_STATUS_FLAG>(MAV_WINCH_STATUS_FLAG value) {
    switch(value) {
        case MAV_WINCH_STATUS_FLAG::MAV_WINCH_STATUS_HEALTY :
            return "is healty";
        case MAV_WINCH_STATUS_FLAG::MAV_WINCH_STATUS_FULLY_RETRACTED :
            return "thread is fully retracted";
        case MAV_WINCH_STATUS_FLAG::MAV_WINCH_STATUS_MOVING :
            return "motor is moving";
        case MAV_WINCH_STATUS_FLAG::MAV_WINCH_STATUS_CLUTCH_ENGAGED :
            return "clutch is engaged";
    }
}

enum class MAG_CAL_STATUS {
    MAG_CAL_NOT_STARTED = 0,
    MAG_CAL_WAITING_TO_START = 1,
    MAG_CAL_RUNNING_STEP_ONE = 2,
    MAG_CAL_RUNNING_STEP_TWO = 3,
    MAG_CAL_SUCCESS = 4,
    MAG_CAL_FAILED = 5,
    MAG_CAL_BAD_ORIENTATION = 6,
    MAG_CAL_BAD_RADIUS = 7
};

template<>
QString enum_cast<MAG_CAL_STATUS>(MAG_CAL_STATUS value) {
    switch(value) {
        case MAG_CAL_STATUS::MAG_CAL_NOT_STARTED :
            return "not started";
        case MAG_CAL_STATUS::MAG_CAL_WAITING_TO_START :
            return "waiting to start";
        case MAG_CAL_STATUS::MAG_CAL_RUNNING_STEP_ONE :
            return "running step 1";
        case MAG_CAL_STATUS::MAG_CAL_RUNNING_STEP_TWO :
            return "running step 2";
        case MAG_CAL_STATUS::MAG_CAL_SUCCESS :
            return "success";
        case MAG_CAL_STATUS::MAG_CAL_FAILED :
            return "failed";
        case MAG_CAL_STATUS::MAG_CAL_BAD_ORIENTATION :
            return "bad orientation";
        case MAG_CAL_STATUS::MAG_CAL_BAD_RADIUS :
            return "bad radius";
    }
}

enum class MAV_CMD {
    MAV_CMD_NAV_WAYPOINT = 16,
    MAV_CMD_NAV_LOITER_UNLIM = 17,
    MAV_CMD_NAV_LOITER_TURNS = 18,
    MAV_CMD_NAV_LOITER_TIME = 19,
    MAV_CMD_NAV_RETURN_TO_LAUNCH = 20,
    MAV_CMD_NAV_LAND = 21,
    MAV_CMD_NAV_TAKEOFF = 22,
    MAV_CMD_NAV_LAND_LOCAL = 23,
    MAV_CMD_NAV_TAKEOFF_LOCAL = 24,
    MAV_CMD_NAV_FOLLOW = 25,
    MAV_CMD_NAV_CONTINUE_AND_CHANGE_ALT = 30,
    MAV_CMD_NAV_LOITER_TO_ALT = 31,
    MAV_CMD_DO_FOLLOW = 32,
    MAV_CMD_DO_FOLLOW_REPOSITION = 33,
    MAV_CMD_DO_ORBIT = 34,
    MAV_CMD_NAV_ROI = 80,
    MAV_CMD_NAV_PATHPLANNING = 81,
    MAV_CMD_NAV_SPLINE_WAYPOINT = 82,
    MAV_CMD_NAV_VTOL_TAKEOFF = 84,
    MAV_CMD_NAV_VTOL_LAND = 85,
    MAV_CMD_NAV_GUIDED_ENABLE = 92,
    MAV_CMD_NAV_DELAY = 93,
    MAV_CMD_NAV_PAYLOAD_PLACE = 94,
    MAV_CMD_NAV_LAST = 95,
    MAV_CMD_CONDITION_DELAY = 112,
    MAV_CMD_CONDIDTION_CHANGE_ALT = 113,
    MAV_CMD_CONDIDTION_DISTANCE = 114,
    MAV_CMD_CONDIDTION_YAW = 115,
    MAV_CMD_CONDIDTION_LAST = 159,
    MAV_CMD_DO_SET_MODE = 176,
    MAV_CMD_DO_JUMP = 177,
    MAV_CMD_DO_CHANGE_SPEED = 178,
    MAV_CMD_DO_SET_HOME = 179,
    MAV_CMD_DO_SET_PARAMETER = 180,
    MAV_CMD_DO_SET_RELAY = 181,
    MAV_CMD_REPEAT_RELAY = 182,
    MAV_CMD_DO_SET_SERVO = 183,
    MAV_CMD_DO_REPEAT_SERVO = 184,
    MAV_CMD_DO_FLIGHTTERMINATION = 185,
    MAV_CMD_DO_CHANGE_ALTITUDE = 186,
    MAV_CMD_DO_SET_ACTUATOR = 187,
    MAV_CMD_DO_LAND_START = 189,
    MAV_CMD_DO_RALLY_LAND = 190,
    MAV_CMD_DO_GO_AROUND = 191,
    MAV_CMD_DO_REPOSITION = 192,
    MAV_CMD_DO_PAUSE_CONTINUE = 193,
    MAV_CMD_DO_SET_REVERSE = 194,
    MAV_CMD_DO_SET_ROI_LOCATION = 195,
    MAV_CMD_DO_SET_ROI_WPNEXT_OFFSET = 196,
    MAV_CMD_DO_SET_ROI_NONE = 197,
    MAV_CMD_DO_SET_ROI_SYSID = 198,
    MAV_CMD_DO_CONTROL_VIDEO = 200,
    MAV_CMD_DO_SET_ROI = 201,
    MAV_CMD_DO_DIGICAM_CONFIGURE = 202,
    MAV_CMD_DO_DIGICAM_CONTROL = 203,
    MAV_CMD_DO_MOUNT_CONFIGURE = 204,
    MAV_CMD_DO_MOUNT_CONTROL = 205,
    MAV_CMD_DO_SET_CAM_TRIGG_DIST = 206,
    MAV_CMD_DO_FENCE_ENABLE = 207,
    MAV_CMD_DO_PARACHUTE = 208,
    MAV_CMD_DO_MOTOR_TEST = 209,
    MAV_CMD_DO_INVERTED_FLIGHT = 210,
    MAV_CMD_DO_GRIPPER = 211,
    MAV_CMD_DO_AUTOTUNE_ENABLE = 212,
    MAV_CMD_NAV_SET_YAW_SPEED = 213,
    MAV_CMD_DO_SET_CAM_TRIGG_INTERVAL = 214,
    MAV_CMD_DO_MOUNT_CONTROL_QUAR = 220,
    MAV_CMD_DO_GUIDED_MASTER = 221,
    MAV_CMD_DO_GUIDED_LIMITS = 222,
    MAV_CMD_DO_ENGINE_CONTROL = 223,
    MAV_CMD_DO_SET_MISSION_CURRENT = 224,
    MAV_CMD_DO_LAST = 240,
    MAV_CMD_PREFLIGHT_CALIBRATION = 241,
    MAV_CMD_PREFLIGHT_SET_SENSOR_OFFSETS = 242,
    MAV_CMD_PREFLIGHT_UAVCAN = 243,
    MAV_CMD_PREFLIGHT_STORAGE = 245,
    MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN = 246,
    MAV_CMD_DO_UPGRADE = 247,
    MAV_CMD_OVERRIDE_GOTO = 252,
    MAV_CMD_OBLIQUE_SURVEY = 260,
    MAV_CMD_MISSION_START = 300,
    MAV_CMD_COMPONENT_ARM_DISARM = 400,
    MAV_CMD_ILLUMINATOR_ON_OFF = 405,
    MAV_CMD_GET_HOME_POSITION = 410,
    MAV_CMD_INJECT_FAILURE = 420,
    MAV_CMD_START_RX_PAIR = 500,
    MAV_CMD_GET_MESSAGE_INTERVAL = 510,
    MAV_CMD_SET_MESSAGE_INTERVAL = 511,
    MAV_CMD_REQUEST_PROTOCOL_VERSION = 519,
    MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES = 520,
    MAV_CMD_REQUEST_CAMERA_INFORMATION = 521,
    MAV_CMD_REQUEST_CAMERA_SETTINGS = 522,
    MAV_CMD_REQUEST_STORAGE_INFORMATION = 525,
    MAV_CMD_STORAGE_FORMAT = 526,
    MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS = 527,
    MAV_CMD_REQUEST_FLIGHT_INFORMATION = 528,
    MAV_CMD_RESET_CAMERA_SETTINGS = 529,
    MAV_CMD_SET_CAMERA_MODE = 530,
    MAV_CMD_SET_CAMERA_ZOOM = 531,
    MAV_CMD_SET_CAMERA_FOCUS = 532,
    MAV_CMD_JUMP_TAG = 600,
    MAV_CMD_DO_JUMP_TAG = 601,
    MAV_CMD_PARAM_TRANSACTION = 900,
    MAV_CMD_DO_GIMBAL_MANAGER_PITCHYAW = 1000,
    MAV_CMD_DO_GIMBAL_MANAGER_CONFIGURE = 1001,
    MAV_CMD_IMAGE_START_CAPTURE = 2000,
    MAV_CMD_IMAGE_STOP_CAPTURE = 2001,
    MAV_CMD_REQUEST_CAMERA_IMAGE_CAPTURE = 2002,
    MAV_CMD_DO_TRIGGER_CONTROL = 2003,
    MAV_CMD_CAMERA_TRACK_POINT = 2004,
    MAV_CMD_CAMERA_TRACK_RECTANGLE = 2005,
    MAV_CMD_CAMERA_STOP_TRACKING = 2010,
    MAV_CMD_VIDEO_START_CAPTURE = 2500,
    MAV_CMD_VIDEO_STOP_CAPTURE = 2501,
    MAV_CMD_VIDEO_START_STREAMING = 2502,
    MAV_CMD_VIDEO_STOP_STREAMING = 2503,
    MAV_CMD_REQUEST_VIDEO_STREAM_INFORMATION = 2504,
    MAV_CMD_REQUEST_VIDEO_STREAM_STATUS = 2505,
    MAV_CMD_LOGGING_START = 2510,
    MAV_CMD_LOGGING_STOP = 2511,
    MAV_CMD_AIRFRAME_CONFIGURATION = 2520,
    MAV_CMD_CONTROL_HIGH_LATENCY = 2600,
    MAV_CMD_PANORAMA_CREATE = 2800,
    MAV_CMD_DO_VTOL_TRANSISTION = 3000,
    MAV_CMD_ARM_AUTHORIZATION_REQUEST = 3001,
    MAV_CMD_SET_GUIDED_SUBMODE_STANDARD = 4000,
    MAV_CMD_SET_GUIDED_SUBMODE_CIRCLE = 4001,
    MAV_CMD_CONDIDTION_GATE = 4501,
    MAV_CMD_NAV_FENCE_RETURN_POINT = 5000,
    MAV_CMD_NAV_FENCE_POLYGON_VERTEX_INCLUSION = 5001,
    MAV_CMD_NAV_FENCE_POLYGON_VERTEX_EXCLUSION = 5002,
    MAV_CMD_NAV_FENCE_CIRCLE_INCLUSION = 5003,
    MAV_CMD_NAV_FENCE_CIRCLE_EXCLUSION = 5004,
    MAV_CMD_NAV_RALLY_POINT = 5100,
    MAV_CMD_UAVCAN_GET_NODE_INFO = 5200,
    MAV_CMD_PAYLOAD_PREPARE_DEPLOY = 30001,
    MAV_CMD_PAYLOAD_CONTROL_DEPLOY = 30002,
    MAV_CMD_FIXED_MAG_CAL_YAW = 42006,
    MAV_CMD_DO_WINCH = 42600,
    MAV_CMD_WAYPOINT_USER_1 = 31000,
    MAV_CMD_WAYPOINT_USER_2 = 31001,
    MAV_CMD_WAYPOINT_USER_3 = 31002,
    MAV_CMD_WAYPOINT_USER_4 = 31003,
    MAV_CMD_WAYPOINT_USER_5 = 31004,
    MAV_CMD_SPATIAL_USER_1 = 31005,
    MAV_CMD_SPATIAL_USER_2 = 31006,
    MAV_CMD_SPATIAL_USER_3 = 31007,
    MAV_CMD_SPATIAL_USER_4 = 31008,
    MAV_CMD_SPATIAL_USER_5 = 31009,
    MAV_CMD_USER_1 = 31010,
    MAV_CMD_USER_2 = 31011,
    MAV_CMD_USER_3 = 31012,
    MAV_CMD_USER_4 = 31013,
    MAV_CMD_USER_5 = 31014
};

template<>
QString enum_cast<MAV_CMD>(MAV_CMD value) {
    switch(value) {
        case MAV_CMD::MAV_CMD_NAV_WAYPOINT :
            return "navigate to waypoint";
        case MAV_CMD::MAV_CMD_NAV_LOITER_UNLIM :
            return "unlimited loiter";
        case MAV_CMD::MAV_CMD_NAV_LOITER_TURNS :
            return "loiter for specified number of turns";
        case MAV_CMD::MAV_CMD_NAV_LOITER_TIME :
            return "loiter for specified amount of time";
        case MAV_CMD::MAV_CMD_NAV_RETURN_TO_LAUNCH :
            return "return to launch location";
        case MAV_CMD::MAV_CMD_NAV_LAND :
            return "landing";
        case MAV_CMD::MAV_CMD_NAV_TAKEOFF :
            return "takeoff";
        case MAV_CMD::MAV_CMD_NAV_LAND_LOCAL :
            return "land at local position";
        case MAV_CMD::MAV_CMD_NAV_TAKEOFF_LOCAL :
            return "takeoff from local position";
        case MAV_CMD::MAV_CMD_NAV_FOLLOW :
            return "vehicle following";
        case MAV_CMD::MAV_CMD_NAV_CONTINUE_AND_CHANGE_ALT :
            return "continue on the current course and change to specified altitude";
        case MAV_CMD::MAV_CMD_NAV_LOITER_TO_ALT :
            return "loiter until specified altitude is reached";
        case MAV_CMD::MAV_CMD_DO_FOLLOW :
            return "begin following a target";
        case MAV_CMD::MAV_CMD_DO_FOLLOW_REPOSITION :
            return "reposition after follow target has been sent";
        case MAV_CMD::MAV_CMD_DO_ORBIT :
            return "start orbiting";
        case MAV_CMD::MAV_CMD_NAV_ROI :
            return "set the region of interest";
        case MAV_CMD::MAV_CMD_NAV_PATHPLANNING :
            return "control auto path planing";
        case MAV_CMD::MAV_CMD_NAV_SPLINE_WAYPOINT :
            return "navigate using spline path";
        case MAV_CMD::MAV_CMD_NAV_VTOL_TAKEOFF :
            return "vertical takeoff";
        case MAV_CMD::MAV_CMD_NAV_VTOL_LAND :
            return "vertical landing";
        case MAV_CMD::MAV_CMD_NAV_GUIDED_ENABLE :
            return "external control enable";
        case MAV_CMD::MAV_CMD_NAV_DELAY :
            return "delay the next navigation command";
        case MAV_CMD::MAV_CMD_NAV_PAYLOAD_PLACE :
            return "place payload";
        case MAV_CMD::MAV_CMD_NAV_LAST :
            return "NOP (NAV / ACTION commands enum limit)";
        case MAV_CMD::MAV_CMD_CONDITION_DELAY :
            return "delay mission state machine";
        case MAV_CMD::MAV_CMD_CONDIDTION_CHANGE_ALT :
            return "change altitude to target at specified rate";
        case MAV_CMD::MAV_CMD_CONDIDTION_DISTANCE :
            return "delay mission state machine until desired distance";
        case MAV_CMD::MAV_CMD_CONDIDTION_YAW :
            return "reach a certain target angle";
        case MAV_CMD::MAV_CMD_CONDIDTION_LAST :
            return "NOP (CONDITION commands enum limit)";
        case MAV_CMD::MAV_CMD_DO_SET_MODE :
            return "set system mode";
        case MAV_CMD::MAV_CMD_DO_JUMP :
            return "jump to specified command in mission list";
        case MAV_CMD::MAV_CMD_DO_CHANGE_SPEED :
            return "change speed";
        case MAV_CMD::MAV_CMD_DO_SET_HOME :
            return "change home location";
        case MAV_CMD::MAV_CMD_DO_SET_PARAMETER :
            return "set a system parameter";
        case MAV_CMD::MAV_CMD_DO_SET_RELAY :
            return "set relay condidtion";
        case MAV_CMD::MAV_CMD_REPEAT_RELAY :
            return "cycle a relay condidtions";
        case MAV_CMD::MAV_CMD_DO_SET_SERVO :
            return "set servo PWM value";
        case MAV_CMD::MAV_CMD_DO_REPEAT_SERVO :
            return "cycle servo PWM values";
        case MAV_CMD::MAV_CMD_DO_FLIGHTTERMINATION :
            return "terminate flight immediately";
        case MAV_CMD::MAV_CMD_DO_CHANGE_ALTITUDE :
            return "change altitude";
        case MAV_CMD::MAV_CMD_DO_SET_ACTUATOR :
            return "set actuators";
        case MAV_CMD::MAV_CMD_DO_LAND_START :
            return "start landing";
        case MAV_CMD::MAV_CMD_DO_RALLY_LAND :
            return "landing from rally point";
        case MAV_CMD::MAV_CMD_DO_GO_AROUND :
            return "safely abort landing";
        case MAV_CMD::MAV_CMD_DO_REPOSITION :
            return "reposition to WGS84 global position";
        case MAV_CMD::MAV_CMD_DO_PAUSE_CONTINUE :
            return "hold current position or continue";
        case MAV_CMD::MAV_CMD_DO_SET_REVERSE :
            return "set moving direction forward or reverse";
        case MAV_CMD::MAV_CMD_DO_SET_ROI_LOCATION :
            return "set the region of interest to a location";
        case MAV_CMD::MAV_CMD_DO_SET_ROI_WPNEXT_OFFSET :
            return "set the region of interest to the next waypoint";
        case MAV_CMD::MAV_CMD_DO_SET_ROI_NONE :
            return "cancel previous region of interest commands";
        case MAV_CMD::MAV_CMD_DO_SET_ROI_SYSID :
            return "mount tracks with specified system ID";
        case MAV_CMD::MAV_CMD_DO_CONTROL_VIDEO :
            return "control onboard camera system";
        case MAV_CMD::MAV_CMD_DO_SET_ROI :
            return "set the region of interest";
        case MAV_CMD::MAV_CMD_DO_DIGICAM_CONFIGURE :
            return "configure digital camera";
        case MAV_CMD::MAV_CMD_DO_DIGICAM_CONTROL :
            return "control digital camera";
        case MAV_CMD::MAV_CMD_DO_MOUNT_CONFIGURE :
            return "configure camera / antenna mount";
        case MAV_CMD::MAV_CMD_DO_MOUNT_CONTROL :
            return "control camera / antenna mount";
        case MAV_CMD::MAV_CMD_DO_SET_CAM_TRIGG_DIST :
            return "set camera trigger distance";
        case MAV_CMD::MAV_CMD_DO_FENCE_ENABLE :
            return "enable geofence";
        case MAV_CMD::MAV_CMD_DO_PARACHUTE :
            return "release a parachute";
        case MAV_CMD::MAV_CMD_DO_MOTOR_TEST :
            return "perform motor test";
        case MAV_CMD::MAV_CMD_DO_INVERTED_FLIGHT :
            return "change to / from inverted flight";
        case MAV_CMD::MAV_CMD_DO_GRIPPER :
            return "operate a gripper";
        case MAV_CMD::MAV_CMD_DO_AUTOTUNE_ENABLE :
            return "enable / disable autotune";
        case MAV_CMD::MAV_CMD_NAV_SET_YAW_SPEED :
            return "set vehicle turn angle and speed change";
        case MAV_CMD::MAV_CMD_DO_SET_CAM_TRIGG_INTERVAL :
            return "set camera trigger interval";
        case MAV_CMD::MAV_CMD_DO_MOUNT_CONTROL_QUAR :
            return "control camera / antenna mount using quaternion";
        case MAV_CMD::MAV_CMD_DO_GUIDED_MASTER :
            return "set ID of master controller";
        case MAV_CMD::MAV_CMD_DO_GUIDED_LIMITS :
            return "set limits for external control";
        case MAV_CMD::MAV_CMD_DO_ENGINE_CONTROL :
            return "control engine";
        case MAV_CMD::MAV_CMD_DO_SET_MISSION_CURRENT :
            return "set specified mission item as current item";
        case MAV_CMD::MAV_CMD_DO_LAST :
            return "NOP (DO commands enum limit)";
        case MAV_CMD::MAV_CMD_PREFLIGHT_CALIBRATION :
            return "trigger calibration";
        case MAV_CMD::MAV_CMD_PREFLIGHT_SET_SENSOR_OFFSETS :
            return "set sensors offsets";
        case MAV_CMD::MAV_CMD_PREFLIGHT_UAVCAN :
            return "trigger UAVCAN configuration";
        case MAV_CMD::MAV_CMD_PREFLIGHT_STORAGE :
            return "request storage of different parameter values and logs";
        case MAV_CMD::MAV_CMD_PREFLIGHT_REBOOT_SHUTDOWN :
            return "reboot / shutdown";
        case MAV_CMD::MAV_CMD_DO_UPGRADE :
            return "request to start upgrade";
        case MAV_CMD::MAV_CMD_OVERRIDE_GOTO :
            return "override current mission";
        case MAV_CMD::MAV_CMD_OBLIQUE_SURVEY :
            return "set Camera Auto Pivoting Oblique Survey";
        case MAV_CMD::MAV_CMD_MISSION_START :
            return "start running mission";
        case MAV_CMD::MAV_CMD_COMPONENT_ARM_DISARM :
            return "arm / disarm component";
        case MAV_CMD::MAV_CMD_ILLUMINATOR_ON_OFF :
            return "tune illuminators on / off";
        case MAV_CMD::MAV_CMD_GET_HOME_POSITION :
            return "request home position";
        case MAV_CMD::MAV_CMD_INJECT_FAILURE :
            return "inject artificial failure";
        case MAV_CMD::MAV_CMD_START_RX_PAIR :
            return "start receiver pairing";
        case MAV_CMD::MAV_CMD_GET_MESSAGE_INTERVAL :
            return "request the interval between messages";
        case MAV_CMD::MAV_CMD_SET_MESSAGE_INTERVAL :
            return "set the interval between messages";
        case MAV_CMD::MAV_CMD_REQUEST_PROTOCOL_VERSION :
            return "request MAVLink protocol version compatibillity";
        case MAV_CMD::MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES :
            return "request autopilot capabilities";
        case MAV_CMD::MAV_CMD_REQUEST_CAMERA_INFORMATION :
            return "request camera informatin";
        case MAV_CMD::MAV_CMD_REQUEST_CAMERA_SETTINGS :
            return "request camera settings";
        case MAV_CMD::MAV_CMD_REQUEST_STORAGE_INFORMATION :
            return "request storage information";
        case MAV_CMD::MAV_CMD_STORAGE_FORMAT :
            return "format a storage medium";
        case MAV_CMD::MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS :
            return "request camera capture status";
        case MAV_CMD::MAV_CMD_REQUEST_FLIGHT_INFORMATION :
            return "request flight information";
        case MAV_CMD::MAV_CMD_RESET_CAMERA_SETTINGS :
            return "reset all camera settings to Factory Default";
        case MAV_CMD::MAV_CMD_SET_CAMERA_MODE :
            return "set camera running mode";
        case MAV_CMD::MAV_CMD_SET_CAMERA_ZOOM :
            return "set camera zoom";
        case MAV_CMD::MAV_CMD_SET_CAMERA_FOCUS :
            return "set camera focus";
        case MAV_CMD::MAV_CMD_JUMP_TAG :
            return "tagged jump targe";
        case MAV_CMD::MAV_CMD_DO_JUMP_TAG :
            return "jump to the matching tag in the mission list";
        case MAV_CMD::MAV_CMD_PARAM_TRANSACTION :
            return "request to start / end parameter transacrion";
        case MAV_CMD::MAV_CMD_DO_GIMBAL_MANAGER_PITCHYAW :
            return "set a gimbal attitude";
        case MAV_CMD::MAV_CMD_DO_GIMBAL_MANAGER_CONFIGURE :
            return "gimbal manager configuration";
        case MAV_CMD::MAV_CMD_IMAGE_START_CAPTURE :
            return "start image capture sequence";
        case MAV_CMD::MAV_CMD_IMAGE_STOP_CAPTURE :
            return "stop image capture sequence";
        case MAV_CMD::MAV_CMD_REQUEST_CAMERA_IMAGE_CAPTURE :
            return "re-request a CAMERA_IMAGE_CAPTURED message";
        case MAV_CMD::MAV_CMD_DO_TRIGGER_CONTROL :
            return "enable / disable camera triggering system";
        case MAV_CMD::MAV_CMD_CAMERA_TRACK_POINT :
            return "initiate visual point tracking";
        case MAV_CMD::MAV_CMD_CAMERA_TRACK_RECTANGLE :
            return "initiate visual rectangle tracking";
        case MAV_CMD::MAV_CMD_CAMERA_STOP_TRACKING :
            return "stop visual tracking";
        case MAV_CMD::MAV_CMD_VIDEO_START_CAPTURE :
            return "start video capture";
        case MAV_CMD::MAV_CMD_VIDEO_STOP_CAPTURE :
            return "stop video capture";
        case MAV_CMD::MAV_CMD_VIDEO_START_STREAMING :
            return "start video streaming";
        case MAV_CMD::MAV_CMD_VIDEO_STOP_STREAMING :
            return "stop video streaming";
        case MAV_CMD::MAV_CMD_REQUEST_VIDEO_STREAM_INFORMATION :
            return "request video streaming information";
        case MAV_CMD::MAV_CMD_REQUEST_VIDEO_STREAM_STATUS :
            return "request video stream status";
        case MAV_CMD::MAV_CMD_LOGGING_START :
            return "request to start streaning log data";
        case MAV_CMD::MAV_CMD_LOGGING_STOP :
            return "request to stop streaming log data";
        case MAV_CMD::MAV_CMD_AIRFRAME_CONFIGURATION :
            return "airframe configuration";
        case MAV_CMD::MAV_CMD_CONTROL_HIGH_LATENCY :
            return "request to start / stop transmitting over the high latency telemetry";
        case MAV_CMD::MAV_CMD_PANORAMA_CREATE :
            return "create a panorama at the current position";
        case MAV_CMD::MAV_CMD_DO_VTOL_TRANSISTION :
            return "request VTOL transition";
        case MAV_CMD::MAV_CMD_ARM_AUTHORIZATION_REQUEST :
            return "arm authorization request";
        case MAV_CMD::MAV_CMD_SET_GUIDED_SUBMODE_STANDARD :
            return "set submode to standard guided";
        case MAV_CMD::MAV_CMD_SET_GUIDED_SUBMODE_CIRCLE :
            return "set submode to circle guided";
        case MAV_CMD::MAV_CMD_CONDIDTION_GATE :
            return "delay mission state machine until gate has been reached";
        case MAV_CMD::MAV_CMD_NAV_FENCE_RETURN_POINT :
            return "set fence return point";
        case MAV_CMD::MAV_CMD_NAV_FENCE_POLYGON_VERTEX_INCLUSION :
            return "set polygonal fence area vertex inside";
        case MAV_CMD::MAV_CMD_NAV_FENCE_POLYGON_VERTEX_EXCLUSION :
            return "set polygonal fence area vertex outside";
        case MAV_CMD::MAV_CMD_NAV_FENCE_CIRCLE_INCLUSION :
            return "set circular fence area inside";
        case MAV_CMD::MAV_CMD_NAV_FENCE_CIRCLE_EXCLUSION :
            return "set circular fence area outside";
        case MAV_CMD::MAV_CMD_NAV_RALLY_POINT :
            return "set rally point";
        case MAV_CMD::MAV_CMD_UAVCAN_GET_NODE_INFO :
            return "UAVCAN get node info";
        case MAV_CMD::MAV_CMD_PAYLOAD_PREPARE_DEPLOY :
            return "prepare to deploy payload at specified coordinates";
        case MAV_CMD::MAV_CMD_PAYLOAD_CONTROL_DEPLOY :
            return "control the payload deployment";
        case MAV_CMD::MAV_CMD_FIXED_MAG_CAL_YAW :
            return "magnitometer calibration on known yaw";
        case MAV_CMD::MAV_CMD_DO_WINCH :
            return "operate winch";
        case MAV_CMD::MAV_CMD_WAYPOINT_USER_1 :
            return "user defined waypoint item 1";
        case MAV_CMD::MAV_CMD_WAYPOINT_USER_2 :
            return "user defined waypoint item 2";
        case MAV_CMD::MAV_CMD_WAYPOINT_USER_3 :
            return "user defined waypoint item 3";
        case MAV_CMD::MAV_CMD_WAYPOINT_USER_4 :
            return "user defined waypoint item 4";
        case MAV_CMD::MAV_CMD_WAYPOINT_USER_5 :
            return "user defined waypoint item 5";
        case MAV_CMD::MAV_CMD_SPATIAL_USER_1 :
            return "user defined spatial item 1";
        case MAV_CMD::MAV_CMD_SPATIAL_USER_2 :
            return "user defined spatial item 2";
        case MAV_CMD::MAV_CMD_SPATIAL_USER_3 :
            return "user defined spatial item 3";
        case MAV_CMD::MAV_CMD_SPATIAL_USER_4 :
            return "user defined spatial item 4";
        case MAV_CMD::MAV_CMD_SPATIAL_USER_5 :
            return "user defined spatial item 5";
        case MAV_CMD::MAV_CMD_USER_1 :
            return "user defined command 1";
        case MAV_CMD::MAV_CMD_USER_2 :
            return "user defined command 2";
        case MAV_CMD::MAV_CMD_USER_3 :
            return "user defined command 3";
        case MAV_CMD::MAV_CMD_USER_4 :
            return "user defined command 4";
        case MAV_CMD::MAV_CMD_USER_5 :
            return "user defined command 5";
    }
}


#endif // MAVLINKENUMS_HPP
